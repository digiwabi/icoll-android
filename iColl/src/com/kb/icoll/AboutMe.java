//  iCollMobile
//
//  Created by Khaled Noui
//  Copyright (c) 2014 digiwabi. All rights reserved.
//

package com.kb.icoll;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.InfoWindowAdapter;
import com.google.android.gms.maps.GoogleMap.OnInfoWindowClickListener;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;

public class AboutMe extends Fragment implements OnClickListener, LocationListener {
	
	private GoogleMap mMap;
	SupportMapFragment fragment;
	static LatLng LOC;
	SeekBar sb;
	double c_lat,c_lon;
	String provider="network";
	Location location;
	Handler h;
	Runnable r;
	Map<Marker, InfoWindowContent> markersContent = new HashMap<Marker, InfoWindowContent>();
	ArrayAdapter<InfoWindowContent> ad;
	ListView lv_prospects;
	TextView plan,liste,none_within_25;
	float distance;
	ArrayList<InfoWindowContent> al_dis_within_25km;
	double lat,lon;

	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		View view = inflater.inflate(R.layout.about_me, container, false);
	
		TextView back=(TextView)view.findViewById(R.id.tv_back);
		back.setOnClickListener(this);
		
		if(getArguments().getString("about_me_call").equals("prospects")){
			back.setText("Prospects");
		}else{
			back.setText("Clients");
		}
		
		lv_prospects=(ListView)view.findViewById(R.id.lv_prospects);
		lv_prospects.setVisibility(View.GONE);
		
		sb=(SeekBar)view.findViewById(R.id.seekBar1);
		sb.setMax(10);
		sb.setProgress(5);
		
		sb.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
			
			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {
				int distance =51-seekBar.getProgress()*5;
				System.out.println(distance);
				addData(distance);
				implementListView(al_dis_within_25km);		
			}
			
			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {
				
			}
			
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress,
					boolean fromUser) {
				mMap.animateCamera(CameraUpdateFactory.zoomTo((progress+11)/2+5), 2000, null);
			}
		});
		
		plan=(TextView)view.findViewById(R.id.tv_plan);
		plan.setOnClickListener(this);
		plan.setBackgroundResource(R.drawable.orange_solid_left_rounded);
		plan.setTextColor(getResources().getColor(R.color.white));
		
		liste=(TextView)view.findViewById(R.id.tv_liste);
		liste.setOnClickListener(this);
		liste.setBackgroundResource(0);
		liste.setTextColor(getResources().getColor(R.color.theme_color));
		
		none_within_25=(TextView)view.findViewById(R.id.tv_none_within_25);
		none_within_25.setVisibility(View.GONE);
		
		if (mMap == null) {
			 mMap = ((SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map)).getMap();
		     if (mMap != null) {
		    	 
		    	mMap.setMyLocationEnabled(true);
		    	LocationManager lm = (LocationManager)getActivity().getSystemService(Context.LOCATION_SERVICE);
		 		lm.requestLocationUpdates(provider, 1000L, 1.0f, this);
		 		location = lm.getLastKnownLocation(provider);
		    	
		 		h=new Handler();
				r =new Runnable() {
						
					public void run() {
						if(location!=null){							
							  c_lat=location.getLatitude();
							  c_lon=location.getLongitude();
							  System.out.println("Current Lat is: "+c_lat+"  Long is: "+c_lon);
							  LOC =new LatLng(c_lat, c_lon);
//							  c_lat=49.0085158;
//							  c_lon=2.3840064;
					    	   
//							  Marker venue_loc = mMap.addMarker(new MarkerOptions().position(LOC));	 
							  mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(LOC, 15));
							  mMap.animateCamera(CameraUpdateFactory.zoomTo(10), 2000, null);	
							  
							  addData(25);
						}else{
							h.postDelayed(r, 3000);
						}
					}
				};
				h.post(r);		    			    	
		    	
		       //showing others location on map
				
				if(getArguments().getString("about_me_call").equals("prospects")){
					al_dis_within_25km=new ArrayList<InfoWindowContent>();
					markersContent.clear();
					for(int i=0;i<Prospects.al.size();i++){
						lat=Prospects.al.get(i).prospect_latitude;
						lon=Prospects.al.get(i).prospect_longitude;
						System.out.println("Latitude: "+lat);
						System.out.println("Longitude: "+lon);
						if(lat!=0 || lon!=0){
							Marker venue_loc = mMap.addMarker(new MarkerOptions().position(new LatLng(lat, lon)));							
							InfoWindowContent markerContent = new InfoWindowContent();
							markerContent.client_id=Prospects.al.get(i).id;
							markerContent.client_name=Prospects.al.get(i).first_name+" "+Prospects.al.get(i).name;
							markerContent.addresse=Prospects.al.get(i).adresse;
							markerContent.address=Prospects.al.get(i).postal_code+" "+Prospects.al.get(i).ville;
							markerContent.date=Prospects.al.get(i).last_rdv_confirmed_start_date;
							markerContent.last_rdv_confirmed_statut=Prospects.al.get(i).last_rdv_confirmed_statut;
							markerContent.tel_fixe=Prospects.al.get(i).tel_fixe;
					        markersContent.put(venue_loc, markerContent);					        
					        distance=getDistance(lat, lon, c_lat, c_lon);
							distance=distance/1000;
							System.out.println("Distance from current pos: "+distance+" km");
							if(distance<25){
								//System.out.println(markersContent.get(venue_loc).client_name);
								al_dis_within_25km.add(markersContent.get(venue_loc));
							}
						}
					}
				}else{
					al_dis_within_25km=new ArrayList<AboutMe.InfoWindowContent>();
					markersContent.clear();
					for(int i=0;i<Clients.al.size();i++){
						lat=Clients.al.get(i).client_latitude;
						lon=Clients.al.get(i).client_longitude;						
						if(lat!=0 || lon!=0){
							System.out.println("Latitude: "+lat);
							System.out.println("Longitude: "+lon);
							Marker venue_loc = mMap.addMarker(new MarkerOptions().position(new LatLng(lat, lon)));
							InfoWindowContent markerContent = new InfoWindowContent();
							markerContent.client_id=Clients.al.get(i).client_id;
							markerContent.projet_id=Clients.al.get(i).projet_id;
							markerContent.client_name=Clients.al.get(i).name+" "+Clients.al.get(i).first_name;
							markerContent.addresse=Clients.al.get(i).adresse;
							markerContent.address=Clients.al.get(i).postal_code+" "+Clients.al.get(i).ville;
							markerContent.tel_fixe=Clients.al.get(i).tel_fixe;
							markerContent.package_name=Clients.al.get(i).package_name;
							markerContent.package_puissance=Clients.al.get(i).package_puissance;
							markerContent.projet_etat_installation=Clients.al.get(i).projet_etat_installation;
							markerContent.projet_date_installation=Clients.al.get(i).projet_date_installation;
					        markersContent.put(venue_loc, markerContent);
					        
					        distance=getDistance(lat, lon, c_lat, c_lon);
							distance=distance/1000;
							System.out.println("Distance from current pos: "+distance+" km");
							if(distance<25){
								System.out.println("<<<< "+markersContent.get(venue_loc).client_name);
								al_dis_within_25km.add(markersContent.get(venue_loc));
							}
						}
					}
				}
				
				//addData(25);
		    }
		     
		     
		     mMap.setInfoWindowAdapter(new InfoWindowAdapter() {

		         @Override
		         public View getInfoWindow(Marker arg0) {
		             return null;
		         }

		         @Override
		         public View getInfoContents(Marker arg0) { 

		             // Getting view from the layout file info_window_layout
		             View v = getActivity().getLayoutInflater().inflate(R.layout.map_details_cell_prospects, null);
		             InfoWindowContent markerContent = markersContent.get(arg0);

		             LinearLayout ll_package_type=(LinearLayout)v.findViewById(R.id.ll_package_type);
		             LinearLayout ll_puissance=(LinearLayout)v.findViewById(R.id.ll_puissance);
		             LinearLayout ll_etat_installation=(LinearLayout)v.findViewById(R.id.ll_etat_installation);
		             LinearLayout ll_date_installation=(LinearLayout)v.findViewById(R.id.ll_date_installation);
		             LinearLayout ll_date_dernier=(LinearLayout)v.findViewById(R.id.ll_date_dernier);
		             LinearLayout ll_statut_dernier=(LinearLayout)v.findViewById(R.id.ll_statut_dernier);
		            		 
		             TextView client_name = (TextView)v.findViewById(R.id.tv_client_name);
		             client_name.setText(markerContent.client_name);
		             
		             TextView add = (TextView)v.findViewById(R.id.tv_addresse);
		             add.setText(markerContent.addresse);
		             
		             TextView add1= (TextView)v.findViewById(R.id.tv_address);
		             add1.setText(markerContent.address);
		             
		             if(getArguments().getString("about_me_call").equals("prospects")){
		            	 ll_date_dernier.setVisibility(View.VISIBLE);
		            	 ll_statut_dernier.setVisibility(View.VISIBLE);
		            	 ll_package_type.setVisibility(View.GONE);
		            	 ll_puissance.setVisibility(View.GONE);
		            	 ll_etat_installation.setVisibility(View.GONE);
		            	 ll_date_installation.setVisibility(View.GONE);
		            	 
		            	 TextView date= (TextView)v.findViewById(R.id.tv_date_du_dernier_rdv);
			             date.setText(markerContent.date);
			             
			             TextView last_rdv_confirmed_statut= (TextView)v.findViewById(R.id.tv_statut_dernier_rdv);
			             last_rdv_confirmed_statut.setText(markerContent.last_rdv_confirmed_statut);		      
		            	 
		            	 
		             }else{
		            	 ll_date_dernier.setVisibility(View.GONE);
		            	 ll_statut_dernier.setVisibility(View.GONE);
		            	 ll_package_type.setVisibility(View.VISIBLE);
		            	 ll_puissance.setVisibility(View.VISIBLE);
		            	 ll_etat_installation.setVisibility(View.VISIBLE);
		            	 ll_date_installation.setVisibility(View.VISIBLE);
		            	 
		            	 TextView package_type= (TextView)v.findViewById(R.id.tv_package_type);
		            	 package_type.setText(markerContent.package_name);
			             
			             TextView puissance_installation= (TextView)v.findViewById(R.id.tv_puissance_installation);
			             puissance_installation.setText(markerContent.package_puissance);
			             
			             TextView etat_installation= (TextView)v.findViewById(R.id.tv_etat_installation);
			             etat_installation.setText(markerContent.projet_etat_installation);
			             
			             TextView date_installation= (TextView)v.findViewById(R.id.tv_date_installation);
			             if(markerContent.projet_date_installation.length()==0){
			            	 date_installation.setText("non renseigné");
			             }else{
			            	 date_installation.setText(markerContent.projet_date_installation);
			             }
		             }

		             return v;

		         }
		     });
		     
		     mMap.setOnInfoWindowClickListener(new OnInfoWindowClickListener() {
				
				@Override
				public void onInfoWindowClick(Marker arg0) {
					InfoWindowContent markerContent = markersContent.get(arg0);
					InfoDetails id=new InfoDetails();
					Bundle b=new Bundle();
					b.putString("client_name", markerContent.client_name); 
					b.putString("addresse", markerContent.addresse); 
					b.putString("address", markerContent.address);
					b.putString("tel_fixe", markerContent.tel_fixe);
					b.putDouble("current_lat", c_lat);
					b.putDouble("current_lon", c_lon);
					b.putDouble("destination_lat", arg0.getPosition().latitude);
					b.putDouble("destination_lon", arg0.getPosition().longitude);
					if(getArguments().getString("about_me_call").equals("prospects")){
						b.putString("id", markerContent.client_id);
						b.putString("date", markerContent.date); 
						b.putString("last_rdv_confirmed_statut", markerContent.last_rdv_confirmed_statut);
						b.putString("info_call_via", "prospects");
					}else{
						b.putString("client_id", markerContent.client_id);
						b.putString("projet_id", markerContent.projet_id);
						b.putString("package_type", markerContent.package_name);
						b.putString("package_puissance", markerContent.package_puissance);
						b.putString("etat_installation", markerContent.projet_etat_installation);
						b.putString("date_installation", markerContent.projet_date_installation);
						b.putString("info_call_via", "client");
					}
					id.setArguments(b);
					getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.content_frame2, id, "info_details").commit();
				}
			});
		}
		 
		return view;
	}
	
	private void addData(int x) {
		al_dis_within_25km=new ArrayList<InfoWindowContent>();
		if(getArguments().getString("about_me_call").equals("prospects")){			
			for(int i=0;i<Prospects.al.size();i++){
				lat=Prospects.al.get(i).prospect_latitude;
				lon=Prospects.al.get(i).prospect_longitude;
				System.out.println("Latitude: "+lat);
				System.out.println("Longitude: "+lon);				
				if(lat!=0 || lon!=0){					
					InfoWindowContent markerContent = new InfoWindowContent();
					markerContent.client_id=Prospects.al.get(i).id;
					markerContent.client_name=Prospects.al.get(i).first_name+" "+Prospects.al.get(i).name;
					markerContent.addresse=Prospects.al.get(i).adresse;
					markerContent.address=Prospects.al.get(i).postal_code+" "+Prospects.al.get(i).ville;
					markerContent.date=Prospects.al.get(i).last_rdv_confirmed_start_date;
					markerContent.last_rdv_confirmed_statut=Prospects.al.get(i).last_rdv_confirmed_statut;
					markerContent.tel_fixe=Prospects.al.get(i).tel_fixe;				        
			        distance=getDistance(lat, lon, c_lat, c_lon);
					distance=distance/1000;
					System.out.println("Distance from current pos: "+distance+" km");
					if(distance<x){
						al_dis_within_25km.add(markerContent);
					}
				}
			}
		}else{
			for(int i=0;i<Clients.al.size();i++){
				lat=Clients.al.get(i).client_latitude;
				lon=Clients.al.get(i).client_longitude;
				System.out.println("Latitude: "+lat);
				System.out.println("Longitude: "+lon);
				if(lat!=0 || lon!=0){
					InfoWindowContent markerContent = new InfoWindowContent();
					markerContent.client_id=Clients.al.get(i).client_id;
					markerContent.projet_id=Clients.al.get(i).projet_id;
					markerContent.client_name=Clients.al.get(i).name+" "+Clients.al.get(i).first_name;
					markerContent.addresse=Clients.al.get(i).adresse;
					markerContent.address=Clients.al.get(i).postal_code+" "+Clients.al.get(i).ville;
					markerContent.tel_fixe=Clients.al.get(i).tel_fixe;
					markerContent.package_name=Clients.al.get(i).package_name;
					markerContent.package_puissance=Clients.al.get(i).package_puissance;
					markerContent.projet_etat_installation=Clients.al.get(i).projet_etat_installation;
					markerContent.projet_date_installation=Clients.al.get(i).projet_date_installation;			        			        
			        distance=getDistance(lat, lon, c_lat, c_lon);
					distance=distance/1000;
					System.out.println("Distance from current pos: "+distance+" km");
					if(distance<x){
						al_dis_within_25km.add(markerContent);
					}
				}
			}
		}
	}

	private class InfoWindowContent{
		String client_name,addresse,address,date,last_rdv_confirmed_statut,tel_fixe,package_name,package_puissance,projet_etat_installation,projet_date_installation,client_id,projet_id;
	}
	
		private float getDistance(double lat1, double lon1, double lat2, double lon2){
			Location loc1 = new Location("");
			loc1.setLatitude(lat1);
			loc1.setLongitude(lon1);

			Location loc2 = new Location("");
			loc2.setLatitude(lat2);
			loc2.setLongitude(lon2);

			float distanceInMeters = loc1.distanceTo(loc2);
			return distanceInMeters;
		}
		
		
		
		
		@Override
		public void onClick(View v) {
			int id = v.getId();
			if (id == R.id.tv_back) {
				//				if(getArguments().getString("about_me_call").equalsIgnoreCase("prospects")){
//					getActivity().getSupportFragmentManager().beginTransaction()
//					.replace(R.id.content_frame, new Prospects(), "prospects").commit();
//				}else{
//					getActivity().getSupportFragmentManager().beginTransaction()
//					.replace(R.id.content_frame, new Clients(), "clients").commit();
//				}
				getActivity().getSupportFragmentManager().beginTransaction()
				.remove(this).commit();
			} else if (id == R.id.tv_plan) {
				lv_prospects.setVisibility(View.GONE);
				none_within_25.setVisibility(View.GONE);
				plan.setBackgroundResource(R.drawable.orange_solid_left_rounded);
				plan.setTextColor(getResources().getColor(R.color.white));
				liste.setBackgroundResource(0);
				liste.setTextColor(getResources().getColor(R.color.theme_color));
			} else if (id == R.id.tv_liste) {
				lv_prospects.setVisibility(View.VISIBLE);
				liste.setBackgroundResource(R.drawable.orange_solid_right_rounded);
				liste.setTextColor(getResources().getColor(R.color.white));
				plan.setBackgroundResource(0);
				plan.setTextColor(getResources().getColor(R.color.theme_color));
				System.out.println("size of list: "+al_dis_within_25km.size());
				implementListView(al_dis_within_25km);
				if(al_dis_within_25km.size()>0){
					none_within_25.setVisibility(View.GONE);
				}else{
					none_within_25.setVisibility(View.VISIBLE);
				}
			}
		}
		

		@Override
		public void onLocationChanged(Location location) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void onStatusChanged(String provider, int status, Bundle extras) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void onProviderEnabled(String provider) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void onProviderDisabled(String provider) {
			// TODO Auto-generated method stub
			
		}
		
		private void implementListView(ArrayList<InfoWindowContent> al2) {
			if(al2.size()>0){
				none_within_25.setVisibility(View.GONE);
			}else{
				none_within_25.setVisibility(View.VISIBLE);
			}
			ad = new MyAdapter(getActivity(), R.layout.about_me, al2);	
			lv_prospects.setAdapter(ad);
			ad.notifyDataSetChanged();
			lv_prospects.setTextFilterEnabled(true);
		}
		
		private class MyAdapter extends ArrayAdapter<InfoWindowContent>{
			
			ArrayList<InfoWindowContent> my_al_data;
			
			public MyAdapter(Context context, int textViewResourceId,
					ArrayList<InfoWindowContent> al) {
				super(context, textViewResourceId, al);	
				my_al_data=al;
			}
			
			
			@Override
			public View getView(final int position, View convertView, ViewGroup parent) {
				View v = convertView;
				
				if (v == null) {				
					LayoutInflater vi = (LayoutInflater)getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);				
			        v = vi.inflate(R.layout.alerts_prospects_liste_cell, null);		        			        				
				}												

				TextView prospects_name=(TextView)v.findViewById(R.id.tv_prospects_name);
				//System.out.println("Client name: "+my_al_data.get(position).client_name);
				prospects_name.setText(my_al_data.get(position).client_name);	

				v.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						if(getArguments().getString("about_me_call").equals("prospects")){
							ProspectDetails cd=new ProspectDetails();
							Bundle b=new Bundle();
							b.putString("id", my_al_data.get(position).client_id);
							b.putString("prospect_details_via", "prospects");
							cd.setArguments(b);
							getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, cd, "prospect_details").commit();							
						}else{
							ClientDetails cd=new ClientDetails();
							Bundle b=new Bundle();
							b.putString("client_id", my_al_data.get(position).client_id);
							b.putString("projet_id", my_al_data.get(position).projet_id);
							b.putString("is_from_alert", "no");
							cd.setArguments(b);
							getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, cd, "client_details").commit();
						}
						getActivity().getSupportFragmentManager().beginTransaction().remove(AboutMe.this).commit();
					}
				});
			
				return v;
			}
		}
}
