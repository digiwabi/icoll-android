//  iCollMobile
//
//  Created by Khaled Noui
//  Copyright (c) 2014 digiwabi. All rights reserved.
//

package com.kb.icoll;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.kb.icoll.utils.OnTabChangedListener;
import com.kb.icoll.utils.RESTful;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;

public class Agenda extends Fragment implements OnClickListener{
	OnTabChangedListener mListener;
	TextView liste,jour,mois;
	public static ArrayList<AgendaData> al_agenda_list=new ArrayList<AgendaData>();
	public static ArrayList<String> al_new_ag_list=new ArrayList<String>();
	TextView header;
	String today;
	int flag;
	JSONArray data;
	TextView non_statue;
	SharedPreferences myPref;
	TextView aujourd_d_hui;
	public double ag_lat,ag_lon;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.agenda, container,false);
		mListener.onTabChanged(2);
		
		myPref=getActivity().getSharedPreferences("app_data", 0);
		
		non_statue=(TextView)view.findViewById(R.id.tv_non_statue);
		non_statue.setOnClickListener(this);
		
		aujourd_d_hui=(TextView)view.findViewById(R.id.tv_aujour_d_hui);
		aujourd_d_hui.setOnClickListener(this);
		
		liste=(TextView)view.findViewById(R.id.tv_liste);  //Listview
		liste.setOnClickListener(this);
		
		jour=(TextView)view.findViewById(R.id.tv_jour);  //Day
		jour.setOnClickListener(this);
		
		mois=(TextView)view.findViewById(R.id.tv_mois);  //Month
		mois.setOnClickListener(this);
		
		header=(TextView)view.findViewById(R.id.tv_header);
		
		if(new RESTful(getActivity()).checkNetworkConnection()){
			new GetAgendaListing().execute("");
		}
		
		setSelectedOption(3);
		
		return view;
	}
	
	 @Override
	 public void onAttach(Activity activity) {
		 super.onAttach(activity);
	     try {	       
	    	 mListener= (OnTabChangedListener) activity;
	     } catch (ClassCastException e) {
	    	 throw new ClassCastException(activity.toString()+ " must implement OnTabChangedListener");
	     }
	 }

	@Override
	public void onClick(View v) {
		int id = v.getId();
		if (id == R.id.tv_liste) {
			setSelectedOption(1);
		} else if (id == R.id.tv_jour) {
			setSelectedOption(2);
		} else if (id == R.id.tv_mois) {
			setSelectedOption(3);
		} else if (id == R.id.tv_non_statue) {
			AgendaListe al= (AgendaListe)getActivity().getSupportFragmentManager().findFragmentByTag("agenda_liste");
			if(al!=null){
				al.showNoResultForTodayAlert();
			}
			//			AgendaListe a=new AgendaListe();
//			a.no_result_to_display.setVisibility(View.GONE);
			aujourd_d_hui.setTextSize(12);
			if(data!=null){
				al_agenda_list=new ArrayList<AgendaData>();
				al_new_ag_list=new ArrayList<String>();
				String s="";
				try{
				for(int i=0;i<data.length();i++){					
					JSONObject jo=data.getJSONObject(i);					
					if(!jo.getString("start_date").equals(s)){
						AgendaData ad=new AgendaData();
						ad.start_date=jo.getString("start_date");
						ad.value_type=0;
						if(!jo.getString("statut_rdv_lb").equalsIgnoreCase("Non statué")&&(flag==0)){
							al_agenda_list.add(ad);
							s=ad.start_date;
						}else {
							al_agenda_list.add(ad);
							s=ad.start_date;
						}											
					}									
					AgendaData ad=new AgendaData();
					ad.value_type=1;
					ad.event_id=jo.getString("event_id");
					ad.client_id=jo.getString("client_id");
					ad.address=jo.getString("cp")+" "+jo.getString("ville");
					ad.cp=jo.getString("cp");
					ad.addresse=jo.getString("adresse");
					ad.start_date=jo.getString("start_date");
					
					
					String converted_st_dt = "";
					SimpleDateFormat fmt = new SimpleDateFormat("EEEE d MMMM yyyy",Locale.FRANCE);
			        SimpleDateFormat fmt2 = new SimpleDateFormat("yyyy-MM-dd");
			        
			        String sd=jo.getString("start_date");
			        
			        sd=sd.replace("décembre", "décembre");
			        sd=sd.replace("février", "février");
			        
			       // ad.start_date=sd;

			        try {
			        		Date date = fmt.parse(sd);
			        		converted_st_dt= fmt2.format(date);
			        		
			        }catch(ParseException  pe) {
			        		pe.printStackTrace();
			        }
					
			        System.out.println("Date is: "+converted_st_dt);
					
					ad.start_time=jo.getString("start_time");
					ad.end_time=jo.getString("end_time");
					ad.nom=jo.getString("nom");
					ad.prenom=jo.getString("prenom");
					ad.telephone=jo.getString("tel_fixe");
					ad.description=jo.getString("description");
					ad.statut_rdv=jo.getString("statut_rdv");
					ad.statut_rdv_lb=jo.getString("statut_rdv_lb");
					ad.commentaire=jo.getString("commentaire");
					ad.tel_port=jo.getString("tel_port1");
					try{
						ad.latitude=jo.getDouble("latitude");
						ad.longitude=jo.getDouble("longitude");
						ag_lat=jo.getDouble("latitude");
						ag_lon=jo.getDouble("longitude");
					}catch(JSONException e){
						ad.latitude=0.0;
						ad.longitude=0.0;
						ag_lat=0.0;
						ag_lon=0.0;
					}
					ad.type=jo.getString("type");	
					
					if(flag==0){
						if(ad.statut_rdv_lb.equalsIgnoreCase("Non statué")){
							al_new_ag_list.add(converted_st_dt);
							al_agenda_list.add(ad);
						}
					}else{
						al_new_ag_list.add(converted_st_dt);
						al_agenda_list.add(ad);
					}					
				}
				if(flag==0){
					flag=1;
					non_statue.setText("Tous");
					header.setText("Agenda (Non statués)");
				}else{
					flag=0;
					non_statue.setText("Non statués");
					header.setText("Agenda");
				}
				}catch(JSONException e){
					
				}
				MonthView mv= (MonthView)getActivity().getSupportFragmentManager().findFragmentByTag("month_view");
				if(mv!=null){
					mv.setCalendarEvents();
				}
				AgendaListe aa= (AgendaListe)getActivity().getSupportFragmentManager().findFragmentByTag("agenda_liste");
				if(aa!=null){
					aa.refresh();
				}
			}
		} else if (id == R.id.tv_aujour_d_hui) {
			aujourd_d_hui.setTextSize(14);
			SharedPreferences.Editor editor=myPref.edit();
			editor.putBoolean("is_today_data", true);
			editor.commit();
			getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.ll_agenda_sub_container, new AgendaListe(), "agenda_liste").commit();
			header.setText("Agenda");
		}
	}
	
	private void setSelectedOption(int i){
		aujourd_d_hui.setTextSize(12);
		liste.setBackgroundResource(0);
		jour.setBackgroundResource(0);
		mois.setBackgroundResource(0);
		liste.setTextColor(getResources().getColor(R.color.theme_color));
		jour.setTextColor(getResources().getColor(R.color.theme_color));
		mois.setTextColor(getResources().getColor(R.color.theme_color));
		if(i==1){
			liste.setBackgroundResource(R.drawable.orange_solid_left_rounded);
			liste.setTextColor(getResources().getColor(R.color.white));
			getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.ll_agenda_sub_container, new AgendaListe(), "agenda_liste").commit();
		}if(i==2){
			jour.setBackgroundColor(getResources().getColor(R.color.theme_color));
			jour.setTextColor(getResources().getColor(R.color.white));
			getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.ll_agenda_sub_container, new AgendaDayView(), "agenda_day_view").commit();
		}if(i==3){
			mois.setBackgroundResource(R.drawable.orange_solid_right_rounded);
			mois.setTextColor(getResources().getColor(R.color.white));
			getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.ll_agenda_sub_container, new MonthView(), "month_view").commit();
		}
	}
	 
	private class GetAgendaListing extends AsyncTask<String,Void,Void> {
		ProgressDialog pd;
		@SuppressWarnings("unused")
		int status_flag,total_alert,new_alert,non_statues,no_projets_concernes,
			rendez_confirms,no_photos,no_ticket,no_changement,no_sav_du_jour;
		
		@Override
		public void onPreExecute(){
			pd=new ProgressDialog(getActivity());
			pd.setMessage(getResources().getString(R.string.PLEASE_WAIT));
			pd.setCanceledOnTouchOutside(false);
			pd.show();
		}

		@Override
		protected Void doInBackground(String... params1) {								
			try {
				String reply=new RESTful(getActivity()).queryServer(getResources().getString(R.string.api_end_point)+"agenda", true);
				
		        System.out.println("json_cnst-1: " + reply);

				reply=reply.replace(getResources().getString(R.string.json_cnst), ""); 
		        System.out.println("json_cnst-2: " + reply);

				data = new JSONArray(reply);

				al_agenda_list=new ArrayList<AgendaData>();
				al_new_ag_list=new ArrayList<String>();
				String s="";
				for(int i=0;i<data.length();i++){
					
					JSONObject jo=data.getJSONObject(i);					
					if(!jo.getString("start_date").equals(s)){
						AgendaData ad=new AgendaData();
						ad.start_date=jo.getString("start_date");
						ad.value_type=0;
						al_agenda_list.add(ad);
						s=ad.start_date;
						
					}									
					AgendaData ad=new AgendaData();
					ad.value_type=1;
					ad.event_id=jo.getString("event_id");
					ad.client_id=jo.getString("client_id");
					ad.address=jo.getString("cp")+" "+jo.getString("ville");
					ad.cp=jo.getString("cp");
					ad.addresse=jo.getString("adresse");
					ad.start_date=jo.getString("start_date");
					
					
					String converted_st_dt = "";
					SimpleDateFormat fmt = new SimpleDateFormat("EEEE d MMMM yyyy",Locale.FRANCE);
			        SimpleDateFormat fmt2 = new SimpleDateFormat("yyyy-MM-dd");
			        
			        String sd=jo.getString("start_date");
			        sd=sd.replace("décembre", "décembre");
			        sd=sd.replace("février", "février");
			      //  ad.start_date=sd;
			        try {
			        		Date date = fmt.parse(sd);
			        		converted_st_dt= fmt2.format(date);
			        		
			        }catch(ParseException  pe) {
			        		pe.printStackTrace();
			        }
					
			        //System.out.println("Date 2 is: "+converted_st_dt);
					
					ad.start_time=jo.getString("start_time");
					ad.end_time=jo.getString("end_time");
					ad.nom=jo.getString("nom");
					ad.prenom=jo.getString("prenom");
					ad.telephone=jo.getString("tel_fixe");
					ad.description=jo.getString("description");
					ad.statut_rdv=jo.getString("statut_rdv");
					ad.statut_rdv_lb=jo.getString("statut_rdv_lb");
					ad.commentaire=jo.getString("commentaire");
					ad.tel_port=jo.getString("tel_port1");
					try{
						ad.latitude=jo.getDouble("latitude");
						ad.longitude=jo.getDouble("longitude");
						ag_lat=jo.getDouble("latitude");
						ag_lon=jo.getDouble("longitude");
					}catch(JSONException e){
						ad.latitude=0.0;
						ad.longitude=0.0;
						ag_lat=0.0;
						ag_lon=0.0;
					}
					
					ad.type=jo.getString("type");	
					
					al_new_ag_list.add(converted_st_dt);
					al_agenda_list.add(ad);
				}
			
				System.out.println("size of agenda list: "+al_agenda_list.size());

				status_flag=2;
			} catch (IOException e) {
				if(e.getMessage().equals("403")){
					status_flag=5;
				}else if(e.getMessage().equals("204")){
					status_flag=7;
				}
				else{
					status_flag=3;
				}
				e.printStackTrace();
			} catch (JSONException e) {
				status_flag=4;
			}catch (NullPointerException e) {
				status_flag=6;
			}										 				 	 			 	
			return null;
		}

		@Override
		public void onPostExecute(Void m_adapter){               	
			pd.dismiss();
			if(status_flag==2){
				MonthView mv= (MonthView)getActivity().getSupportFragmentManager().findFragmentByTag("month_view");
				if(mv!=null){
					mv.setCalendarEvents();
				}
				
				//implementListView(al_agenda_list);
		 	}else if(status_flag==3){
		 		new AlertDialog.Builder(getActivity())
				.setMessage("Erreur de connexion. Veuillez réessayer à nouveau")
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					
					public void onClick(DialogInterface dialog, int id) {							
									
					}
				}).setIcon(android.R.drawable.stat_notify_error).show();
		 	}else if(status_flag==4){
		 		new AlertDialog.Builder(getActivity())
				.setMessage("Erreur du serveur. Veuillez réessayer à nouveau")
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					
					public void onClick(DialogInterface dialog, int id) {							
									
					}
				}).setIcon(android.R.drawable.stat_notify_error).show();
		 	}else if(status_flag==5){
		 		new AlertDialog.Builder(getActivity())
				.setMessage("Mauvaises credintials!")
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					
					public void onClick(DialogInterface dialog, int id) {							
									
					}
				}).setIcon(android.R.drawable.stat_notify_error).show();
		 	}else if(status_flag==6){
		 		new AlertDialog.Builder(getActivity())
				.setMessage("Aucune donnée reçue . S'il vous plait réessayer plus tard.")
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					
					public void onClick(DialogInterface dialog, int id) {							
									
					}
				}).setIcon(android.R.drawable.stat_notify_error).show();
		 	}
		}
	}
	
}
