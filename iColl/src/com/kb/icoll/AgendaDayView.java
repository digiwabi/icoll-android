//  iCollMobile
//
//  Created by Khaled Noui
//  Copyright (c) 2014 digiwabi. All rights reserved.
//

package com.kb.icoll;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Locale;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class AgendaDayView extends Fragment implements OnClickListener{
	Calendar c;
	SimpleDateFormat sdf,sdf1;
	ListView event_list;
	TextView date,no_result_to_display;
	ArrayList<AgendaData> al;
	ArrayList<String> al_date;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.agenda_day_view, container,false);
		
		no_result_to_display=(TextView)view.findViewById(R.id.tv_no_result_disp);
		
		ImageView prev=(ImageView)view.findViewById(R.id.iv_prev);
		prev.setOnClickListener(this);
		
		ImageView nxt=(ImageView)view.findViewById(R.id.iv_next);
		nxt.setOnClickListener(this);
		
		date=(TextView)view.findViewById(R.id.tv_date);
		c=Calendar.getInstance();
		sdf=new SimpleDateFormat("EEEE, dd MMM yyyy", Locale.FRENCH); 
		sdf1=new SimpleDateFormat("yyyy-MM-dd");
		date.setText(sdf.format(c.getTime()));
		
		event_list=(ListView)view.findViewById(R.id.lv_event_list);
		
		al=new ArrayList<AgendaData>();
		for(int i=0;i<Agenda.al_agenda_list.size();i++){
			if(Agenda.al_agenda_list.get(i).statut_rdv_lb!=null){
				al.add(Agenda.al_agenda_list.get(i));				
			}			
		}
		
		al_date=Agenda.al_new_ag_list;
		
		refreshEventList(sdf1.format(c.getTime())); 
		return view;
	}

	@Override
	public void onClick(View v) {
		int id = v.getId();
		if (id == R.id.iv_prev) {
			c.add(Calendar.DATE, -1);
		} else if (id == R.id.iv_next) {
			c.add(Calendar.DATE, 1);
		}
		refreshEventList(sdf1.format(c.getTime())); 
	}
	
	private void refreshEventList(String current_date) {
		date.setText(sdf.format(c.getTime()));				
		ArrayList<AgendaData> s_al=new ArrayList<AgendaData>();
		
		for(int i=0;i<al_date.size();i++){
			if(al_date.get(i).equals(current_date)){
				s_al.add(al.get(i)); 
			}
		}
		
		if(s_al.size()>0){
			no_result_to_display.setVisibility(View.GONE);
		}else{
			no_result_to_display.setVisibility(View.VISIBLE);
		}
		Collections.sort(s_al, new CustomComparatorAsc());
		implementListView(s_al);
	}
	
	private void implementListView(ArrayList<AgendaData> al_cd) {
		ArrayAdapter<AgendaData> ad = new MyAdapter(getActivity(),
				R.layout.agenda_liste_cell_date, al_cd);	
		event_list.setAdapter(ad);
		ad.notifyDataSetChanged();
		event_list.setTextFilterEnabled(true);
	}
	
	public class CustomComparatorAsc implements Comparator<AgendaData> { 

		@Override
		public int compare(AgendaData lhs, AgendaData rhs) {
			SimpleDateFormat fmt =new SimpleDateFormat("HH:mm");
			try {
				return fmt.parse(lhs.start_time).compareTo(fmt.parse(rhs.start_time));
			} catch (java.text.ParseException e) {
				e.printStackTrace();
				return 0;
			}
		}
	}
	
	private class MyAdapter extends ArrayAdapter<AgendaData>{
		ArrayList<AgendaData> my_al;
		
		public MyAdapter(Context context, int textViewResourceId, ArrayList<AgendaData> al) {
			super(context, textViewResourceId, al);	
			my_al=al;
		}
		
		@Override
		public View getView(final int position, View convertView, ViewGroup parent) {
			View v = convertView;
			
			if (v == null) {
				LayoutInflater vi = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				v = vi.inflate(R.layout.agenda_liste_cell_record, null);		
			}
			
			TextView name=(TextView)v.findViewById(R.id.tv_name);
	        name.setText(my_al.get(position).nom+" "+my_al.get(position).prenom);
	        
	        TextView time=(TextView)v.findViewById(R.id.tv_time);
	        time.setText(my_al.get(position).start_time);
	        
	        ImageView dot=(ImageView)v.findViewById(R.id.iv_indicator);
	        TextView hyphen=(TextView)v.findViewById(R.id.tv_hyphen);
	        
	        TextView cp=(TextView)v.findViewById(R.id.tv_cp);
	        cp.setText("("+my_al.get(position).cp+")");
	        
	        if(my_al.get(position).statut_rdv_lb.equalsIgnoreCase("Non statué")){
	        	name.setTextColor(Color.parseColor("#fc0301"));
	        	time.setTextColor(Color.parseColor("#fc0301"));
	        	cp.setTextColor(Color.parseColor("#fc0301"));
	        	hyphen.setTextColor(Color.parseColor("#fc0301"));
	        	dot.setVisibility(View.VISIBLE);
	        }else{
	        	name.setTextColor(Color.BLACK);
	        	time.setTextColor(Color.BLACK);
	        	cp.setTextColor(Color.BLACK);
	        	hyphen.setTextColor(Color.BLACK);
	        	dot.setVisibility(View.GONE);
	        }
			
			v.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					AgendaListe.ag_data=my_al.get(position);
					AlertDetailsRDV al=new AlertDetailsRDV();
					Bundle b=new Bundle();
					//b.putBoolean("is_alertdetails_via_agenda", true);
					b.putString("is_alertdetails_via_agenda","month_view");
					al.setArguments(b);
					getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.content_frame2, al, "alert_details_rdv").commit();
				}
			});
			return v;
		}
	}
}
