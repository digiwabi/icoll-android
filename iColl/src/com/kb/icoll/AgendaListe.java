//  iCollMobile
//
//  Created by Khaled Noui
//  Copyright (c) 2014 digiwabi. All rights reserved.
//

package com.kb.icoll;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.Locale;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class AgendaListe extends Fragment{
	
	ListView lv_agenda_liste;
	ArrayAdapter<AgendaData> ad;
	//ArrayAdapter<AgendaData> ad;
	SharedPreferences myPref;
	String today;
	public static AgendaData ag_data;
	TextView no_result_to_display;
	ArrayList<AgendaData> al_local;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.agenda_liste, container,false);
		
		myPref=getActivity().getSharedPreferences("app_data", 0);
		
		lv_agenda_liste=(ListView)view.findViewById(R.id.lv_agenda_liste);
		
		no_result_to_display=(TextView)view.findViewById(R.id.tv_no_result_disp);
		//new GetAgendaListing().execute("");
		//implementListView(Agenda.al_agenda_list);

		SimpleDateFormat df = new SimpleDateFormat("EEEE d MMMM yyyy", Locale.FRANCE);
		GregorianCalendar my_month = (GregorianCalendar) GregorianCalendar.getInstance();
		GregorianCalendar today_gc= (GregorianCalendar) my_month.clone();
		today=df.format(today_gc.getTime());
		
		System.out.println("Today is: "+today);
	
		if(myPref.getBoolean("is_today_data", false)){
			al_local=new ArrayList<AgendaData>();
			for(int i=0;i<Agenda.al_agenda_list.size();i++)
			{
				if(Agenda.al_agenda_list.get(i).start_date.equals(today)){
					System.out.println("today's event: "+Agenda.al_agenda_list.get(i));
					al_local.add(Agenda.al_agenda_list.get(i));
				}
			}
			implementListView(al_local);
			
			if(al_local.size()==0){
				no_result_to_display.setVisibility(View.VISIBLE);
			}else{
				no_result_to_display.setVisibility(View.GONE);
			}
			
			SharedPreferences.Editor ed=myPref.edit();
			ed.putBoolean("is_today_data", false);
			ed.commit();
		}else{
			implementListView(Agenda.al_agenda_list);
		}
			
		return view;
	}
	
	public void showNoResultForTodayAlert() {
		no_result_to_display.setVisibility(View.GONE);
	}

	public void refresh(){
		implementListView(Agenda.al_agenda_list);
	}
	
	private void implementListView(ArrayList<AgendaData> al_cd) {
		ad = new MyAdapter(getActivity(),
				R.layout.agenda_liste_cell_date, al_cd);	
		lv_agenda_liste.setAdapter(ad);
		ad.notifyDataSetChanged();
		lv_agenda_liste.setTextFilterEnabled(true);
	}
	
	private class MyAdapter extends ArrayAdapter<AgendaData>{
		ArrayList<AgendaData> my_al;
		private static final int CELL_DATE=0;
		private static final int CELL_RECORD=1;
		
		public MyAdapter(Context context, int textViewResourceId, ArrayList<AgendaData> al) {
			super(context, textViewResourceId, al);	
			my_al=al;
		}
				
		@Override
		public int getViewTypeCount() {			
			return 2;
		}
		
		@Override
		public int getItemViewType(int position) {		
			if(my_al.get(position).value_type==0){
				return CELL_DATE;
			}else{
				return CELL_RECORD;
			}			
		}
		
		@Override
		public View getView(final int position, View convertView, ViewGroup parent) {
			View v = convertView;
			
			if (v == null) {
				LayoutInflater vi = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				switch (getItemViewType(position)) {
				case CELL_DATE:			
			        v = vi.inflate(R.layout.agenda_liste_cell_date, null);	
					break;
				case CELL_RECORD:				
			        v = vi.inflate(R.layout.agenda_liste_cell_record, null);
					break;
				}
			}
			
			switch (getItemViewType(position)) {
			case CELL_DATE:			
				TextView date=(TextView)v.findViewById(R.id.tv_date);
				my_al.get(position).start_date=my_al.get(position).start_date.replace("décembre", "décembre");
				my_al.get(position).start_date=my_al.get(position).start_date.replace("février", "février");
				date.setText(my_al.get(position).start_date); 
				
				break;
			case CELL_RECORD:				
		        TextView name=(TextView)v.findViewById(R.id.tv_name);
		        name.setText(my_al.get(position).nom+" "+my_al.get(position).prenom);
		        
		        TextView time=(TextView)v.findViewById(R.id.tv_time);
		        time.setText(my_al.get(position).start_time);
		        
		        ImageView dot=(ImageView)v.findViewById(R.id.iv_indicator);
		        TextView hyphen=(TextView)v.findViewById(R.id.tv_hyphen);
		        
		        TextView cp=(TextView)v.findViewById(R.id.tv_cp);
		        cp.setText("("+my_al.get(position).cp+")");
		        
		        if(my_al.get(position).statut_rdv_lb.equalsIgnoreCase("Non statué")){
		        	name.setTextColor(Color.parseColor("#fc0301"));
		        	time.setTextColor(Color.parseColor("#fc0301"));
		        	cp.setTextColor(Color.parseColor("#fc0301"));
		        	hyphen.setTextColor(Color.parseColor("#fc0301"));
		        	dot.setVisibility(View.VISIBLE);
		        }else{
		        	name.setTextColor(Color.BLACK);
		        	time.setTextColor(Color.BLACK);
		        	cp.setTextColor(Color.BLACK);
		        	hyphen.setTextColor(Color.BLACK);
		        	dot.setVisibility(View.GONE);
		        }
		        
		        v.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						ag_data=my_al.get(position);
						AlertDetailsRDV al=new AlertDetailsRDV();
						Bundle b=new Bundle();
						//b.putBoolean("is_alertdetails_via_agenda", true);
						b.putString("is_alertdetails_via_agenda","agenda_liste");
						al.setArguments(b);
						getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.content_frame1, al, "alert_details_rdv").commit();
					}
				});
				break;
			}
			
			return v;
		}
	}

}
