//  iCollMobile
//
//  Created by Khaled Noui
//  Copyright (c) 2014 digiwabi. All rights reserved.
//

package com.kb.icoll;

import java.io.IOException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.kb.icoll.utils.RESTful;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;

public class AlertChangementDetails extends Fragment implements OnClickListener {
	
	TextView name,address,etape,commentaire,par,date;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.alert_changement_details, container,false);
		
		TextView voir_la_fiche=(TextView)view.findViewById(R.id.tv_voir_la_fiche);
		voir_la_fiche.setOnClickListener(this);
		
		TextView back=(TextView)view.findViewById(R.id.tv_back);
		back.setOnClickListener(this);
		
		name=(TextView)view.findViewById(R.id.tv_name);
		
		address=(TextView)view.findViewById(R.id.tv_address);
		
		etape=(TextView)view.findViewById(R.id.tv_etape);
		
		commentaire=(TextView)view.findViewById(R.id.tv_commentaire);
		
		par=(TextView)view.findViewById(R.id.tv_par);
		
		date=(TextView)view.findViewById(R.id.tv_date);
	
		new GetAlertChangementDetails().execute("");
		
		return view;
	}

	@Override
	public void onClick(View v) {
		int id = v.getId();
		if (id == R.id.tv_voir_la_fiche) {
			getActivity().getSupportFragmentManager().beginTransaction().remove(this).commit();
			ClientDetails cd=new ClientDetails();
			Bundle b=new Bundle();
			b.putString("client_id", AlertListing.cdd.client_id);
			b.putString("is_from_alert", "alert");
			cd.setArguments(b);
			getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, cd, "client_details").commit();
		} else if (id == R.id.tv_back) {
			getActivity().getSupportFragmentManager().beginTransaction().remove(this).commit();
		}
	}
	
	private class GetAlertChangementDetails extends AsyncTask<String,Void,Void> {
		ProgressDialog pd;
		@SuppressWarnings("unused")
		int status_flag,total_alert,new_alert,non_statues,no_projets_concernes,
			rendez_confirms,no_photos,no_ticket,no_changement,no_sav_du_jour;
		ChangementDetailsData chdd;
		
		@Override
		public void onPreExecute(){
			pd=new ProgressDialog(getActivity());
			pd.setMessage(getResources().getString(R.string.PLEASE_WAIT));
			pd.setCanceledOnTouchOutside(false);
			pd.show();
		}

		@Override
		protected Void doInBackground(String... params1) {								
			try {
				String reply=new RESTful(getActivity()).queryServer(getResources().getString(R.string.api_end_point)+"alerte?type_alerte_id=7&client_id="+AlertListing.cdd.client_id+"&commande_id="+AlertListing.cdd.commande_id, true);
		        System.out.println("json_cnst-1: " + reply);

				reply=reply.replace(getResources().getString(R.string.json_cnst), ""); 
		        System.out.println("json_cnst-2: " + reply);
				JSONArray data = new JSONArray(reply);

				for(int i=0;i<data.length();i++){
					JSONObject jo=data.getJSONObject(i);					
					chdd=new ChangementDetailsData();
					chdd.par=jo.getString("par");
					chdd.etape=jo.getString("sujet");
					chdd.date=jo.getString("date");
					chdd.commentaire=jo.getString("comment");					
				}

				status_flag=2;
			} catch (IOException e) {
				if(e.getMessage().equals("403")){
					status_flag=5;
				}else{
					status_flag=3;
				}
				e.printStackTrace();
			} catch (JSONException e) {
				status_flag=4;
			}catch (NullPointerException e) {
				status_flag=6;
			}										 				 	 			 	
			return null;
		}

		@Override
		public void onPostExecute(Void m_adapter){               	
			pd.dismiss();
			if(status_flag==2){
				
				name.setText(AlertListing.cdd.name+" "+AlertListing.cdd.first_name);
				address.setText(AlertListing.cdd.address);
				etape.setText(chdd.etape);
				par.setText(chdd.par);
				commentaire.setText(chdd.commentaire);
				date.setText(chdd.date);
				
		 	}else if(status_flag==3){
		 		new AlertDialog.Builder(getActivity())
				.setMessage("Erreur de connexion. Veuillez réessayer à nouveau")
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					
					public void onClick(DialogInterface dialog, int id) {							
									
					}
				}).setIcon(android.R.drawable.stat_notify_error).show();
		 	}else if(status_flag==4){
		 		new AlertDialog.Builder(getActivity())
				.setMessage("Erreur du serveur. Veuillez réessayer à nouveau")
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					
					public void onClick(DialogInterface dialog, int id) {							
									
					}
				}).setIcon(android.R.drawable.stat_notify_error).show();
		 	}else if(status_flag==5){
		 		new AlertDialog.Builder(getActivity())
				.setMessage("Paramètres de connexion incorrects")
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					
					public void onClick(DialogInterface dialog, int id) {							
									
					}
				}).setIcon(android.R.drawable.stat_notify_error).show();
		 	}else if(status_flag==6){
		 		new AlertDialog.Builder(getActivity())
				.setMessage("Aucune donnée reçue. Veuillez réessayer à nouveau")
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					
					public void onClick(DialogInterface dialog, int id) {							
									
					}
				}).setIcon(android.R.drawable.stat_notify_error).show();
		 	}
		}
	}
	
	private class ChangementDetailsData{
		String par,etape,commentaire,date;
	}

}
