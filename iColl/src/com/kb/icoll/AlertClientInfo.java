//  iCollMobile
//
//  Created by Khaled Noui
//  Copyright (c) 2014 digiwabi. All rights reserved.
//

package com.kb.icoll;

import com.kb.icoll.AlertListing.SAVdujourData;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class AlertClientInfo extends Fragment{
	
TextView name,address,zip_ville,etape_etat,type_de_package,package_choisi,email,telephone;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.alert_client_info, container,false);
		
		name=(TextView)view.findViewById(R.id.tv_name);
		address=(TextView)view.findViewById(R.id.tv_address);
		zip_ville=(TextView)view.findViewById(R.id.tv_zip_ville);
		etape_etat=(TextView)view.findViewById(R.id.tv_etape_etat);
		type_de_package=(TextView)view.findViewById(R.id.tv_type_of_package);
		package_choisi=(TextView)view.findViewById(R.id.tv_package_choisi);
		email=(TextView)view.findViewById(R.id.tv_email);
		telephone=(TextView)view.findViewById(R.id.tv_telephone);
		
		if(AlertListing.sdjd!=null){
			SAVdujourData sdd=AlertListing.sdjd;
			name.setText(sdd.first_name+" "+sdd.name); 
			address.setText(sdd.address);
//			zip_ville.setText(sdd.zip+" "+sdd.ville);
//			etape_etat.setText("Dernier état modifié : "+sdd.projet_etape+" "+sdd.projet_etat);
//			type_de_package.setText("Type de package : "+sdd.type_of_package);
//			package_choisi.setText("Package choisi : "+sdd.package_name);
//			email.setText("Email : "+sdd.email);
//			telephone.setText(text);
		}
		return view;
	}
}
