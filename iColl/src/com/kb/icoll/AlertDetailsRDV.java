//  iCollMobile
//
//  Created by Khaled Noui
//  Copyright (c) 2014 digiwabi. All rights reserved.
//

package com.kb.icoll;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;

public class AlertDetailsRDV extends Fragment implements OnClickListener{
	
	// myPref;
	TextView tele;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.alert_details_rdv, container,false);
		
		//myPref=getActivity().getSharedPreferences("app_data", 0);
		
		TextView back=(TextView)view.findViewById(R.id.tv_back);
		back.setOnClickListener(this);
		
		TextView statuer_rule=(TextView)view.findViewById(R.id.tv_statuer); // rule
		statuer_rule.setOnClickListener(this);
		
		TextView mes_prospects_around=(TextView)view.findViewById(R.id.tv_mes_prospects_autour); // prospects around
		mes_prospects_around.setOnClickListener(this);
		
		TextView localiser=(TextView)view.findViewById(R.id.tv_localiser);
		localiser.setOnClickListener(this);
		
		TextView acceder_la_ficfe=(TextView)view.findViewById(R.id.tv_acceder_a_la_fiche);
		acceder_la_ficfe.setOnClickListener(this);
		
		TextView client=(TextView)view.findViewById(R.id.tv_client);
		TextView address=(TextView)view.findViewById(R.id.tv_address);
		TextView description=(TextView)view.findViewById(R.id.tv_description);
		TextView date=(TextView)view.findViewById(R.id.tv_date);
		TextView status=(TextView)view.findViewById(R.id.tv_status);
		TextView comment=(TextView)view.findViewById(R.id.tv_comments);
		
		tele=(TextView)view.findViewById(R.id.tv_telephone);
		tele.setOnClickListener(this);

		//////////
		//if(getArguments().getBoolean("is_alertdetails_via_agenda", false)){
		if(getArguments().getString("is_alertdetails_via_agenda").equalsIgnoreCase("agenda_liste")){
			client.setText(AgendaListe.ag_data.nom+" "+AgendaListe.ag_data.prenom);
			address.setText(AgendaListe.ag_data.addresse+"\n"+AgendaListe.ag_data.address);
			tele.setText(AgendaListe.ag_data.telephone);
			if(AgendaListe.ag_data.description.length()==0){
				description.setText("Non renseigné");
			}else{
				description.setText(AgendaListe.ag_data.description);
			}
			date.setText(AgendaListe.ag_data.start_date+"\n"+AgendaListe.ag_data.start_time+" à "+AgendaListe.ag_data.end_time);
			status.setText(AgendaListe.ag_data.statut_rdv_lb);
			if(AgendaListe.ag_data.commentaire.length()==0){
				comment.setText("Non renseigné");
			}else{
				comment.setText(AgendaListe.ag_data.commentaire);
			}
		} else if(getArguments().getString("is_alertdetails_via_agenda").equalsIgnoreCase("month_view")){
			client.setText(AgendaListe.ag_data.nom+" "+AgendaListe.ag_data.prenom);
			address.setText(AgendaListe.ag_data.addresse+"\n"+AgendaListe.ag_data.address);
			tele.setText(AgendaListe.ag_data.telephone);
			if(AgendaListe.ag_data.description.length()==0){
				description.setText("Non renseigné");
			}else{
				description.setText(AgendaListe.ag_data.description);
			}
			date.setText(AgendaListe.ag_data.start_date+"\n"+AgendaListe.ag_data.start_time+" à "+AgendaListe.ag_data.end_time);
			status.setText(AgendaListe.ag_data.statut_rdv_lb);
			if(AgendaListe.ag_data.commentaire.length()==0){
				comment.setText("Non renseigné");
			}else{
				comment.setText(AgendaListe.ag_data.commentaire);
			}
		}else{
			client.setText(AlertListing.rsd.name+" "+AlertListing.rsd.first_name);
			address.setText(AlertListing.rsd.adresse+"\n"+AlertListing.rsd.address);
			tele.setText(AlertListing.rsd.telephone);
			if(AlertListing.rsd.description.length()==0){
				description.setText("Non renseigné");
			}else{
				description.setText(AlertListing.rsd.description);
			}		
			date.setText(AlertListing.rsd.start_date+"\n"+AlertListing.rsd.start_time+" à "+AlertListing.rsd.end_time);
			status.setText(AlertListing.rsd.statut);
			if(AlertListing.rsd.commentaire.length()==0){
				comment.setText("Non renseigné");
			}else{
				comment.setText(AlertListing.rsd.commentaire);
			}
		}
		 
		//System.out.println("Client id: "+AlertListing.rsd.client_id);
		return view;
	}

	@Override
	public void onClick(View v) {
		int id = v.getId();
		if (id == R.id.tv_back) {
			//			AlertListing al=new AlertListing();
//			Bundle b=new Bundle();
//			if(getArguments().getString("is_alertdetails_via_agenda").equalsIgnoreCase("agenda_liste")){
//				getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.content_frame1, new Agenda(), "agenda").commit();
//			}else if(getArguments().getString("is_alertdetails_via_agenda").equalsIgnoreCase("month_view")){
//				getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.content_frame1, new Agenda(), "agenda").commit();
//			}else{
//				b.putInt("type",3);
//				al.setArguments(b);
//				getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.content_frame1, al, "alert_listing").commit();
//			}
//			getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.content_frame1, al, "alert_listing").commit();
			getActivity().getSupportFragmentManager().beginTransaction().remove(this).commit();
		} else if (id == R.id.tv_statuer) {
			AlertDetailsStatuer p=new AlertDetailsStatuer();
			Bundle b=new Bundle();
			if(getArguments().getString("is_alertdetails_via_agenda").equalsIgnoreCase("agenda_liste")){//(getArguments().getBoolean("is_alertdetails_via_agenda", false))
				b.putString("client_name", AgendaListe.ag_data.nom+" "+AgendaListe.ag_data.prenom );
				b.putString("client_address", AgendaListe.ag_data.address);
				b.putString("client_id", AgendaListe.ag_data.client_id);
				b.putString("event_id", AgendaListe.ag_data.event_id);
			} else if(getArguments().getString("is_alertdetails_via_agenda").equalsIgnoreCase("month_view")){
				b.putString("client_name", AgendaListe.ag_data.nom+" "+AgendaListe.ag_data.prenom );
				b.putString("client_address", AgendaListe.ag_data.address);
				b.putString("client_id", AgendaListe.ag_data.client_id);
				b.putString("event_id", AgendaListe.ag_data.event_id);
			}else{
				b.putString("client_name", AlertListing.rsd.name+" "+AlertListing.rsd.first_name );
				b.putString("client_address", AlertListing.rsd.address);
				b.putString("client_id", AlertListing.rsd.client_id);
				b.putString("event_id", AlertListing.rsd.event_id);
			}
			p.setArguments(b);
			getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.content_frame1, p, "alert_details_statuer").commit();
			getActivity().getSupportFragmentManager().beginTransaction().remove(this).commit();
		} else if (id == R.id.tv_mes_prospects_autour) {
			AlertsProspectsAround ap=new AlertsProspectsAround();
			Bundle b1=new Bundle();
			if(getArguments().getString("is_alertdetails_via_agenda").equalsIgnoreCase("agenda_liste")){//(getArguments().getBoolean("is_alertdetails_via_agenda", false))
				b1.putString("alert_pros_around_via","agenda_liste");
			} else if(getArguments().getString("is_alertdetails_via_agenda").equalsIgnoreCase("month_view")){
				b1.putString("alert_pros_around_via","month_view");
			}else{
				b1.putString("alert_pros_around_via","alert_listing");
			}
			//			b1.putString("client_name", AlertListing.rsd.name+" "+AlertListing.rsd.first_name );
//			b1.putString("client_address", AlertListing.rsd.address);
			ap.setArguments(b1);
			getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.content_frame2, ap, "alert_prospects_around").commit();
		} else if (id == R.id.tv_localiser) {
			Plan p1=new Plan();
			Bundle b2=new Bundle();
			if(getArguments().getString("is_alertdetails_via_agenda").equalsIgnoreCase("agenda_liste")){
				b2.putString("from", "agenda");
				p1.setArguments(b2);
				getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.content_frame1, p1, "plan").commit();
			}else if(getArguments().getString("is_alertdetails_via_agenda").equalsIgnoreCase("month_view")){
				b2.putString("from", "agenda");
				p1.setArguments(b2);
				getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.content_frame2, p1, "plan").commit();
			}else{
				b2.putString("from", "alert");
				p1.setArguments(b2);
				getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.content_frame1, p1, "plan").commit();
			}
		} else if (id == R.id.tv_acceder_a_la_fiche) {
			ProspectDetails pd=new ProspectDetails();
			Bundle b3=new Bundle();
			if(getArguments().getString("is_alertdetails_via_agenda").equalsIgnoreCase("agenda_liste")){
				b3.putString("id", AgendaListe.ag_data.client_id);
				b3.putString("prospect_details_via", "agenda_liste");
			}else if(getArguments().getString("is_alertdetails_via_agenda").equalsIgnoreCase("month_view")){
				b3.putString("id", AgendaListe.ag_data.client_id);
				b3.putString("prospect_details_via", "month_view");
			}else{
				b3.putString("id", AlertListing.rsd.client_id);
				b3.putString("prospect_details_via", "alert");
			}
			pd.setArguments(b3);
			getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, pd, "prospect_details").commit();
			getActivity().getSupportFragmentManager().beginTransaction().remove(this).commit();
		} else if (id == R.id.tv_telephone) {
			String uri = "tel:" + tele.getText().toString().trim() ;
			Intent intent = new Intent(Intent.ACTION_DIAL);
			intent.setData(Uri.parse(uri));
			startActivity(intent);
		}
	}
}
