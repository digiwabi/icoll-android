//  iCollMobile
//
//  Created by Khaled Noui
//  Copyright (c) 2014 digiwabi. All rights reserved.
//

package com.kb.icoll;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import com.kb.icoll.utils.DatePickerFragment;
import com.kb.icoll.utils.RESTful;
import com.kb.icoll.utils.TimePickerFragment;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class AlertDetailsStatuer extends Fragment implements OnClickListener, LocationListener {

	String[] ar_status={"Non statué","Client absent","Client problématique","Client non équipable","RDV annulé définitivement","Commande refusée","RDV à repositionner par un opérateur","RDV repositionné par le client","RDV repositionné par le vendeur","Nouveau RDV pour pièces","Client en réflexion","Commande signée"};
	EditText status_comments;
	LinearLayout ll_rdv_more,ll_parent,ll_commande_more;
	TextView date_rendezvous,heure_debut,heure_fin,date_de_la_commande;
	EditText numero_de_commande;
	int post_flag=0;
	Spinner sp_status;
	String client_id,event_id;
	Handler h;
	Runnable r;
	String provider="network";
	Location location;
	double current_lat,current_lon;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.alert_details_statuer, container,false);
		
		client_id=getArguments().getString("client_id");
		event_id=getArguments().getString("event_id");
		
		TextView name=(TextView)view.findViewById(R.id.tv_name);
		name.setText(getArguments().getString("client_name"));
		
		TextView address=(TextView)view.findViewById(R.id.tv_address);
		address.setText(getArguments().getString("client_address"));
		
		TextView back=(TextView)view.findViewById(R.id.tv_back);
		back.setOnClickListener(this);
		
		TextView cancel=(TextView)view.findViewById(R.id.tv_cancel);
		cancel.setOnClickListener(this);
		
		TextView valider=(TextView)view.findViewById(R.id.tv_valider);
		valider.setOnClickListener(this);
		
		status_comments=(EditText)view.findViewById(R.id.ed_status_comments);
		
		ll_rdv_more=(LinearLayout)view.findViewById(R.id.ll_rdv_more);
		ll_commande_more=(LinearLayout)view.findViewById(R.id.ll_commande_more);
		
		ll_parent=(LinearLayout)view.findViewById(R.id.ll_parent);
		ll_parent.setOnClickListener(this);
		
		date_rendezvous=(TextView)view.findViewById(R.id.tv_date_rendezvous);
		date_rendezvous.setOnClickListener(this);
		
		heure_debut=(TextView)view.findViewById(R.id.tv_heure_de_debut);
		heure_debut.setOnClickListener(this);
		
		heure_fin=(TextView)view.findViewById(R.id.tv_heure_de_fin);
		heure_fin.setOnClickListener(this);
		
		date_de_la_commande=(TextView)view.findViewById(R.id.tv_date_de_la_commande);
		date_de_la_commande.setOnClickListener(this);
		
		numero_de_commande=(EditText)view.findViewById(R.id.ed_numero_de_commande);
		
		sp_status=(Spinner)view.findViewById(R.id.sp_status);
		ArrayAdapter<String> dataAdapter1 = new ArrayAdapter<String>(getActivity(), R.layout.spinner_text_orange, ar_status);
        dataAdapter1.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line); 
        sp_status.setAdapter(dataAdapter1);
        sp_status.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long id) {
				if(position==7||position==8||position==9){
					status_comments.setVisibility(View.VISIBLE);
					ll_rdv_more.setVisibility(View.VISIBLE);
					ll_commande_more.setVisibility(View.GONE);
					post_flag=1;
				}else if(position==12){
					ll_rdv_more.setVisibility(View.GONE);
					status_comments.setVisibility(View.VISIBLE);
					ll_commande_more.setVisibility(View.VISIBLE);
					post_flag=2;
				}else{
					status_comments.setVisibility(View.VISIBLE);
					ll_rdv_more.setVisibility(View.GONE);
					ll_commande_more.setVisibility(View.GONE);
					post_flag=0;
				}
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				// TODO Auto-generated method stub
				
			}
		});
        
        // Getting the current location
     	LocationManager lm = (LocationManager)getActivity().getSystemService(Context.LOCATION_SERVICE);
      	lm.requestLocationUpdates(provider, 1000L, 1.0f, this);
      	location = lm.getLastKnownLocation(provider);
      		
      	h=new Handler();
      	r=new Runnable() {
     			
     			@Override		
     			public void run() {
     					if(location!=null){							
     							current_lat=location.getLatitude();
     							current_lon=location.getLongitude();
     							System.out.println("Current lat is: "+current_lat+" Current lon is: "+current_lon);
     							
     					}else{
     							h.postDelayed(r, 2000);
     					}	
     			};
      	};
     	h.post(r);
		
		return view;
	}

	@Override
	public void onClick(View v) {
		int id = v.getId();
		if (id == R.id.tv_back) {
			getActivity().getSupportFragmentManager().beginTransaction().remove(this).commit();
		} else if (id == R.id.tv_cancel) {
			AlertDetailsRDV al=new AlertDetailsRDV();
			Bundle b=new Bundle();
			b.putString("is_alertdetails_via_agenda","alert_listing");
			al.setArguments(b);
			getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.content_frame1, al, "alert_details_rdv").commit();
		} else if (id == R.id.tv_date_rendezvous) {
			DialogFragment newFragment = new DatePickerFragment(date_rendezvous,"yyyy-MM-dd", true);
			newFragment.show(getActivity().getSupportFragmentManager(), "datePicker");
		} else if (id == R.id.tv_heure_de_debut) {
			DialogFragment newFragment1 = new TimePickerFragment(heure_debut,"HH:mm");
			newFragment1.show(getActivity().getSupportFragmentManager(), "timePicker");
		} else if (id == R.id.tv_heure_de_fin) {
			DialogFragment newFragment2 = new TimePickerFragment(heure_fin,"HH:mm");
			newFragment2.show(getActivity().getSupportFragmentManager(), "timePicker");
		} else if (id == R.id.tv_date_de_la_commande) {
			DialogFragment newFragment3 = new DatePickerFragment(date_de_la_commande,"yyyy-MM-dd", true);
			newFragment3.show(getActivity().getSupportFragmentManager(), "datePicker");
		} else if (id == R.id.tv_valider) {
			if(post_flag==1){
				if(/*status_comments.getText().toString().length()==0||*/date_rendezvous.getText().toString().length()==0||heure_debut.getText().toString().length()==0||heure_fin.getText().toString().length()==0){
					new AlertDialog.Builder(getActivity())
					.setMessage("Veuillez saisir toutes les informations")
					.setPositiveButton("OK", new DialogInterface.OnClickListener() {
						
						public void onClick(DialogInterface dialog, int id) {							
										
						}
					}).setIcon(android.R.drawable.stat_notify_error).show();
				}else {
					new PostStatus().execute("");
				}
			}else if(post_flag==2){
				if(/*status_comments.getText().toString().length()==0||*/numero_de_commande.getText().toString().length()==0||date_de_la_commande.getText().toString().length()==0){
					new AlertDialog.Builder(getActivity())
					.setMessage("Veuillez saisir toutes les informations")
					.setPositiveButton("OK", new DialogInterface.OnClickListener() {
						
						public void onClick(DialogInterface dialog, int id) {							
										
						}
					}).setIcon(android.R.drawable.stat_notify_error).show();
				}else {
					new PostStatus().execute("");
				}
			}else{
//				if(status_comments.getText().toString().length()==0){
//					new AlertDialog.Builder(getActivity())
//					.setMessage("Veuillez saisir toutes les informations")
//					.setPositiveButton("OK", new DialogInterface.OnClickListener() {
//						
//						public void onClick(DialogInterface dialog, int id) {							
//										
//						}
//					}).setIcon(android.R.drawable.stat_notify_error).show();
//				}else {
//					
//				}
				new PostStatus().execute("");
			}
		}
	}
	
	private class PostStatus extends AsyncTask<String,Void,Void> {
		ProgressDialog pd;
		@SuppressWarnings("unused")
		int status_flag,total_alert,new_alert,non_statues,no_projets_concernes,
			rendez_confirms,no_photos,no_ticket,no_changement,no_sav_du_jour;
		
		
		@Override
		public void onPreExecute(){
			pd=new ProgressDialog(getActivity());
			pd.setMessage(getResources().getString(R.string.PLEASE_WAIT));
			pd.setCanceledOnTouchOutside(false);
			pd.show();
		}

		@Override
		protected Void doInBackground(String... params1) {								
			try {
				@SuppressWarnings("unused")
				String reply;
				
				if(post_flag==1){
					List <NameValuePair> nvps = new ArrayList <NameValuePair>();
					nvps.add(new BasicNameValuePair("bon_commande", ""));
					nvps.add(new BasicNameValuePair("date_commande",""));
					nvps.add(new BasicNameValuePair("client_id", client_id));
					nvps.add(new BasicNameValuePair("start_date", date_rendezvous.getText().toString() ));
					nvps.add(new BasicNameValuePair("end_time", heure_fin.getText().toString()));
					nvps.add(new BasicNameValuePair("start_time", heure_debut.getText().toString()));
					nvps.add(new BasicNameValuePair("event_id", event_id));
					nvps.add(new BasicNameValuePair("statut_rdv", ""+(sp_status.getSelectedItemPosition()+1)));
					nvps.add(new BasicNameValuePair("commentaire", status_comments.getText().toString()));
					nvps.add(new BasicNameValuePair("user_latitude", ""+current_lat));
					nvps.add(new BasicNameValuePair("user_longitude", ""+current_lon));

					reply=new RESTful(getActivity()).postServer(getResources().getString(R.string.api_end_point)+"agenda", nvps);
				
				} 
				if(post_flag==2){
					List <NameValuePair> nvps = new ArrayList <NameValuePair>();
					nvps.add(new BasicNameValuePair("bon_commande", numero_de_commande.getText().toString()));
					nvps.add(new BasicNameValuePair("date_commande", date_de_la_commande.getText().toString()));
					nvps.add(new BasicNameValuePair("client_id", client_id));
					nvps.add(new BasicNameValuePair("start_date", ""));
					nvps.add(new BasicNameValuePair("end_time", ""));
					nvps.add(new BasicNameValuePair("start_time", ""));
					nvps.add(new BasicNameValuePair("event_id", event_id));
					nvps.add(new BasicNameValuePair("statut_rdv", ""+(sp_status.getSelectedItemPosition()+1)));
					nvps.add(new BasicNameValuePair("commentaire", status_comments.getText().toString()));
					nvps.add(new BasicNameValuePair("user_latitude", ""+current_lat));
					nvps.add(new BasicNameValuePair("user_longitude", ""+current_lon));

					reply=new RESTful(getActivity()).postServer(getResources().getString(R.string.api_end_point)+"agenda", nvps);
				
				}else{
					List <NameValuePair> nvps = new ArrayList <NameValuePair>();
					nvps.add(new BasicNameValuePair("bon_commande", ""));
					nvps.add(new BasicNameValuePair("date_commande", ""));
					nvps.add(new BasicNameValuePair("client_id", client_id));
					nvps.add(new BasicNameValuePair("start_date", ""));
					nvps.add(new BasicNameValuePair("end_time", ""));
					nvps.add(new BasicNameValuePair("start_time", ""));
					nvps.add(new BasicNameValuePair("event_id", event_id));
					nvps.add(new BasicNameValuePair("statut_rdv", ""+(sp_status.getSelectedItemPosition()+1)));
					nvps.add(new BasicNameValuePair("commentaire", status_comments.getText().toString()));
					nvps.add(new BasicNameValuePair("user_latitude", ""+current_lat));
					nvps.add(new BasicNameValuePair("user_longitude", ""+current_lon));


					reply=new RESTful(getActivity()).postServer(getResources().getString(R.string.api_end_point)+"agenda", nvps);
				
				}
				status_flag=2;
			} catch (IOException e) {
				if(e.getMessage().equals("201")){
					status_flag=7;
				}else{
					e.printStackTrace();
					status_flag=5;
				}
				
			} catch (NullPointerException e) {
				e.printStackTrace();
				status_flag=6;
			}										 				 	 			 	
			return null;
		}

		@Override
		public void onPostExecute(Void m_adapter){               	
			pd.dismiss();
			if(status_flag==2){
				Toast.makeText(getActivity(), "Rendez-vous modifié", Toast.LENGTH_SHORT).show();
				
				AlertListing al=new AlertListing();
				Bundle b=new Bundle();
				b.putInt("type",3);
				al.setArguments(b);
				getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.content_frame1, al, "alert_listing").commit();
				//getActivity().getSupportFragmentManager().beginTransaction().remove(AlertDetailsStatuer.this).commit();
//				name.setText(AlertListing.cdd.name+" "+AlertListing.cdd.first_name);
//				address.setText(AlertListing.cdd.address);
//				etape.setText(chdd.etape);
//				par.setText(chdd.par);
//				commentaire.setText(chdd.commentaire);
//				date.setText(chdd.date);
				
		 	}else if(status_flag==3){
		 		new AlertDialog.Builder(getActivity())
				.setMessage("Erreur de connexion. Veuillez réessayer à nouveau")
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					
					public void onClick(DialogInterface dialog, int id) {							
									
					}
				}).setIcon(android.R.drawable.stat_notify_error).show();
		 	}else if(status_flag==4){
		 		new AlertDialog.Builder(getActivity())
				.setMessage("Erreur du serveur. Veuillez réessayer à nouveau")
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					
					public void onClick(DialogInterface dialog, int id) {							
									
					}
				}).setIcon(android.R.drawable.stat_notify_error).show();
		 	}else if(status_flag==5){
		 		new AlertDialog.Builder(getActivity())
				.setMessage("Erreur serveur. Veuillez réessayer à nouveau.")
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					
					public void onClick(DialogInterface dialog, int id) {							
									
					}
				}).setIcon(android.R.drawable.stat_notify_error).show();
		 	}else if(status_flag==6){
		 		new AlertDialog.Builder(getActivity())
				.setMessage("Aucune donnée reçue . Veuillez réessayer à nouveau.")
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					
					public void onClick(DialogInterface dialog, int id) {							
									
					}
				}).setIcon(android.R.drawable.stat_notify_error).show();
		 	}else if(status_flag==7){
		 		Toast.makeText(getActivity(), "Rendez-vous créé", Toast.LENGTH_SHORT).show();
				
				AlertListing al=new AlertListing();
				Bundle b=new Bundle();
				b.putInt("type",3);
				al.setArguments(b);
				getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.content_frame1, al, "alert_listing").commit();
		 	}
		}
	}

	@Override
	public void onLocationChanged(Location location) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onProviderEnabled(String provider) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onProviderDisabled(String provider) {
		// TODO Auto-generated method stub
		
	}
}
