//  iCollMobile
//
//  Created by Khaled Noui
//  Copyright (c) 2014 digiwabi. All rights reserved.
//

package com.kb.icoll;

import java.io.IOException;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.kb.icoll.ClientProjetSavReclamation.SAVReclamationData;
import com.kb.icoll.utils.RESTful;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

public class AlertDocumentsDetails extends Fragment implements OnClickListener {

	TextView name,address,date_de_commande,document_manquant;
	String projet_id,client_id;
	ListView lv_document;
	ArrayList<String> al_document;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.alert_documents_details, container,false);
		
		name=(TextView)view.findViewById(R.id.tv_name);
		name.setText(AlertListing.dnrd.first_name+" "+AlertListing.dnrd.name);
		
		address=(TextView)view.findViewById(R.id.tv_address);
		address.setText(AlertListing.dnrd.address);
		
		date_de_commande=(TextView)view.findViewById(R.id.tv_date);
		date_de_commande.setText(AlertListing.dnrd.commande_date);
		
		document_manquant=(TextView)view.findViewById(R.id.tv_document_manquant);  // tag missing
		//document_manquant.setText(AlertListing.dnrd.commande_date);
		
		TextView voir_la_fiche=(TextView)view.findViewById(R.id.tv_voir_la_fiche);
		voir_la_fiche.setOnClickListener(this);
		
		TextView back=(TextView)view.findViewById(R.id.tv_back);
		back.setOnClickListener(this);
		
		LinearLayout ll_parent=(LinearLayout)view.findViewById(R.id.ll_parent);
		ll_parent.setOnClickListener(this);
		
		lv_document=(ListView)view.findViewById(R.id.lv_document);
		
		client_id=AlertListing.dnrd.client_id;
		projet_id=AlertListing.dnrd.projet_id;
		
		new GetDocumentDetails().execute("");
		
		return view;
	}

	@Override
	public void onClick(View v) {
		int id = v.getId();
		if (id == R.id.tv_voir_la_fiche) {
			getActivity().getSupportFragmentManager().beginTransaction().remove(this).commit();
			ClientDetails cd=new ClientDetails();
			Bundle b=new Bundle();
			b.putString("client_id", client_id);
			b.putString("projet_id", projet_id);
			b.putString("is_from_alert", "alert");
			cd.setArguments(b);
			getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, cd, "client_details").commit();
		} else if (id == R.id.tv_back) {
			getActivity().getSupportFragmentManager().beginTransaction().remove(this).commit();
		}
	}
	
	
	private class GetDocumentDetails extends AsyncTask<String,Void,Void> {
		ProgressDialog pd;
		int status_flag;
		
		
		@Override
		public void onPreExecute(){
			pd=new ProgressDialog(getActivity());
			pd.setMessage(getResources().getString(R.string.PLEASE_WAIT));
			pd.setCanceledOnTouchOutside(false);
			pd.show();
		}

		@Override
		protected Void doInBackground(String... params1) {								
			try {
				String reply=new RESTful(getActivity()).queryServer(getResources().getString(R.string.api_end_point)+"alerte?client_id="+client_id+"&type_alerte_id=1", true);
		        System.out.println("json_cnst-1: " + reply);

		        reply=reply.replace(getResources().getString(R.string.json_cnst), "");
		        System.out.println("json_cnst-2: " + reply);

				JSONArray data = new JSONArray(reply);
				al_document=new ArrayList<String>();
				for(int i=0;i<data.length();i++){
					JSONObject jo=data.getJSONObject(i);					
					String doc=jo.getString("Document manquant");
					
					al_document.add(doc);
					
				}
				System.out.println("Size of sav array: "+al_document.size());
				status_flag=2;
			} catch (IOException e) {
				if(e.getMessage().equals("403")){
					status_flag=5;
				}else{
					status_flag=3;
				}
				e.printStackTrace();
			} catch (JSONException e) {
				status_flag=4;
			}catch (NullPointerException e) {
				status_flag=6;
			}										 				 	 			 	
			return null;
		}

		@Override
		public void onPostExecute(Void m_adapter){               	
			pd.dismiss();
			if(status_flag==2){
				
				implementListView(al_document);
				
		 	}else if(status_flag==3){
		 		new AlertDialog.Builder(getActivity())
				.setMessage("Erreur de connexion. Veuillez réessayer à nouveau")
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					
					public void onClick(DialogInterface dialog, int id) {							
									
					}
				}).setIcon(android.R.drawable.stat_notify_error).show();
		 	}else if(status_flag==4){
		 		new AlertDialog.Builder(getActivity())
				.setMessage("Erreur du serveur. Veuillez réessayer à nouveau")
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					
					public void onClick(DialogInterface dialog, int id) {							
									
					}
				}).setIcon(android.R.drawable.stat_notify_error).show();
		 	}else if(status_flag==5){
		 		new AlertDialog.Builder(getActivity())
				.setMessage("Paramètres de connexion incorrects")
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					
					public void onClick(DialogInterface dialog, int id) {							
									
					}
				}).setIcon(android.R.drawable.stat_notify_error).show();
		 	}else if(status_flag==6){
		 		new AlertDialog.Builder(getActivity())
				.setMessage("Aucune donnée reçue . S'il vous plait réessayer plus tard.")
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					
					public void onClick(DialogInterface dialog, int id) {							
									
					}
				}).setIcon(android.R.drawable.stat_notify_error).show();
		 	}
		}
	}
	
	private void implementListView(ArrayList<String> al) {
		ArrayAdapter<String> ad = new MyAdapter(getActivity(),
				R.layout.alert_documents_details, al);	
		lv_document.setAdapter(ad);
		ad.notifyDataSetChanged();
		lv_document.setTextFilterEnabled(true);
	}
	
	private class MyAdapter extends ArrayAdapter<String>{
		ArrayList<String> my_al;
		
		public MyAdapter(Context context, int textViewResourceId, ArrayList<String> al) {
			super(context, textViewResourceId, al);	
			my_al=al;
		}
				
		@Override
		public View getView(final int position, View convertView, ViewGroup parent) {
			View v = convertView;
			
			if (v == null) {				
				LayoutInflater vi = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);				
		        v = vi.inflate(R.layout.alert_document_cell, null);		        			        				
			}
			
			TextView document_manquant=(TextView)v.findViewById(R.id.tv_document_manquant);
			document_manquant.setText(my_al.get(position)); 
			
			
			v.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					
				}
			});
			return v;
		}
	}
}
