//  iCollMobile
//
//  Created by Khaled Noui
//  Copyright (c) 2014 digiwabi. All rights reserved.
//

package com.kb.icoll;

import java.io.IOException;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.kb.icoll.Prospects.ProspectData;
import com.kb.icoll.utils.RESTful;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class AlertListing extends Fragment implements OnClickListener{
	ListView alert_list;
	public static RDVNonStatusData rsd;
	public static DocumentsNonRescueData dnrd;
	public static ChangementData cdd;
	public static SAVdujourData sdjd;
	public static ArrayList<RDVNonStatusData> al_rdv_non_status;

	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.alert_listing, container,false);
		
		alert_list=(ListView)view.findViewById(R.id.lv_alert_list);
		TextView back=(TextView)view.findViewById(R.id.tv_back);
		back.setOnClickListener(this);
		
		TextView header=(TextView)view.findViewById(R.id.tv_header);
		if(getArguments().getInt("type")==3){
			header.setText("RDV non statués");
		}else if(getArguments().getInt("type")==1){
			header.setText("Documents non reçus");
		}else if(getArguments().getInt("type")==2){
			header.setText("Photos non insérées");
		}else if(getArguments().getInt("type")==4){
			header.setText("RDV confirmés du jour");
		}else if(getArguments().getInt("type")==7){
			header.setText("Changements d'état du jour");
		}else if(getArguments().getInt("type")==6){
			header.setText("SAV du jour");
		} else if(getArguments().getInt("type")==5){
			header.setText("Tickets en attende\nde réponse");
		}
		
		
		if(new RESTful(getActivity()).checkNetworkConnection()){
			new GetAlertListing().execute("");
		}
		
		return view;
	}
	
	private class GetAlertListing extends AsyncTask<String,Void,Void> {
		ProgressDialog pd;
		@SuppressWarnings("unused")
		int status_flag,total_alert,new_alert,non_statues,no_projets_concernes,
			rendez_confirms,no_photos,no_ticket,no_changement,no_sav_du_jour;
		
		ArrayList<DocumentsNonRescueData> al_documents_non_rescue;
		ArrayList<PhotoInsererData> al_photo_inserer;
		ArrayList<ChangementData> al_changement;
		ArrayList<SAVdujourData> al_sav_du_jour;
		ArrayList<TicketsData> al_tickets_data;

		
		@Override
		public void onPreExecute(){
			pd=new ProgressDialog(getActivity());
			pd.setMessage(getResources().getString(R.string.PLEASE_WAIT));
			pd.setCanceledOnTouchOutside(false); 
			pd.show();
		}

		@Override
		protected Void doInBackground(String... params1) {								
			try {
				String reply=new RESTful(getActivity()).queryServer(getResources().getString(R.string.api_end_point)+"alerte?type_alerte_id="+getArguments().getInt("type"), true);
		        System.out.println("json_cnst-1: " + reply);

				reply=reply.replace(getResources().getString(R.string.json_cnst), ""); 
		        System.out.println("json_cnst-2: " + reply);
				JSONArray data = new JSONArray(reply);
				if(getArguments().getInt("type")==3||getArguments().getInt("type")==4){
					String reply1=new RESTful(getActivity()).queryServer(getResources().getString(R.string.api_end_point)+"prospect", true);
			        System.out.println("json_cnst-3: " + reply);

					reply1=reply1.replace(getResources().getString(R.string.json_cnst), "");
			        System.out.println("json_cnst-4: " + reply);

					JSONArray data1 = new JSONArray(reply1);
					Prospects.al=new ArrayList<ProspectData>();
					for(int i=0;i<data1.length();i++){
						JSONObject jo=data1.getJSONObject(i);
						ProspectData cd=new Prospects().new ProspectData();
						cd.first_name=jo.getString("prenom");
						cd.name=jo.getString("nom");
						cd.postal_code=jo.getString("cp");
						cd.ville=jo.getString("ville");
						cd.statut=jo.getString("statut");
						cd.id=jo.getString("id");
						cd.adresse=jo.getString("adresse");
						cd.last_rdv_confirmed_start_date=jo.getString("last_rdv_confirmed_start_date");
						cd.last_rdv_confirmed_statut=jo.getString("last_rdv_confirmed_statut");
						cd.tel_fixe=jo.getString("tel_fixe");
						try{
							cd.prospect_latitude=jo.getDouble("latitude");
							cd.prospect_longitude=jo.getDouble("longitude");
						}catch(JSONException e){
							cd.prospect_latitude=0.0;
							cd.prospect_longitude=0.0;
						}						
						Prospects.al.add(cd);
					}
				}
				if(getArguments().getInt("type")==3){
					al_rdv_non_status=new ArrayList<RDVNonStatusData>();
					for(int i=0;i<data.length();i++){
						JSONObject jo=data.getJSONObject(i);
						RDVNonStatusData rs=new RDVNonStatusData();
						rs.first_name=jo.getString("prenom");
						rs.name=jo.getString("nom");
						rs.event_id=jo.getString("event_id");
						rs.client_id=jo.getString("client_id");
						rs.address=jo.getString("cp")+" "+jo.getString("ville");
						rs.adresse=jo.getString("adresse");
						rs.start_date=jo.getString("start_date");
						rs.start_time=jo.getString("start_time");
						rs.end_time=jo.getString("end_time");
						rs.telephone=jo.getString("tel_fixe");
						rs.description=jo.getString("description");
						rs.statut=jo.getString("statut_rdv_lb");
						rs.commentaire=jo.getString("commentaire");
						try{
							rs.latitude=jo.getDouble("latitude");
							rs.longitude=jo.getDouble("longitude");
						}catch(JSONException e){
							rs.latitude=0.0;
							rs.longitude=0.0;
						}
						
						al_rdv_non_status.add(rs);
					}
				}else if(getArguments().getInt("type")==1){		
					al_documents_non_rescue=new ArrayList<DocumentsNonRescueData>();
					for(int i=0;i<data.length();i++){
						JSONObject jo=data.getJSONObject(i);
						DocumentsNonRescueData rs=new DocumentsNonRescueData();
						rs.first_name=jo.getString("prenom");
						rs.name=jo.getString("nom");
						rs.address=jo.getString("code_postal")+" "+jo.getString("ville");
						rs.nombre=jo.getString("Nombre");
						rs.commande_date=jo.getString("date_commande");
						rs.client_id=jo.getString("client_id");
						rs.projet_id=jo.getString("projet_id");
						al_documents_non_rescue.add(rs);
					}
				}else if(getArguments().getInt("type")==2){		
					al_photo_inserer=new ArrayList<PhotoInsererData>();
					for(int i=0;i<data.length();i++){
						JSONObject jo=data.getJSONObject(i);
						PhotoInsererData rs=new PhotoInsererData();
						rs.first_name=jo.getString("prenom");
						rs.name=jo.getString("nom");
						rs.address=jo.getString("code_postal")+" "+jo.getString("ville");
						rs.photo4=jo.getString("Photos dp4");
						rs.photo7=jo.getString("Photos dp7");
						rs.client_id=jo.getString("client_id");
						al_photo_inserer.add(rs);
					}
				}else if(getArguments().getInt("type")==4){
					al_rdv_non_status=new ArrayList<RDVNonStatusData>();
					for(int i=0;i<data.length();i++){
						JSONObject jo=data.getJSONObject(i);
						RDVNonStatusData rcd=new RDVNonStatusData();
						rcd.event_id=jo.getString("event_id");
						rcd.client_id=jo.getString("client_id");
						rcd.first_name=jo.getString("prenom");
						rcd.name=jo.getString("nom");
						rcd.address=jo.getString("cp")+" "+jo.getString("ville");
						rcd.adresse=jo.getString("adresse");
						rcd.start_date=jo.getString("start_date");
						rcd.start_time=jo.getString("start_time");
						rcd.end_time=jo.getString("end_time");
						rcd.telephone=jo.getString("tel_fixe");
						rcd.description=jo.getString("description");
						rcd.statut=jo.getString("statut_rdv_lb");
						rcd.commentaire=jo.getString("commentaire");
						try{
							rcd.latitude=jo.getDouble("latitude");
							rcd.longitude=jo.getDouble("longitude");
						}catch(JSONException e){
							rcd.latitude=0.0;
							rcd.longitude=0.0;
						}
						al_rdv_non_status.add(rcd);
					}
				}else if(getArguments().getInt("type")==7){
					al_changement=new ArrayList<ChangementData>();
					for(int i=0;i<data.length();i++){
						JSONObject jo=data.getJSONObject(i);
						ChangementData cd=new ChangementData();
						cd.first_name=jo.getString("prenom");
						cd.name=jo.getString("nom");
						cd.address=jo.getString("code_postal")+" "+jo.getString("ville");
						cd.nombre=jo.getString("Nombre");
						cd.client_id=jo.getString("client_id");
						cd.commande_id=jo.getString("commande_id");
						
						al_changement.add(cd);
					}
				}else if(getArguments().getInt("type")==6){
					al_sav_du_jour=new ArrayList<SAVdujourData>();
					for(int i=0;i<data.length();i++){
						JSONObject jo=data.getJSONObject(i);
						SAVdujourData sd=new SAVdujourData();
						sd.first_name=jo.getString("prenom");
						sd.name=jo.getString("nom");
						sd.address=jo.getString("cp")+" "+jo.getString("ville");
						sd.client_id=jo.getString("client_id");
						sd.commande_id=jo.getString("commande_id");
						al_sav_du_jour.add(sd);
					}
				}else if(getArguments().getInt("type")==5){
					al_tickets_data=new ArrayList<TicketsData>();
					for(int i=0;i<data.length();i++){
					JSONObject jo=data.getJSONObject(i);
					TicketsData tid=new TicketsData();
					tid.client_id=jo.getString("client_id");
					tid.postal_code=jo.getString("code_postal");
					tid.ville=jo.getString("ville");
					tid.name=jo.getString("nom");
					tid.first_name=jo.getString("prenom");
					tid.no_ticket=jo.getString("Nombre de tickets sans réponse");
					al_tickets_data.add(tid);
					}
				}
				
				
				status_flag=2;
			} catch (IOException e) {
				if(e.getMessage().equals("403")){
					status_flag=5;
				}else{
					status_flag=3;
				}
				e.printStackTrace();
			} catch (JSONException e) {
				status_flag=4;
			}catch (NullPointerException e) {
				status_flag=6;
			}										 				 	 			 	
			return null;
		}

		@Override
		public void onPostExecute(Void m_adapter){               	
			pd.dismiss();
			if(status_flag==2){
				if(getArguments().getInt("type")==3){
					implementListView3(al_rdv_non_status);
				}else if(getArguments().getInt("type")==1){
					implementListView1(al_documents_non_rescue);
				}else if(getArguments().getInt("type")==2){
					implementListView2(al_photo_inserer);
				}else if(getArguments().getInt("type")==4){
					implementListView3(al_rdv_non_status);
				}else if(getArguments().getInt("type")==7){
					implementListView7(al_changement);
				}else if(getArguments().getInt("type")==6){
					implementListView6(al_sav_du_jour);
				}else if(getArguments().getInt("type")==5){
					implementListView5(al_tickets_data);
				}
		
		 	}else if(status_flag==3){
		 		new AlertDialog.Builder(getActivity())
				.setMessage("Erreur de connexion. Veuillez réessayer à nouveau")
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					
					public void onClick(DialogInterface dialog, int id) {							
									
					}
				}).setIcon(android.R.drawable.stat_notify_error).show();
		 	}else if(status_flag==4){
		 		new AlertDialog.Builder(getActivity())
				.setMessage("Erreur du serveur. Veuillez réessayer à nouveau")
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					
					public void onClick(DialogInterface dialog, int id) {							
									
					}
				}).setIcon(android.R.drawable.stat_notify_error).show();
		 	}else if(status_flag==5){
		 		new AlertDialog.Builder(getActivity())
				.setMessage("Paramètres de connexion incorrects")
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					
					public void onClick(DialogInterface dialog, int id) {							
									
					}
				}).setIcon(android.R.drawable.stat_notify_error).show();
		 	}else if(status_flag==6){
		 		new AlertDialog.Builder(getActivity())
				.setMessage("Aucune donnée reçue . S'il vous plait réessayer plus tard.")
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					
					public void onClick(DialogInterface dialog, int id) {							
									
					}
				}).setIcon(android.R.drawable.stat_notify_error).show();
		 	}
		}
	}
	
	private void implementListView2(ArrayList<PhotoInsererData> al_photos_inserer) {
		ArrayAdapter<PhotoInsererData> ad = new MyAdapter2(getActivity(),
				R.layout.alert_listing_cell, al_photos_inserer);	
		alert_list.setAdapter(ad);
		ad.notifyDataSetChanged();
		alert_list.setTextFilterEnabled(true);
	}
	
	private class MyAdapter2 extends ArrayAdapter<PhotoInsererData>{
		ArrayList<PhotoInsererData> my_al;
		
		public MyAdapter2(Context context, int textViewResourceId, ArrayList<PhotoInsererData> al) {
			super(context, textViewResourceId, al);	
			my_al=al;
		}
				
		@Override
		public View getView(final int position, View convertView, ViewGroup parent) {
			View v = convertView;
			
			if (v == null) {				
				LayoutInflater vi = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);				
		        v = vi.inflate(R.layout.alert_listing_cell, null);		        			        				
			}
			
			TextView name=(TextView)v.findViewById(R.id.tv_name);
			name.setText(my_al.get(position).name+" "+my_al.get(position).first_name); 
			
			TextView address=(TextView)v.findViewById(R.id.tv_address);
			address.setText(my_al.get(position).address); 
			
			TextView nombre=(TextView)v.findViewById(R.id.tv_date);
			nombre.setText("Photos dp4 : "+my_al.get(position).photo4);
			
			TextView date=(TextView)v.findViewById(R.id.tv_time);
			date.setText("Photos dp7 : "+my_al.get(position).photo7);
			
			v.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					//getActivity().getSupportFragmentManager().beginTransaction().remove(this).commit();
					ClientDetails cd=new ClientDetails();
					Bundle b=new Bundle();
					b.putString("client_id", my_al.get(position).client_id);
					b.putString("is_from_alert", "photo");
					//b.putString("projet_id", projet_id);
					cd.setArguments(b);
					getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, cd, "client_details").commit();
				}
			});
			return v;
		}
	}
	
	private void implementListView1(ArrayList<DocumentsNonRescueData> al_documents_non_rescue) {
		ArrayAdapter<DocumentsNonRescueData> ad = new MyAdapter1(getActivity(),
				R.layout.alert_listing_cell, al_documents_non_rescue);	
		alert_list.setAdapter(ad);
		ad.notifyDataSetChanged();
		alert_list.setTextFilterEnabled(true);
	}
	
	private class MyAdapter1 extends ArrayAdapter<DocumentsNonRescueData>{
		ArrayList<DocumentsNonRescueData> my_al;
		
		public MyAdapter1(Context context, int textViewResourceId, ArrayList<DocumentsNonRescueData> al) {
			super(context, textViewResourceId, al);	
			my_al=al;
		}
				
		@Override
		public View getView(final int position, View convertView, ViewGroup parent) {
			View v = convertView;
			
			if (v == null) {				
				LayoutInflater vi = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);				
		        v = vi.inflate(R.layout.alert_listing_cell, null);		        			        				
			}
			
			TextView name=(TextView)v.findViewById(R.id.tv_name);
			name.setText(my_al.get(position).name+" "+my_al.get(position).first_name); 
			
			TextView address=(TextView)v.findViewById(R.id.tv_address);
			address.setText(my_al.get(position).address); 
			
			TextView nombre=(TextView)v.findViewById(R.id.tv_date);
			nombre.setText("Nombre de documents en attente : "+my_al.get(position).nombre);
			
			TextView date=(TextView)v.findViewById(R.id.tv_time);
			date.setText("Date de commande : "+my_al.get(position).commande_date);
			
			v.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					dnrd=my_al.get(position);
					getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.content_frame1, new AlertDocumentsDetails(), "alert_documents_details").commit();
				}
			});
			return v;
		}
	}

	// RENDEZVOUS-Confirmes aujourd'hui
	private void implementListView3(ArrayList<RDVNonStatusData> al_ed) {
		ArrayAdapter<RDVNonStatusData> ad = new MyAdapter3(getActivity(),
				R.layout.alert_listing_cell, al_ed);	
		alert_list.setAdapter(ad);
		ad.notifyDataSetChanged();
		alert_list.setTextFilterEnabled(true);
	}
	
	private class MyAdapter3 extends ArrayAdapter<RDVNonStatusData>{
		ArrayList<RDVNonStatusData> my_al;
		
		public MyAdapter3(Context context, int textViewResourceId, ArrayList<RDVNonStatusData> al) {
			super(context, textViewResourceId, al);	
			my_al=al;
		}
				
		@Override
		public View getView(final int position, View convertView, ViewGroup parent) {
			View v = convertView;
			
			if (v == null) {				
				LayoutInflater vi = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);				
		        v = vi.inflate(R.layout.alert_listing_cell, null);		        			        				
			}
			
			TextView name=(TextView)v.findViewById(R.id.tv_name);
			name.setText(my_al.get(position).first_name+" "+my_al.get(position).name); 
			
			TextView address=(TextView)v.findViewById(R.id.tv_address);
			address.setText(my_al.get(position).address); 
			
			TextView start_date=(TextView)v.findViewById(R.id.tv_date);
			start_date.setText(my_al.get(position).start_date);
			
			TextView time=(TextView)v.findViewById(R.id.tv_time);
			time.setText(my_al.get(position).start_time+" à "+my_al.get(position).end_time);
			
			v.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					rsd=my_al.get(position);
					AlertDetailsRDV al=new AlertDetailsRDV();
					Bundle b=new Bundle();
					//b.putBoolean("is_alertdetails_via_agenda", false);
					b.putString("is_alertdetails_via_agenda","alert_listing");
					al.setArguments(b);
					getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.content_frame1, al, "alert_details_rdv").commit();
				}
			});
			return v;
		}
	}
	
	private void implementListView7(ArrayList<ChangementData> al_cd) {
		ArrayAdapter<ChangementData> ad = new MyAdapter7(getActivity(),
				R.layout.alert_listing_cell, al_cd);	
		alert_list.setAdapter(ad);
		ad.notifyDataSetChanged();
		alert_list.setTextFilterEnabled(true);
	}
	
	private class MyAdapter7 extends ArrayAdapter<ChangementData>{
		ArrayList<ChangementData> my_al;
		
		public MyAdapter7(Context context, int textViewResourceId, ArrayList<ChangementData> al) {
			super(context, textViewResourceId, al);	
			my_al=al;
		}
				
		@Override
		public View getView(final int position, View convertView, ViewGroup parent) {
			View v = convertView;
			
			if (v == null) {				
				LayoutInflater vi = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);				
		        v = vi.inflate(R.layout.alert_listing_cell, null);		        			        				
			}
			
			TextView name=(TextView)v.findViewById(R.id.tv_name);
			name.setText(my_al.get(position).first_name+" "+my_al.get(position).name); 
			
			TextView address=(TextView)v.findViewById(R.id.tv_address);
			address.setText(my_al.get(position).address); 
			
			TextView start_date=(TextView)v.findViewById(R.id.tv_date);
			start_date.setText("Nombre : "+my_al.get(position).nombre);
			
			TextView time=(TextView)v.findViewById(R.id.tv_time);
			time.setVisibility(View.INVISIBLE);
			
			v.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					cdd=my_al.get(position);
					getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.content_frame1, new AlertChangementDetails(), "alert_changement_details").commit();
				}
			});
			return v;
		}
	}
	
	//// SAV du jour -> Service Day
	private void implementListView6(ArrayList<SAVdujourData> al_cd) {
		ArrayAdapter<SAVdujourData> ad = new MyAdapter6(getActivity(),
				R.layout.alert_listing_cell, al_cd);	
		alert_list.setAdapter(ad);
		ad.notifyDataSetChanged();
		alert_list.setTextFilterEnabled(true);
	}
	
	private class MyAdapter6 extends ArrayAdapter<SAVdujourData>{
		ArrayList<SAVdujourData> my_al;
		
		public MyAdapter6(Context context, int textViewResourceId, ArrayList<SAVdujourData> al) {
			super(context, textViewResourceId, al);	
			my_al=al;
		}
				
		@Override
		public View getView(final int position, View convertView, ViewGroup parent) {
			View v = convertView;
			
			if (v == null) {				
				LayoutInflater vi = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);				
		        v = vi.inflate(R.layout.alert_listing_cell, null);		        			        				
			}
			
			TextView name=(TextView)v.findViewById(R.id.tv_name);
			name.setText(my_al.get(position).first_name+" "+my_al.get(position).name); 
			
			TextView address=(TextView)v.findViewById(R.id.tv_address);
			address.setText(my_al.get(position).address); 
			
			TextView start_date=(TextView)v.findViewById(R.id.tv_date);
			start_date.setVisibility(View.INVISIBLE);
			
			TextView time=(TextView)v.findViewById(R.id.tv_time);
			time.setVisibility(View.INVISIBLE);
			
			v.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					sdjd=my_al.get(position);
					getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.content_frame1, new AlertSAVdujourDetails(), "alert_sav_du_jour_details").commit();
				}
			});
			return v;
		}
	}


	@Override
	public void onClick(View v) {
		int id = v.getId();
		if (id == R.id.tv_back) {
			getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, new Alerts(), "alerts").commit();
			getActivity().getSupportFragmentManager().beginTransaction().remove(this).commit();
		}
	}
	
	class RDVNonStatusData{
		String first_name,name,start_date,start_time,end_time,address,adresse,telephone,description,statut,commentaire,client_id,event_id;
		double latitude,longitude;
	}
	
	public class DocumentsNonRescueData{
		String first_name,name,commande_date,nombre,address,client_id,projet_id;
	}
	
	private class PhotoInsererData{
		String first_name,name,photo4,photo7,address,client_id;
	}
	
	class ChangementData{
		String first_name,name,commande_id,nombre,address,client_id;
	}
	
	public class SAVdujourData{
		String commande_id,client_id,name,first_name,address;
	}
	
	public class TicketsData{
		String client_id,postal_code,ville,name,first_name,no_ticket;
	}
	
	//SAV du jour -> Tickets
	private void implementListView5(ArrayList<TicketsData> al_cd) {
		ArrayAdapter<TicketsData> ad = new MyAdapter5(getActivity(),
		R.layout.alert_listing_cell, al_cd);
		alert_list.setAdapter(ad);
		ad.notifyDataSetChanged();
		alert_list.setTextFilterEnabled(true);
	}

	private class MyAdapter5 extends ArrayAdapter<TicketsData>{
	ArrayList<TicketsData> my_al;

	public MyAdapter5(Context context, int textViewResourceId, ArrayList<TicketsData> al) {
		super(context, textViewResourceId, al);
		my_al=al;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		View v = convertView;
	
		if (v == null) {
			LayoutInflater vi = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			v = vi.inflate(R.layout.alert_listing_cell, null);
		}
	
		TextView name=(TextView)v.findViewById(R.id.tv_name);
		name.setText(my_al.get(position).first_name+" "+my_al.get(position).name);
	
		TextView address=(TextView)v.findViewById(R.id.tv_address);
		address.setText(my_al.get(position).postal_code+" "+my_al.get(position).ville);
	
		TextView start_date=(TextView)v.findViewById(R.id.tv_date);
		start_date.setText("Nombre de tickets sans réponse: "+my_al.get(position).no_ticket);
	
		TextView time=(TextView)v.findViewById(R.id.tv_time);
		time.setVisibility(View.INVISIBLE);
	
		v.setOnClickListener(new OnClickListener() {
	
		@Override
		public void onClick(View v) {
			ClientDetails cd=new ClientDetails();
			Bundle b=new Bundle();
			b.putString("client_id", my_al.get(position).client_id);
			b.putString("is_from_alert", "ticket");
			cd.setArguments(b);
			getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, cd, "client_details").commit();
		}
		});
		return v;
	}
	}


}
