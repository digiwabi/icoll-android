//  iCollMobile
//
//  Created by Khaled Noui
//  Copyright (c) 2014 digiwabi. All rights reserved.
//

package com.kb.icoll;

import com.kb.icoll.AlertSAVdujourDetails.SAVDuJourDetailsData;
import com.kb.icoll.ClientProjetSavReclamation.SAVReclamationData;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;

public class AlertSAVDetailsInfo extends Fragment implements OnClickListener {
	
	TextView etat,probleme,commentaire,date_d_intervention,horaire_d_intervention,intervenant,commentaire_d_intervenant;
	SAVDuJourDetailsData sd;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.alert_sav_details_info, container,false);
		
		sd=(SAVDuJourDetailsData) getArguments().getSerializable("sav_data");
		
		getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.ll_client_info_container2, new AlertClientInfo(),"alert_client_info").commit();
		
		TextView header=(TextView)view.findViewById(R.id.tv_header);
		header.setText(AlertListing.sdjd.name+" "+AlertListing.sdjd.first_name);
		
		TextView sub_header=(TextView)view.findViewById(R.id.tv_sub_header);
		sub_header.setText(sd.incident_type+" - "+sd.incident_probleme);
		
		TextView back=(TextView)view.findViewById(R.id.tv_back);
		back.setOnClickListener(this); 
		
		etat=(TextView)view.findViewById(R.id.tv_etat);
		probleme=(TextView)view.findViewById(R.id.tv_probleme);
		commentaire=(TextView)view.findViewById(R.id.tv_commentaire);
		date_d_intervention=(TextView)view.findViewById(R.id.tv_date_d_intervention);
		horaire_d_intervention=(TextView)view.findViewById(R.id.tv_horaire_d_intervention);
		intervenant=(TextView)view.findViewById(R.id.tv_intervenant);
		commentaire_d_intervenant=(TextView)view.findViewById(R.id.tv_commentaire_de_intervenant);
		
		if(sd.incident_etat.length()==0){
			etat.setText("non renseigné");
		}else{
			etat.setText(sd.incident_etat);
		}
		if(sd.incident_probleme.length()==0){
			probleme.setText("non renseigné");
		}else{
			probleme.setText(sd.incident_probleme);
		}
		if(sd.incident_commentaire.length()==0){
			commentaire.setText("non renseigné");
		}else{
			commentaire.setText(sd.incident_commentaire);
		}
		if(sd.incident_date_intervention.length()==0){
			date_d_intervention.setText("non renseigné");
		}else{
			date_d_intervention.setText(sd.incident_date_intervention);
		}
		if(sd.incident_heure_intervention.length()==0){
			horaire_d_intervention.setText("non renseigné");
		}else{
			horaire_d_intervention.setText(sd.incident_heure_intervention);
		}
		if(sd.incident_intervenant.length()==0){
			intervenant.setText("non renseigné");
		}else{
			intervenant.setText(sd.incident_intervenant);
		}
		if(sd.incident_commentaire_intervenant.length()==0){
			commentaire_d_intervenant.setText("non renseigné");
		}else{
			commentaire_d_intervenant.setText(sd.incident_commentaire_intervenant);
		}
		
		return view;
	}

	@Override
	public void onClick(View v) {
		int id = v.getId();
		if (id == R.id.tv_back) {
			getActivity().getSupportFragmentManager().beginTransaction()
			.replace(R.id.content_frame1, new AlertSAVdujourDetails(), "alert_sav_du_jour_details").commit();
		}
	}
}
