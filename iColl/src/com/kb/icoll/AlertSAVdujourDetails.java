//  iCollMobile
//
//  Created by Khaled Noui
//  Copyright (c) 2014 digiwabi. All rights reserved.
//

package com.kb.icoll;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.kb.icoll.utils.RESTful;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

public class AlertSAVdujourDetails extends Fragment implements OnClickListener{

	SAVDuJourDetailsData sjd;
	ArrayList<SAVDuJourDetailsData> al_sav_du_jour;
	ListView lv_sav_du_jour;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.alert_sav_du_jour_details, container,false);
		
		TextView header=(TextView)view.findViewById(R.id.tv_header);
		header.setText(AlertListing.sdjd.name+" "+AlertListing.sdjd.first_name);
		
		getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.ll_client_info_container2, new AlertClientInfo(),"alert_client_info").commit();
		
		TextView back=(TextView)view.findViewById(R.id.tv_back);
		back.setOnClickListener(this);
		
		LinearLayout ll_parent=(LinearLayout)view.findViewById(R.id.ll_parent);
		ll_parent.setOnClickListener(this);
		
		lv_sav_du_jour=(ListView)view.findViewById(R.id.lv_sav_du_jour);
		
		new GetSAVDuJourDetails().execute("");
		
		return view;
	}

	@Override
	public void onClick(View v) {
		int id = v.getId();
		if (id == R.id.tv_back) {
			getActivity().getSupportFragmentManager().beginTransaction().remove(this).commit();
		}
	}
	
	private class GetSAVDuJourDetails extends AsyncTask<String,Void,Void> {
		ProgressDialog pd;
		int status_flag;
		
		
		@Override
		public void onPreExecute(){
			pd=new ProgressDialog(getActivity());
			pd.setMessage(getResources().getString(R.string.PLEASE_WAIT));
			pd.setCanceledOnTouchOutside(false);
			pd.show();
		}

		@Override
		protected Void doInBackground(String... params1) {								
			try {
				String reply=new RESTful(getActivity()).queryServer(getResources().getString(R.string.api_end_point)+"incident?commande_id="+AlertListing.sdjd.commande_id, true);
		        System.out.println("json_cnst-1: " + reply);

		        reply=reply.replace(getResources().getString(R.string.json_cnst), "");
		        System.out.println("json_cnst-2: " + reply);

				JSONArray data = new JSONArray(reply);
				al_sav_du_jour=new ArrayList<SAVDuJourDetailsData>();
				for(int i=0;i<data.length();i++){
					JSONObject jo=data.getJSONObject(i);					
					sjd=new SAVDuJourDetailsData();
					sjd.id_commande=jo.getString("id_commande");
					sjd.date_creation=jo.getString("date_creation");
					sjd.incident_id=jo.getString("incident_id");
					sjd.incident_commentaire=jo.getString("incident_commentaire");
					sjd.incident_commentaire_intervenant=jo.getString("incident_commentaire_intervenant");
					sjd.incident_date_intervention=jo.getString("incident_date_intervention");
					sjd.incident_etat=jo.getString("incident_etat");
					sjd.incident_heure_intervention=jo.getString("incident_heure_intervention");
					sjd.incident_intervenant=jo.getString("incident_intervenant");
					sjd.incident_probleme=jo.getString("incident_probleme");
					sjd.incident_type=jo.getString("incident_type");
					
					al_sav_du_jour.add(sjd);
					
				}
				System.out.println("Size of sav array: "+al_sav_du_jour.size());
				status_flag=2;
			} catch (IOException e) {
				if(e.getMessage().equals("403")){
					status_flag=5;
				}else{
					status_flag=3;
				}
				e.printStackTrace();
			} catch (JSONException e) {
				status_flag=4;
			}catch (NullPointerException e) {
				status_flag=6;
			}										 				 	 			 	
			return null;
		}

		@Override
		public void onPostExecute(Void m_adapter){               	
			pd.dismiss();
			if(status_flag==2){
				
				implementListView(al_sav_du_jour);
				
		 	}else if(status_flag==3){
		 		new AlertDialog.Builder(getActivity())
				.setMessage("Erreur de connexion. Veuillez réessayer à nouveau")
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					
					public void onClick(DialogInterface dialog, int id) {							
									
					}
				}).setIcon(android.R.drawable.stat_notify_error).show();
		 	}else if(status_flag==4){
		 		new AlertDialog.Builder(getActivity())
				.setMessage("Erreur du serveur. Veuillez réessayer à nouveau")
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					
					public void onClick(DialogInterface dialog, int id) {							
									
					}
				}).setIcon(android.R.drawable.stat_notify_error).show();
		 	}else if(status_flag==5){
		 		new AlertDialog.Builder(getActivity())
				.setMessage("Paramètres de connexion incorrects")
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					
					public void onClick(DialogInterface dialog, int id) {							
									
					}
				}).setIcon(android.R.drawable.stat_notify_error).show();
		 	}else if(status_flag==6){
		 		new AlertDialog.Builder(getActivity())
				.setMessage("Aucune donnée reçue . S'il vous plait réessayer plus tard.")
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					
					public void onClick(DialogInterface dialog, int id) {							
									
					}
				}).setIcon(android.R.drawable.stat_notify_error).show();
		 	}
		}
	}
	
	public class SAVDuJourDetailsData implements Serializable{
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		String id_commande,date_creation,incident_id,incident_type,incident_probleme,incident_etat,incident_commentaire,incident_date_intervention,
		incident_heure_intervention,incident_intervenant,incident_commentaire_intervenant;
	}
	
	private void implementListView(ArrayList<SAVDuJourDetailsData> al) {
		ArrayAdapter<SAVDuJourDetailsData> ad = new MyAdapter(getActivity(),
				R.layout.ticket_cell, al);	
		lv_sav_du_jour.setAdapter(ad);
		ad.notifyDataSetChanged();
		lv_sav_du_jour.setTextFilterEnabled(true);
	}
	
	private class MyAdapter extends ArrayAdapter<SAVDuJourDetailsData>{
		ArrayList<SAVDuJourDetailsData> my_al;
		
		public MyAdapter(Context context, int textViewResourceId, ArrayList<SAVDuJourDetailsData> al) {
			super(context, textViewResourceId, al);	
			my_al=al;
		}
				
		@Override
		public View getView(final int position, View convertView, ViewGroup parent) {
			View v = convertView;
			
			if (v == null) {				
				LayoutInflater vi = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);				
		        v = vi.inflate(R.layout.client_sav_reclamation_cell, null);		        			        				
			}
			
			TextView sav=(TextView)v.findViewById(R.id.tv_sav);
			sav.setText(my_al.get(position).incident_type+" - "+my_al.get(position).incident_probleme); 
			
			
			v.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					AlertSAVDetailsInfo alsav=new AlertSAVDetailsInfo();
					Bundle b=new Bundle();
					b.putSerializable("sav_data", my_al.get(position));
					alsav.setArguments(b);
					getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.content_frame1, alsav, "alert_sav_details_info").commit();
				}
			});
			return v;
		}
	}
}
