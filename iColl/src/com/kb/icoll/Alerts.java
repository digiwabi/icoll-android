//  iCollMobile
//
//  Created by Khaled Noui
//  Copyright (c) 2014 digiwabi. All rights reserved.
//

package com.kb.icoll;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import org.json.JSONException;
import org.json.JSONObject;

import com.kb.icoll.utils.OnTabChangedListener;
import com.kb.icoll.utils.RESTful;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class Alerts extends Fragment implements OnClickListener{
	OnTabChangedListener mListener;
	SharedPreferences my_pref;
	TextView no_non_statues,no_non_statues_new, documents_projets_concernes,documents_projets_concernes_new,rendez_con,rendez_con_new,photos,photos_new,ticket,ticket_new,changement,changement_new,sav_du_jour,sav_du_jour_new;
	int total_alert,new_alert,non_statues,no_projets_concernes,rendez_confirms,no_photos,no_ticket,no_changement,no_sav_du_jour;
	int new_non_statues,new_confirmes,new_documents,new_photos,new_tickets,new_changement,new_sav_du_jour;
	OnNewAlertListener mAlertListener;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.alerts, container,false);
		my_pref=getActivity().getSharedPreferences(getResources().getString(R.string.APP_DATA), Activity.MODE_PRIVATE); 
		mListener.onTabChanged(1);
		
		TextView name=(TextView)view.findViewById(R.id.tv_name);
		name.setText("Bonjour, "+my_pref.getString("first_name", "")+" "+my_pref.getString("last_name", ""));
		
		TextView societe=(TextView)view.findViewById(R.id.tv_societe);
		societe.setText("Société : "+my_pref.getString("societe_nom", ""));
		
		TextView time=(TextView)view.findViewById(R.id.tv_time);
		SimpleDateFormat sdf=new SimpleDateFormat("HH:mm", Locale.getDefault());
		time.setText("Dernière mise à jour: "+sdf.format(Calendar.getInstance(Locale.getDefault()).getTime()));
		
		LinearLayout ll_statues=(LinearLayout)view.findViewById(R.id.ll_non_statues);
		ll_statues.setOnClickListener(this);
		
		LinearLayout ll_confirms_aujourd=(LinearLayout)view.findViewById(R.id.ll_confirmes_aujourd);
		ll_confirms_aujourd.setOnClickListener(this);
		
		LinearLayout ll_documents=(LinearLayout)view.findViewById(R.id.ll_documents_en_attente_reception);
		ll_documents.setOnClickListener(this);
		
		LinearLayout ll_photo_inserer=(LinearLayout)view.findViewById(R.id.ll_photos_inserer);
		ll_photo_inserer.setOnClickListener(this);
		
		LinearLayout ll_changement_detats=(LinearLayout)view.findViewById(R.id.ll_changement_detats);
		ll_changement_detats.setOnClickListener(this);
		
		LinearLayout ll_sav_du_jour=(LinearLayout)view.findViewById(R.id.ll_sav_du_jour);
		ll_sav_du_jour.setOnClickListener(this);
		
		LinearLayout ll_tickets=(LinearLayout)view.findViewById(R.id.ll_tickets_en_attente_response);
		ll_tickets.setOnClickListener(this);
		
		no_non_statues=(TextView)view.findViewById(R.id.tv_no_nonstatues);
		documents_projets_concernes=(TextView)view.findViewById(R.id.tv_no_projet_concernes);
		rendez_con=(TextView)view.findViewById(R.id.tv_rendez_confirm);
		photos=(TextView)view.findViewById(R.id.tv_photos);
		ticket=(TextView)view.findViewById(R.id.tv_ticket_no);
		changement=(TextView)view.findViewById(R.id.tv_changement);
		sav_du_jour=(TextView)view.findViewById(R.id.tv_sav_du_jour);
		
		no_non_statues_new=(TextView)view.findViewById(R.id.tv_new_non_statues);
		changement_new=(TextView)view.findViewById(R.id.tv_new_changement);
		rendez_con_new=(TextView)view.findViewById(R.id.tv_new_confirmes_aujourd);
		documents_projets_concernes_new=(TextView)view.findViewById(R.id.tv_new_documents);
		photos_new=(TextView)view.findViewById(R.id.tv_new_photos_inserer);
		sav_du_jour_new=(TextView)view.findViewById(R.id.tv_new_sav_du_jour);
		ticket_new=(TextView)view.findViewById(R.id.tv_new_tickets);
		
		if(new RESTful(getActivity()).checkNetworkConnection()){
			new GetNoOfAlerts().execute("");
		}		
		return view;
	}
	
	public interface OnNewAlertListener{
		public void onNewAlert(int new_alert);
	}
	
	private class GetNoOfAlerts extends AsyncTask<String,Void,Void> {
		ProgressDialog pd;
		int status_flag;

		@Override
		public void onPreExecute(){
			pd=new ProgressDialog(getActivity());
			pd.setMessage(getResources().getString(R.string.PLEASE_WAIT));
			pd.setCanceledOnTouchOutside(false);
			pd.show();
		}

		@Override
		protected Void doInBackground(String... params1) {								
			try {
//				String reply=new RESTful(getActivity()).queryServer(getResources().getString(R.string.api_end_point)+"user", true);
//				reply=reply.replace(getResources().getString(R.string.json_cnst), ""); 
//				JSONObject data = new JSONObject(reply);				
//				total_alert=data.getInt("alertes_total");
//				new_alert=data.getInt("alertes_new");
				String reply=new RESTful(getActivity()).queryServer(getResources().getString(R.string.api_end_point)+"alerte",true);
		        System.out.println("json_cnst-1: " + reply);

				reply=reply.replace(getResources().getString(R.string.json_cnst), ""); 
		        System.out.println("json_cnst-2: " + reply);
				JSONObject data = new JSONObject(reply);	
				try{
					non_statues=data.getInt("Rdv non statués");
				}catch(JSONException e){
					
				}
				try{
					rendez_confirms=data.getInt("Rdv confirmés du jour");
				}catch(JSONException e){
					
				}
				try{
					no_projets_concernes=data.getInt("Projets dont docs en attente de réception");
				}catch(JSONException e){
					
				}
				try{
					no_photos=data.getInt("Projets dont photos manquantes");
				}catch(JSONException e){
					
				}
				try{
					no_sav_du_jour=data.getInt("Sav du jour");
				}catch(JSONException e){
					
				}
				try{
					no_ticket=data.getInt("Projets dont tickets en attente de réponse");
				}catch(JSONException e){
					
				}
				try{
					no_changement=data.getInt("Projets concernés par un changement d'état");
				}catch(JSONException e){
					
				}
				try{
					new_non_statues=data.getInt("Rdv non statués new");
				}catch(JSONException e){
					
				}
				try{
					new_changement=data.getInt("Projets concernés par un changement d'état new");
				}catch(JSONException e){
					
				}
				try{
					new_confirmes=data.getInt("Rdv confirmés du jour new");
				}catch(JSONException e){
					
				}
				try{
					new_documents=data.getInt("Projets dont docs en attente de réception new");
				}catch(JSONException e){
					
				}
				try{
					new_photos=data.getInt("Projets dont photos manquantes new");
				}catch(JSONException e){
					
				}
				try{
					new_sav_du_jour=data.getInt("Sav du jour new");
				}catch(JSONException e){
					
				}
				try{
					new_tickets=data.getInt("Projets dont tickets en attente de réponse new");
				}catch(JSONException e){
					
				}			
				status_flag=2;
			} catch (IOException e) {
				if(e.getMessage().equals("403")){
					status_flag=5;
				}else{
					status_flag=3;
				}
				e.printStackTrace();
			} catch (JSONException e) {
				e.printStackTrace();
				status_flag=4;
			}catch (NullPointerException e) {
				status_flag=6;
			}
					 				 	 			 	
			return null;
		}

		@Override
		public void onPostExecute(Void m_adapter){               	
			pd.dismiss();
			if(status_flag==2){
		 		no_non_statues.setText(""+non_statues);
		 		documents_projets_concernes.setText(""+no_projets_concernes);
		 		rendez_con.setText(""+rendez_confirms);
		 		photos.setText(""+no_photos);
		 		ticket.setText(""+no_ticket);
		 		changement.setText(""+no_changement);
		 		sav_du_jour.setText(""+no_sav_du_jour);
		 		
		 		if(new_changement>0){
		 			changement_new.setVisibility(View.VISIBLE);
		 			changement_new.setText(""+new_changement);
		 		}else{
		 			changement_new.setVisibility(View.GONE);
		 		}
		 		if(new_confirmes>0){
		 			rendez_con_new.setVisibility(View.VISIBLE);
		 			rendez_con_new.setText(""+new_confirmes);
		 		}else{
		 			rendez_con_new.setVisibility(View.GONE);
		 		}
		 		if(new_documents>0){
		 			documents_projets_concernes_new.setVisibility(View.VISIBLE);
		 			documents_projets_concernes_new.setText(""+new_documents);
		 		}else{
		 			documents_projets_concernes_new.setVisibility(View.GONE);
		 		}
		 		if(new_non_statues>0){
		 			no_non_statues_new.setVisibility(View.VISIBLE);
		 			no_non_statues_new.setText(""+new_non_statues);
		 		}else{
		 			no_non_statues_new.setVisibility(View.GONE);
		 		}
		 		if(new_photos>0){
		 			photos_new.setVisibility(View.VISIBLE);
		 			photos_new.setText(""+new_photos);
		 		}else{
		 			photos_new.setVisibility(View.GONE);
		 		}
		 		if(new_sav_du_jour>0){
		 			sav_du_jour_new.setVisibility(View.VISIBLE);
		 			sav_du_jour_new.setText(""+new_sav_du_jour);
		 		}else{
		 			sav_du_jour_new.setVisibility(View.GONE);
		 		}
		 		if(new_tickets>0){
		 			ticket_new.setVisibility(View.VISIBLE);
		 			ticket_new.setText(""+new_tickets);
		 		}else{
		 			ticket_new.setVisibility(View.GONE);
		 		}
		 		
		 		//mAlertListener.onNewAlert(new_alert);
		 		
		 	}else if(status_flag==3){
		 		new AlertDialog.Builder(getActivity())
				.setMessage("Erreur de connexion. Veuillez réessayer à nouveau")
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					
					public void onClick(DialogInterface dialog, int id) {							
									
					}
				}).setIcon(android.R.drawable.stat_notify_error).show();
		 	}else if(status_flag==4){
		 		new AlertDialog.Builder(getActivity())
				.setMessage("Mauvaises credintials !")
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					
					public void onClick(DialogInterface dialog, int id) {							
									
					}
				}).setIcon(android.R.drawable.stat_notify_error).show();
		 	}else if(status_flag==5){
		 		new AlertDialog.Builder(getActivity())
				.setMessage("Erreur du serveur. Veuillez réessayer à nouveau")
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					
					public void onClick(DialogInterface dialog, int id) {							
									
					}
				}).setIcon(android.R.drawable.stat_notify_error).show();
		 	}else if(status_flag==6){
		 		new AlertDialog.Builder(getActivity())
				.setMessage("Aucune donnée reçue . S'il vous plait réessayer plus tard.")
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					
					public void onClick(DialogInterface dialog, int id) {							
									
					}
				}).setIcon(android.R.drawable.stat_notify_error).show();
		 	}
		}
	}
	
	 @Override
	 public void onAttach(Activity activity) {
		 super.onAttach(activity);
	     try {	       
	    	 mListener= (OnTabChangedListener) activity;
	    	 mAlertListener = (OnNewAlertListener) activity;
	     } catch (ClassCastException e) {
	    	 throw new ClassCastException(activity.toString()+ " must implement OnTabChangedListener/OnNewAlertListener");
	     }
	 }

	@Override
	public void onClick(View v) {
		int id = v.getId();
		if (id == R.id.ll_non_statues) {
			if(non_statues==0){
				Toast.makeText(getActivity(), "Aucun résultat", Toast.LENGTH_SHORT).show();
			}else{
				AlertListing al=new AlertListing();
				Bundle b=new Bundle();
				b.putInt("type", 3);
				al.setArguments(b);
				getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, al, "alert_listing").commit();
			}
		} else if (id == R.id.ll_documents_en_attente_reception) {
			if(no_projets_concernes==0){
				Toast.makeText(getActivity(), "Aucun résultat", Toast.LENGTH_SHORT).show();
			}else{
				AlertListing al=new AlertListing();
				Bundle b=new Bundle();
				b.putInt("type", 1);
				al.setArguments(b);
				getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, al, "alert_listing").commit();
			}
		} else if (id == R.id.ll_photos_inserer) {
			if(no_photos==0){
				Toast.makeText(getActivity(), "Aucun résultat", Toast.LENGTH_SHORT).show();
			}else{
				AlertListing al=new AlertListing();
				Bundle b=new Bundle();
				b.putInt("type", 2);
				al.setArguments(b);
				getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, al, "alert_listing").commit();
			}
		} else if (id == R.id.ll_confirmes_aujourd) {
			if(rendez_confirms==0){
				Toast.makeText(getActivity(), "Aucun résultat", Toast.LENGTH_SHORT).show();
			}else{
				AlertListing al=new AlertListing();
				Bundle b=new Bundle();
				b.putInt("type", 4);
				al.setArguments(b);
				getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, al, "alert_listing").commit();
			}
		} else if (id == R.id.ll_changement_detats) {
			if(no_changement==0){
				Toast.makeText(getActivity(), "Aucun résultat", Toast.LENGTH_SHORT).show();
			}else{
				AlertListing al=new AlertListing();
				Bundle b=new Bundle();
				b.putInt("type", 7);
				al.setArguments(b);
				getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, al, "alert_listing").commit();
			}
		} else if (id == R.id.ll_sav_du_jour) {
			if(no_sav_du_jour==0){
				Toast.makeText(getActivity(), "Aucun résultat", Toast.LENGTH_SHORT).show();
			}else{
				AlertListing al=new AlertListing();
				Bundle b=new Bundle();
				b.putInt("type", 6);
				al.setArguments(b);
				getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, al, "alert_listing").commit();
			}
		} else if (id == R.id.ll_tickets_en_attente_response) {
			if(no_ticket==0){
				Toast.makeText(getActivity(), "Aucun résultat", Toast.LENGTH_SHORT).show();
			}else{
				AlertListing al=new AlertListing();
				Bundle b=new Bundle();
				b.putInt("type", 5);
				al.setArguments(b);
				getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, al, "alert_listing").commit();
			}
		}
	}
}
