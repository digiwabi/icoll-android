//  iCollMobile
//
//  Created by Khaled Noui
//  Copyright (c) 2014 digiwabi. All rights reserved.
//

package com.kb.icoll;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.GoogleMap.InfoWindowAdapter;
import com.google.android.gms.maps.GoogleMap.OnInfoWindowClickListener;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.SeekBar.OnSeekBarChangeListener;

public class AlertsProspectsAround extends Fragment implements OnClickListener, LocationListener {	
	private GoogleMap mMap;
	SupportMapFragment fragment;
	//private static View view;
	static LatLng LOC;
	SeekBar sb;
	double c_lat,c_lon,lat,lon;
	String provider="network";
	Location location;
	Handler h;
	Runnable r;
	TextView plan,liste,none_within_25;
	float distance;
	ArrayList<InfoWindowContent> al_dis_within_25km;
	ListView lv_prospects;
	ArrayAdapter<InfoWindowContent> ad;
	ArrayList<InfoWindowContent> al_prospects;
	Map<Marker, InfoWindowContent> markersContent = new HashMap<Marker, InfoWindowContent>();
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
//		if (view != null) {
//			ViewGroup parent = (ViewGroup) view.getParent();
//		if (parent != null)
//			parent.removeView(view);
//		}
		View view = inflater.inflate(R.layout.alert_prospects_around, container, false);
//		try {
//			
//		} catch (InflateException e) {
//			e.printStackTrace();
//		}
	
		TextView back=(TextView)view.findViewById(R.id.tv_back);
		back.setOnClickListener(this);
		
		lv_prospects=(ListView)view.findViewById(R.id.lv_prospects);
		lv_prospects.setVisibility(View.GONE);
		
		sb=(SeekBar)view.findViewById(R.id.seekBar1);
		sb.setMax(10);
		sb.setProgress(5);
		
		sb.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
			
			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {
				int distance =51-seekBar.getProgress()*5;
				System.out.println(distance);
				
				addData(distance);
				System.out.println("_________))  "+al_dis_within_25km.size());
				implementListView(al_dis_within_25km);
			}						

			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {
				
			}
			
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress,
					boolean fromUser) {
				//mMap.animateCamera(CameraUpdateFactory.zoomTo(progress+5), 2000, null);
				mMap.animateCamera(CameraUpdateFactory.zoomTo((progress+11)/2+5), 2000, null);
			}
		});
		
		plan=(TextView)view.findViewById(R.id.tv_plan);
		plan.setOnClickListener(this);
		plan.setBackgroundResource(R.drawable.orange_solid_left_rounded);
		plan.setTextColor(getResources().getColor(R.color.white));
		
		liste=(TextView)view.findViewById(R.id.tv_liste);
		liste.setOnClickListener(this);
		liste.setBackgroundResource(0);
		liste.setTextColor(getResources().getColor(R.color.theme_color));
		
		none_within_25=(TextView)view.findViewById(R.id.tv_none_within_25);
		none_within_25.setVisibility(View.GONE);
		
		if (mMap == null) {
			 mMap = ((SupportMapFragment)getChildFragmentManager().findFragmentById(R.id.map)).getMap();
			 
//			 android.support.v4.app.FragmentManager myFM = getActivity().getSupportFragmentManager();
//			 final SupportMapFragment myMAPF = (SupportMapFragment) myFM.findFragmentById(R.id.map);
//			 mMap = myMAPF.getMap();
		    
		    	 
		    	 //mMap.setMyLocationEnabled(true);  
		    	// mMap.animateCamera(CameraUpdateFactory.zoomTo(10), 2000, null);
		    	 
		    	 if (mMap != null) {
		    		 
//		    		 addData(25);
			    	 
//				    LocationManager lm = (LocationManager)getActivity().getSystemService(Context.LOCATION_SERVICE);
//				 	lm.requestLocationUpdates(provider, 1000L, 1.0f, this);
//				 	location = lm.getLastKnownLocation(provider);
//				    
//				 	h=new Handler();
//					r =new Runnable() {
//								
//						public void run() {
//							if(location!=null){							
//									  c_lat=location.getLatitude();
//									  c_lon=location.getLongitude();
//									  System.out.println("Current Lat is: "+c_lat+"  Long is: "+c_lon);						  							    
//							}else{
//									h.postDelayed(r, 3000);
//							}
//						}
//					};
//					h.post(r);		    
		    	 
		    	 
		    	 if((getArguments().getString("alert_pros_around_via").equalsIgnoreCase("agenda_liste")||
		    			 getArguments().getString("alert_pros_around_via").equalsIgnoreCase("month_view"))){
		    		 al_dis_within_25km=new ArrayList<InfoWindowContent>();
		    		 c_lat=AgendaListe.ag_data.latitude;
	    			 c_lon=AgendaListe.ag_data.longitude;
		    		 for(int i=0;i<Agenda.al_agenda_list.size();i++){
							lat=Agenda.al_agenda_list.get(i).latitude;
							lon=Agenda.al_agenda_list.get(i).longitude;
							System.out.println("Latitude: "+lat);
							System.out.println("Longitude: "+lon);
							if(lat!=0 || lon!=0){
								Marker venue_loc = mMap.addMarker(new MarkerOptions().position(new LatLng(lat, lon)));
						
								InfoWindowContent markerContent = new InfoWindowContent();
								System.out.println(Agenda.al_agenda_list.get(i).prenom+" "+Agenda.al_agenda_list.get(i).nom);
								markerContent.client_id=Agenda.al_agenda_list.get(i).client_id;
								markerContent.client_name=Agenda.al_agenda_list.get(i).prenom+" "+Agenda.al_agenda_list.get(i).nom;
								markerContent.addresse=Agenda.al_agenda_list.get(i).addresse;
								markerContent.address=Agenda.al_agenda_list.get(i).address;
								markerContent.date=Agenda.al_agenda_list.get(i).start_date;
								markerContent.last_rdv_confirmed_statut=Agenda.al_agenda_list.get(i).statut_rdv_lb;
								markerContent.tel_fixe=Agenda.al_agenda_list.get(i).telephone;
						        markersContent.put(venue_loc, markerContent);	
						        
						        LOC=new LatLng(c_lat, c_lon);
						        
						        distance=getDistance(lat, lon, c_lat, c_lon);
								distance=distance/1000;
								System.out.println("Distance from current pos: "+distance+" km");
								if(distance<25.00){
									//System.out.println(markersContent.get(venue_loc).client_name);
									al_dis_within_25km.add(markersContent.get(venue_loc));
								}
							}
						}
		    	 }else{
		    		 al_dis_within_25km=new ArrayList<InfoWindowContent>();
		    		 for(int i=0;i<AlertListing.al_rdv_non_status.size();i++){		    			 	
							lat=AlertListing.al_rdv_non_status.get(i).latitude;
							lon=AlertListing.al_rdv_non_status.get(i).longitude;
							c_lat=AlertListing.rsd.latitude;
							c_lon=AlertListing.rsd.longitude;
							LOC=new LatLng(c_lat, c_lon);
							System.out.println("Latitude: "+lat);
							System.out.println("Longitude: "+lon);
							System.out.println("cLatitude: "+c_lat);
							System.out.println("cLongitude: "+c_lon);
							if(lat!=0 || lon!=0){
								Marker venue_loc = mMap.addMarker(new MarkerOptions().position(new LatLng(lat, lon)));
								
								InfoWindowContent markerContent = new InfoWindowContent();
								markerContent.client_id=AlertListing.al_rdv_non_status.get(i).client_id;
								markerContent.client_name=AlertListing.al_rdv_non_status.get(i).name+" "+AlertListing.al_rdv_non_status.get(i).first_name;
								markerContent.addresse=AlertListing.al_rdv_non_status.get(i).adresse;
								markerContent.address=AlertListing.al_rdv_non_status.get(i).address;
								markerContent.date=AlertListing.al_rdv_non_status.get(i).start_date;
								markerContent.last_rdv_confirmed_statut=AlertListing.al_rdv_non_status.get(i).statut;
								markerContent.tel_fixe=AlertListing.al_rdv_non_status.get(i).telephone;
						        markersContent.put(venue_loc, markerContent);															
								
								distance=getDistance(lat, lon, c_lat, c_lon);
								distance=distance/1000;
								System.out.println("Distance from current pos: "+distance+" km");
								if(distance<25.00){
									//System.out.println(markersContent.get(venue_loc).client_name);
									al_dis_within_25km.add(markersContent.get(venue_loc));
								}
							}
							
						}
		    		 for(int i=0;i<Prospects.al.size();i++){
							lat=Prospects.al.get(i).prospect_latitude;
							lon=Prospects.al.get(i).prospect_longitude;
							System.out.println("Latitude: "+lat);
							System.out.println("Longitude: "+lon); 
							if(lat!=0 || lon!=0){
								Marker venue_loc = mMap.addMarker(new MarkerOptions().position(new LatLng(lat, lon)));							
								InfoWindowContent markerContent = new InfoWindowContent();
								markerContent.client_id=Prospects.al.get(i).id;
								markerContent.client_name=Prospects.al.get(i).name+" "+Prospects.al.get(i).first_name;
								markerContent.addresse=Prospects.al.get(i).adresse;
								markerContent.address=Prospects.al.get(i).postal_code+" "+Prospects.al.get(i).ville;
								markerContent.date=Prospects.al.get(i).last_rdv_confirmed_start_date;
								markerContent.last_rdv_confirmed_statut=Prospects.al.get(i).last_rdv_confirmed_statut;
								markerContent.tel_fixe=Prospects.al.get(i).tel_fixe;
						        markersContent.put(venue_loc, markerContent);					        
						        distance=getDistance(lat, lon, c_lat, c_lon);
								distance=distance/1000;
								System.out.println("Distance from current pos: "+distance+" km");
								if(distance<25){
									al_dis_within_25km.add(markersContent.get(venue_loc));
								}
							}
						}
		    	 }
		    	 
		    	 mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(LOC, 10));
		    	 mMap.animateCamera(CameraUpdateFactory.zoomTo(10), 2000, null);
		    	 
			     mMap.setInfoWindowAdapter(new InfoWindowAdapter() {

			         @Override
			         public View getInfoWindow(Marker arg0) {
			             return null;
			         }

			         @Override
			         public View getInfoContents(Marker arg0) { 
			             View v = getActivity().getLayoutInflater().inflate(R.layout.map_details_cell_prospects, null);
			             InfoWindowContent markerContent = markersContent.get(arg0);

			             LinearLayout ll_package_type=(LinearLayout)v.findViewById(R.id.ll_package_type);
			             LinearLayout ll_puissance=(LinearLayout)v.findViewById(R.id.ll_puissance);
			             LinearLayout ll_etat_installation=(LinearLayout)v.findViewById(R.id.ll_etat_installation);
			             LinearLayout ll_date_installation=(LinearLayout)v.findViewById(R.id.ll_date_installation);
			             LinearLayout ll_date_dernier=(LinearLayout)v.findViewById(R.id.ll_date_dernier);
			             LinearLayout ll_statut_dernier=(LinearLayout)v.findViewById(R.id.ll_statut_dernier);
			            		 
			             TextView client_name = (TextView)v.findViewById(R.id.tv_client_name);
			             client_name.setText(markerContent.client_name);
			             
			             TextView add = (TextView)v.findViewById(R.id.tv_addresse);
			             add.setText(markerContent.addresse);
			             
			             TextView add1= (TextView)v.findViewById(R.id.tv_address);
			             add1.setText(markerContent.address);
			             
			             TextView date_darnier= (TextView)v.findViewById(R.id.tv_date_du_dernier_rdv);
			             date_darnier.setText(markerContent.date);
			             
			             TextView statut= (TextView)v.findViewById(R.id.tv_statut_dernier_rdv);
			             statut.setText(markerContent.last_rdv_confirmed_statut);
			             
			             if((getArguments().getString("alert_pros_around_via").equalsIgnoreCase("agenda_liste")||getArguments().getString("alert_pros_around_via").equalsIgnoreCase("month_view")||
			            		 getArguments().getString("alert_pros_around_via").equalsIgnoreCase("alert_listing"))){
			            	 ll_package_type.setVisibility(View.GONE);
			            	 ll_puissance.setVisibility(View.GONE);
			            	 ll_etat_installation.setVisibility(View.GONE);
			            	 ll_date_installation.setVisibility(View.GONE);
			             }
//			             if(getArguments().getString("about_me_call").equals("prospects")){
//			            	 ll_date_dernier.setVisibility(View.VISIBLE);
//			            	 ll_statut_dernier.setVisibility(View.VISIBLE);
//			            	 ll_package_type.setVisibility(View.GONE);
//			            	 ll_puissance.setVisibility(View.GONE);
//			            	 ll_etat_installation.setVisibility(View.GONE);
//			            	 ll_date_installation.setVisibility(View.GONE);
//			            	 
//			            	 TextView date= (TextView)v.findViewById(R.id.tv_date_du_dernier_rdv);
//				             date.setText(markerContent.date);
//				             
//				             TextView last_rdv_confirmed_statut= (TextView)v.findViewById(R.id.tv_statut_dernier_rdv);
//				             last_rdv_confirmed_statut.setText(markerContent.last_rdv_confirmed_statut);		      
//			            	 
//			            	 
//			             }else{
//			            	 ll_date_dernier.setVisibility(View.GONE);
//			            	 ll_statut_dernier.setVisibility(View.GONE);
//			            	 ll_package_type.setVisibility(View.VISIBLE);
//			            	 ll_puissance.setVisibility(View.VISIBLE);
//			            	 ll_etat_installation.setVisibility(View.VISIBLE);
//			            	 ll_date_installation.setVisibility(View.VISIBLE);
//			            	 
//			            	 TextView package_type= (TextView)v.findViewById(R.id.tv_package_type);
//			            	 package_type.setText(markerContent.package_name);
//				             
//				             TextView puissance_installation= (TextView)v.findViewById(R.id.tv_puissance_installation);
//				             puissance_installation.setText(markerContent.package_puissance);
//				             
//				             TextView etat_installation= (TextView)v.findViewById(R.id.tv_etat_installation);
//				             etat_installation.setText(markerContent.projet_etat_installation);
//				             
//				             TextView date_installation= (TextView)v.findViewById(R.id.tv_date_installation);
//				             if(markerContent.projet_date_installation.length()==0){
//				            	 date_installation.setText("non renseigné");
//				             }else{
//				            	 date_installation.setText(markerContent.projet_date_installation);
//				             }
//			             }

			             return v;

			         }
			     });

			     mMap.setOnInfoWindowClickListener(new OnInfoWindowClickListener() {

					@Override
					public void onInfoWindowClick(Marker arg0) {
						InfoWindowContent markerContent = markersContent.get(arg0);
						InfoDetails id=new InfoDetails();
						Bundle b=new Bundle();
						b.putString("client_name", markerContent.client_name); 
						b.putString("addresse", markerContent.addresse); 
						b.putString("address", markerContent.address);
						b.putString("tel_fixe", markerContent.tel_fixe);
						b.putDouble("current_lat", c_lat);
						b.putDouble("current_lon", c_lon);
						b.putDouble("destination_lat", arg0.getPosition().latitude);
						b.putDouble("destination_lon", arg0.getPosition().longitude);
						b.putString("date", markerContent.date); 
						b.putString("last_rdv_confirmed_statut", markerContent.last_rdv_confirmed_statut);
						b.putString("id", markerContent.client_id);
//						if((getArguments().getString("alert_pros_around_via").equalsIgnoreCase("agenda_liste")||
//				    			 getArguments().getString("alert_pros_around_via").equalsIgnoreCase("month_view"))){
//							//b.putString("id", markerContent.client_id);
//							b.putString("info_call_via", "prospects");
//						}else{
//							//b.putString("client_id", markerContent.client_id);
//							//b.putString("projet_id", markerContent.projet_id);
//							b.putString("info_call_via", "client");
//						}
						b.putString("info_call_via", "alert_details_rdv");
						id.setArguments(b);
						getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.content_frame3, id, "info_details").commit();
					}
			    	 
			     });
		    	}
		    
		
		}
		return view;
	}
	
	private void addData(int dis) {	
		System.out.println("distance"+dis);
   	 if((getArguments().getString("alert_pros_around_via").equalsIgnoreCase("agenda_liste")||
			 getArguments().getString("alert_pros_around_via").equalsIgnoreCase("month_view"))){
		 al_dis_within_25km=new ArrayList<InfoWindowContent>();
		 c_lat=AgendaListe.ag_data.latitude;
		 c_lon=AgendaListe.ag_data.longitude;
		 for(int i=0;i<Agenda.al_agenda_list.size();i++){
				lat=Agenda.al_agenda_list.get(i).latitude;
				lon=Agenda.al_agenda_list.get(i).longitude;
				System.out.println("Latitude: "+lat);
				System.out.println("Longitude: "+lon);
				if(lat!=0 || lon!=0){	
					InfoWindowContent markerContent = new InfoWindowContent();
					System.out.println(Agenda.al_agenda_list.get(i).prenom+" "+Agenda.al_agenda_list.get(i).nom);
					markerContent.client_id=Agenda.al_agenda_list.get(i).client_id;
					markerContent.client_name=Agenda.al_agenda_list.get(i).prenom+" "+Agenda.al_agenda_list.get(i).nom;
					markerContent.addresse=Agenda.al_agenda_list.get(i).addresse;
					markerContent.address=Agenda.al_agenda_list.get(i).address;
					markerContent.date=Agenda.al_agenda_list.get(i).start_date;
					markerContent.last_rdv_confirmed_statut=Agenda.al_agenda_list.get(i).statut_rdv_lb;
					markerContent.tel_fixe=Agenda.al_agenda_list.get(i).telephone;			        
			        distance=getDistance(lat, lon, c_lat, c_lon);
					distance=distance/1000;
					System.out.println("Distance from current pos: "+distance+" km");
					if(distance<dis){
						al_dis_within_25km.add(markerContent);
					}
				}
			}
	 }else{
		 al_dis_within_25km=new ArrayList<InfoWindowContent>();
		 for(int i=0;i<AlertListing.al_rdv_non_status.size();i++){		    			 	
				lat=AlertListing.al_rdv_non_status.get(i).latitude;
				lon=AlertListing.al_rdv_non_status.get(i).longitude;
				c_lat=AlertListing.rsd.latitude;
				c_lon=AlertListing.rsd.longitude;
				//System.out.println("Latitude: "+lat);
				//System.out.println("Longitude: "+lon);
				//System.out.println("cLatitude: "+c_lat);
				//System.out.println("cLongitude: "+c_lon);
				if(lat!=0 || lon!=0){
					InfoWindowContent markerContent = new InfoWindowContent();
					markerContent.client_id=AlertListing.al_rdv_non_status.get(i).client_id;
					markerContent.client_name=AlertListing.al_rdv_non_status.get(i).first_name+" "+AlertListing.al_rdv_non_status.get(i).name;
					markerContent.addresse=AlertListing.al_rdv_non_status.get(i).adresse;
					markerContent.address=AlertListing.al_rdv_non_status.get(i).address;
					markerContent.date=AlertListing.al_rdv_non_status.get(i).start_date;
					markerContent.last_rdv_confirmed_statut=AlertListing.al_rdv_non_status.get(i).statut;
					markerContent.tel_fixe=AlertListing.al_rdv_non_status.get(i).telephone;					
					distance=getDistance(lat, lon, c_lat, c_lon);
					distance=distance/1000;					
					if(distance<dis){
						System.out.println("Distance from current pos: "+distance+" km");
						al_dis_within_25km.add(markerContent);
					}
				}
				
			}
		 
		 for(int i=0;i<Prospects.al.size();i++){
				lat=Prospects.al.get(i).prospect_latitude;
				lon=Prospects.al.get(i).prospect_longitude;
				System.out.println("Latitude: "+lat);
				System.out.println("Longitude: "+lon); 
				if(lat!=0 || lon!=0){
					Marker venue_loc = mMap.addMarker(new MarkerOptions().position(new LatLng(lat, lon)));							
					InfoWindowContent markerContent = new InfoWindowContent();
					markerContent.client_id=Prospects.al.get(i).id;
					markerContent.client_name=Prospects.al.get(i).name+" "+Prospects.al.get(i).first_name;
					markerContent.addresse=Prospects.al.get(i).adresse;
					markerContent.address=Prospects.al.get(i).postal_code+" "+Prospects.al.get(i).ville;
					markerContent.date=Prospects.al.get(i).last_rdv_confirmed_start_date;
					markerContent.last_rdv_confirmed_statut=Prospects.al.get(i).last_rdv_confirmed_statut;
					markerContent.tel_fixe=Prospects.al.get(i).tel_fixe;
			        markersContent.put(venue_loc, markerContent);					        
			        distance=getDistance(lat, lon, c_lat, c_lon);
					distance=distance/1000;					
					if(distance<dis){
						System.out.println("Distance from current posbb: "+distance+" km");
						al_dis_within_25km.add(markersContent.get(venue_loc)); 
					}
				}
			}
	 }
	}
	
	private class InfoWindowContent{
		String client_name,addresse,address,date,last_rdv_confirmed_statut,tel_fixe,package_name,package_puissance,projet_etat_installation,projet_date_installation,client_id;
	}

	@Override
	public void onClick(View v) {
		int id = v.getId();
		if (id == R.id.tv_plan) {
			lv_prospects.setVisibility(View.GONE);
			none_within_25.setVisibility(View.GONE);
			plan.setBackgroundResource(R.drawable.orange_solid_left_rounded);
			plan.setTextColor(getResources().getColor(R.color.white));
			liste.setBackgroundResource(0);
			liste.setTextColor(getResources().getColor(R.color.theme_color));
		} else if (id == R.id.tv_liste) {
			lv_prospects.setVisibility(View.VISIBLE);
			liste.setBackgroundResource(R.drawable.orange_solid_right_rounded);
			liste.setTextColor(getResources().getColor(R.color.white));
			plan.setBackgroundResource(0);
			plan.setTextColor(getResources().getColor(R.color.theme_color));
			System.out.println("size of list: "+al_dis_within_25km.size());
			implementListView(al_dis_within_25km);
			if(al_dis_within_25km.size()>0){
				none_within_25.setVisibility(View.GONE);
			}else{
				none_within_25.setVisibility(View.VISIBLE);
			}
		} else if (id == R.id.tv_back) {
			getActivity().getSupportFragmentManager().beginTransaction().remove(this).commit();
		}
	}
	
	private class MyAdapter extends ArrayAdapter<InfoWindowContent>{
		
		ArrayList<InfoWindowContent> my_al_data;
		
		public MyAdapter(Context context, int textViewResourceId,
				ArrayList<InfoWindowContent> al) {
			super(context, textViewResourceId, al);	
			my_al_data=al;
		}
		
		
		@Override
		public View getView(final int position, View convertView, ViewGroup parent) {
			View v = convertView;
			
			if (v == null) {				
				LayoutInflater vi = (LayoutInflater)getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);				
		        v = vi.inflate(R.layout.alerts_prospects_liste_cell, null);		        			        				
			}												
	
			TextView prospects_anme=(TextView)v.findViewById(R.id.tv_prospects_name);
			prospects_anme.setText(my_al_data.get(position).client_name);	

			v.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					ProspectDetails cd=new ProspectDetails();
					Bundle b=new Bundle();
					b.putString("id", my_al_data.get(position).client_id);
					b.putString("prospect_details_via", "prospects");
					cd.setArguments(b);
					getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, cd, "prospect_details").commit();	
					AlertDetailsRDV ad=(AlertDetailsRDV)getActivity().getSupportFragmentManager().findFragmentByTag("alert_details_rdv");
					if(ad!=null){
						getActivity().getSupportFragmentManager().beginTransaction().remove(ad).commit();
					}
					getActivity().getSupportFragmentManager().beginTransaction().remove(AlertsProspectsAround.this).commit();
				}
			});
		
			return v;
		}
	}
	
	private float getDistance(double lat1, double lon1, double lat2, double lon2){
		Location loc1 = new Location("");
		loc1.setLatitude(lat1);
		loc1.setLongitude(lon1);

		Location loc2 = new Location("");
		loc2.setLatitude(lat2);
		loc2.setLongitude(lon2);

		float distanceInMeters = loc1.distanceTo(loc2);
		return distanceInMeters;
	}
	
	private void implementListView(ArrayList<InfoWindowContent> al2) {
		ad = new MyAdapter(getActivity(), R.layout.alert_prospects_around, al2);	
		lv_prospects.setAdapter(ad);
		ad.notifyDataSetChanged();
		lv_prospects.setTextFilterEnabled(true);
	}
	

	@Override
	public void onLocationChanged(Location location) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onProviderEnabled(String provider) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onProviderDisabled(String provider) {
		// TODO Auto-generated method stub
		
	}
}
