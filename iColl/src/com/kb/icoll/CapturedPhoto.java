//  iCollMobile
//
//  Created by Khaled Noui
//  Copyright (c) 2014 digiwabi. All rights reserved.
//

package com.kb.icoll;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileDescriptor;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;

import com.kb.icoll.utils.RESTful;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Bitmap.CompressFormat;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.ParcelFileDescriptor;
import android.support.v4.app.Fragment;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

public class CapturedPhoto extends Fragment implements OnClickListener{
	Bitmap b;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.captured_photo, container,false);
		
		ImageView iv=(ImageView)view.findViewById(R.id.iv_photo);
		if(getArguments().getInt("type")==PhotoPickDialog.PICK_FROM_CAMERA){
			Uri uri=Uri.parse(getArguments().getString("path"));
			File imgFile = new File(uri.getPath());
			if(imgFile.exists()){
			    b = getResizedBitmap(BitmapFactory.decodeFile(imgFile.getAbsolutePath()), 680, 680);
			    iv.setImageBitmap(b);
			}
		}else{
			ParcelFileDescriptor parcelFileDescriptor;
          try {
              parcelFileDescriptor = getActivity().getContentResolver().openFileDescriptor(Uri.parse(getArguments().getString("path")), "r");
              FileDescriptor fileDescriptor = parcelFileDescriptor.getFileDescriptor();
              b = BitmapFactory.decodeFileDescriptor(fileDescriptor);
              parcelFileDescriptor.close();
              iv.setImageBitmap(b);
          } catch (FileNotFoundException e) {
              e.printStackTrace();
          } catch (IOException e) {                  
              e.printStackTrace();
          }
		}
		
		TextView cancel=(TextView)view.findViewById(R.id.tv_cancel);
		cancel.setOnClickListener(this);
		
		TextView back=(TextView)view.findViewById(R.id.tv_back);
		back.setOnClickListener(this);
		
		TextView ok=(TextView)view.findViewById(R.id.tv_ok);
		ok.setOnClickListener(this);
		return view;
	}
	
	private Bitmap getResizedBitmap(Bitmap bm, int newWidth, int newHeight) {
        Bitmap newBitmap = Bitmap.createScaledBitmap(bm, newWidth,
                newHeight, false);
        bm.recycle();
        return newBitmap;
    }

	@Override
	public void onClick(View v) {
		int id = v.getId();
		if (id == R.id.tv_back) {
			getActivity().getSupportFragmentManager().beginTransaction().remove(this).commit();
		} else if (id == R.id.tv_cancel) {
			getActivity().getSupportFragmentManager().beginTransaction().remove(this).commit();
		} else if (id == R.id.tv_ok) {
			if(new RESTful(getActivity()).checkNetworkConnection()){
				new PostPhotoData().execute("");
			}
		}
	}
	
	private class PostPhotoData extends AsyncTask<String,Void,Void> {
		ProgressDialog pd;
		@SuppressWarnings("unused")
		int status_flag,total_alert,new_alert,non_statues,no_projets_concernes,
			rendez_confirms,no_photos,no_ticket,no_changement,no_sav_du_jour;
		
		@Override
		public void onPreExecute(){
			pd=new ProgressDialog(getActivity());
			pd.setMessage(getResources().getString(R.string.PLEASE_WAIT));
			pd.setCanceledOnTouchOutside(false);
			pd.show();
		}

		@Override
		protected Void doInBackground(String... params1) {	
//			MultipartEntityBuilder builder = MultipartEntityBuilder.create();        
//	 	    builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
//	 	    Map<String, String> params=new HashMap<String, String>();
//	 	    if(b!=null){
////	 	    	ByteArrayOutputStream bos = new ByteArrayOutputStream();
////	 	    	Bitmap resizedBitmap = Bitmap.createScaledBitmap(b, 950, 750, false);
////	 	    	resizedBitmap.compress(CompressFormat.JPEG, 50 /*ignored for PNG*/, bos);	 	 
////	 	    	
////	 	    	byte[] ba = bos.toByteArray();
////	 	    	byte[] ba1 = Base64.encode(ba, Base64.DEFAULT); 
////				File f = new File(getActivity().getCacheDir(), "temp.png");
////				try {
////					f.createNewFile();
////					ByteArrayOutputStream bos = new ByteArrayOutputStream();				
////					b.compress(CompressFormat.PNG, 0 /*ignored for PNG*/, bos);
////					byte[] bitmapdata = bos.toByteArray();
////
////					//write the bytes in file
////					FileOutputStream fos = new FileOutputStream(f);
////					fos.write(bitmapdata);
////					fos.close();
////					fos.flush();
////				} catch (IOException e) {
////					e.printStackTrace();
////				}
//				//builder.addPart("photoBase64Data", new ByteArrayBody(ba1, "file")); 
//				
////			 	params.put("photoType", "1");
////			    params.put("client_id", Photos.client_id);
////			    params.put("projet_id", Photos.projet_id);
////			    
////			    String str = Base64.encodeToString(ba, Base64.DEFAULT); 
////			    System.out.println(str);
//			    //params.put("photoBase64Data", str);
//			    
////			    builder.addTextBody("photoType", ""+getArguments().getInt("type_id")); 
////		 	    builder.addTextBody("client_id", Photos.client_id);
////		 	    builder.addTextBody("projet_id", Photos.projet_id);
////		 	    builder.addTextBody("photoBase64Data", str);
//		 	    
//		 	   
//			}
	 	   
	 	   if(b!=null){
	 	    	ByteArrayOutputStream bos = new ByteArrayOutputStream();
	 	    	//Bitmap resizedBitmap = Bitmap.createScaledBitmap(b, 800, 750, false);
	 	    	Bitmap resizedBitmap=null;
	 	    	if(b.getWidth()>=b.getHeight()){
	 	    		resizedBitmap = Bitmap.createScaledBitmap(b, 1000, 750, false);
	 	    	}else{
	 	    		resizedBitmap = Bitmap.createScaledBitmap(b, 750, 1000, false);
	 	    	}
	 	    	resizedBitmap.compress(CompressFormat.JPEG, 80 /*ignored for PNG*/, bos);	 	 
	 	    	
	 	    	byte[] ba = bos.toByteArray();
	 	    	
	 	    	try {
					
					//String reply=new RESTful(getActivity()).postMultyPartData(getResources().getString(R.string.api_end_point)+"photo", builder);
					List <NameValuePair> nvps = new ArrayList <NameValuePair>();
					nvps.add(new BasicNameValuePair("photoType", ""+getArguments().getInt("type_id")));
					nvps.add(new BasicNameValuePair("client_id", Photos.client_id));
					nvps.add(new BasicNameValuePair("projet_id", Photos.projet_id));
//					String str = Base64.encodeToString(ba, Base64.DEFAULT); 
					String str = ba.toString();
					nvps.add(new BasicNameValuePair("file", str));
					
					String reply=new RESTful(getActivity()).postServerMultipart(getResources().getString(R.string.api_end_point)+"photo", nvps, ba);
			        System.out.println("json_cnst-1: " + reply);

			        reply=reply.replace(getResources().getString(R.string.json_cnst), ""); 
			        System.out.println("json_cnst-2: " + reply);

			        JSONArray data = new JSONArray(reply);
					
				} catch (IOException e) {
					if(e.getMessage().equals("403")){
						status_flag=5;
					}else if(e.getMessage().equals("201")){
						status_flag=2;
					}else if(e.getMessage().equals("500")){
						status_flag=2;
					}
					else{
						status_flag=3;
					}
					e.printStackTrace();
				} catch (JSONException e) {
					status_flag=4;
				}catch (NullPointerException e) {
					status_flag=6;
				}
	 	   }
								 				 	 			 	
			return null;
		}

		@Override
		public void onPostExecute(Void m_adapter){               	
			pd.dismiss();
			if(status_flag==2){
				getActivity().getSupportFragmentManager().beginTransaction().remove(CapturedPhoto.this).commit();
				Photos p=(Photos)getActivity().getSupportFragmentManager().findFragmentByTag("client_photos");
				if(p!=null){
					p.refreshPhotos(getArguments().getInt("type_id"));
				}
				p=(Photos)getActivity().getSupportFragmentManager().findFragmentByTag("prospect_photos");
				if(p!=null){
					p.refreshPhotos(getArguments().getInt("type_id"));
				}				
		 	}else if(status_flag==3){
		 		new AlertDialog.Builder(getActivity())
				.setMessage("Erreur de connexion. Veuillez réessayer à nouveau")
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					
					public void onClick(DialogInterface dialog, int id) {							
									
					}
				}).setIcon(android.R.drawable.stat_notify_error).show();
		 	}else if(status_flag==4){
		 		new AlertDialog.Builder(getActivity())
				.setMessage("Erreur du serveur. Veuillez réessayer à nouveau")
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					
					public void onClick(DialogInterface dialog, int id) {							
									
					}
				}).setIcon(android.R.drawable.stat_notify_error).show();
		 	}else if(status_flag==5){
		 		new AlertDialog.Builder(getActivity())
				.setMessage("Paramètres de connexion incorrects")
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					
					public void onClick(DialogInterface dialog, int id) {							
									
					}
				}).setIcon(android.R.drawable.stat_notify_error).show();
		 	}else if(status_flag==6){
		 		new AlertDialog.Builder(getActivity())
				.setMessage("Aucune donnée reçue . S'il vous plait réessayer plus tard.")
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					
					public void onClick(DialogInterface dialog, int id) {							
									
					}
				}).setIcon(android.R.drawable.stat_notify_error).show();
		 	}
		}
	}
}
