//  iCollMobile
//
//  Created by Khaled Noui
//  Copyright (c) 2014 digiwabi. All rights reserved.
//

package com.kb.icoll;

import java.io.IOException;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.kb.icoll.utils.ClientDetailsData;
import com.kb.icoll.utils.RESTful;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;

public class ClientDetails extends Fragment implements OnClickListener{
	TextView header,resume,projet,photos,ticket,plan;
	static ClientDetailsData cdd;
	String client_id,projet_id;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.client_details, container,false);
		
		client_id=getArguments().getString("client_id");
		
		TextView back=(TextView)view.findViewById(R.id.tv_back);
		back.setOnClickListener(this);
		
		header=(TextView)view.findViewById(R.id.tv_header);
		
		resume=(TextView)view.findViewById(R.id.tv_resume);
		resume.setOnClickListener(this);
		projet=(TextView)view.findViewById(R.id.tv_projet);
		projet.setOnClickListener(this);
		photos=(TextView)view.findViewById(R.id.tv_photos);
		photos.setOnClickListener(this);
		ticket=(TextView)view.findViewById(R.id.tv_tickets);
		ticket.setOnClickListener(this); 
		plan=(TextView)view.findViewById(R.id.tv_plan);
		plan.setOnClickListener(this); 
		
		if(new RESTful(getActivity()).checkNetworkConnection()){
			new GetClientDetails().execute("");
		}
		
		/*if(getArguments().getBoolean("is_from_alert", false)){
			//setSelected(2);
			System.out.println("*******");
			photos.setBackgroundColor(getResources().getColor(R.color.theme_color));
			photos.setTextColor(getResources().getColor(R.color.white));
			resume.setBackgroundResource(0);
			resume.setTextColor(getResources().getColor(R.color.theme_color));
			Photos cp= (Photos)getActivity().getSupportFragmentManager().findFragmentByTag("client_photos");
			if(cp==null){
				Photos p=new Photos();
				Bundle b=new Bundle();
				b.putString("client_id", client_id);
				b.putString("projet_id", getArguments().getString("projet_id"));
				b.putString("photo_calling", "alert");
				p.setArguments(b);
				getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.ll_client_sub_container, p, "client_photos").commit();
			}
		}else{
			System.out.println("+++++++++");
			getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.ll_client_sub_container, new ClientResume(), "client_resume").commit();
		}*/
		return view;
	}
	
	private class GetClientDetails extends AsyncTask<String,Void,Void> {
		ProgressDialog pd;
		@SuppressWarnings("unused")
		int status_flag,total_alert,new_alert,non_statues,no_projets_concernes,
			rendez_confirms,no_photos,no_ticket,no_changement,no_sav_du_jour;

		@Override
		public void onPreExecute(){
			pd=new ProgressDialog(getActivity());
			pd.setMessage(getResources().getString(R.string.PLEASE_WAIT));
			pd.setCanceledOnTouchOutside(false);
			pd.show();
		}

		@Override
		protected Void doInBackground(String... params1) {								
			try {
				String reply=new RESTful(getActivity()).queryServer(getResources().getString(R.string.api_end_point)+"client?client_id="+client_id, true);
		        
				System.out.println("json_cnst-1: " + reply);

		        reply=reply.replace(getResources().getString(R.string.json_cnst), ""); 
		        System.out.println("json_cnst-2: " + reply);

		        JSONArray data = new JSONArray(reply);
				JSONObject jo=data.getJSONObject(0);
				cdd=new ClientDetailsData();
				projet_id=jo.getString("projet_id");
				cdd.commande_id=jo.getString("commande_id");
				cdd.name=jo.getString("client_nom");
				cdd.first_name=jo.getString("client_prenom");
				cdd.address=jo.getString("projet_adresse");
				cdd.zip=jo.getString("projet_code_postal");
				cdd.ville=jo.getString("projet_ville");
				cdd.projet_etape=jo.getString("projet_etape");
				cdd.projet_etat=jo.getString("projet_etat");
				cdd.type_of_package=jo.getString("categorie_package_diminutif");
				cdd.package_name=jo.getString("package_nom");
				cdd.email=jo.getString("client_email");
				cdd.telephone=jo.getString("client_tel_fixe");
				cdd.creation_date=jo.getString("client_date_creation");
				cdd.client_situation_professionnelle=jo.getString("client_situation_professionnelle");
				cdd.client_date_naissance=jo.getString("client_date_naissance");
				cdd.client_lieu_naissance=jo.getString("client_lieu_naissance");
				cdd.situation_maritale=jo.getString("client_situation_maritale");
				cdd.no_enfant=jo.getString("client_nbr_enfant");
				cdd.no_enfant_charge=jo.getString("client_nbr_enfant_charge");
				cdd.credit_impot=jo.getString("client_credit_impot");
				cdd.kit_fiscal=jo.getString("client_kit_fiscal");
				cdd.nom_conjoint=jo.getString("client_nom_conjoint");
				cdd.prenom_conjoint=jo.getString("client_prenom_conjoint");
				cdd.date_conjoint=jo.getString("client_date_naissance_conjoint");
				cdd.lieu_conjoint=jo.getString("client_lieu_naissance_conjoint");
				//cdd.nationality_conjoint=jo.getString("client_prenom_conjoint");
				cdd.no_commande=jo.getString("commande_num");
				cdd.date_commande=jo.getString("commande_date");
				cdd.category_package=jo.getString("categorie_package_diminutif");
				cdd.kit_choisi=jo.getString("package_nom");
				cdd.total_wc=jo.getString("package_puissance");
				cdd.forfait_racco=jo.getString("package_forfait_raccordement");
				cdd.montant_de_la=jo.getString("commande_montant_vente");
				cdd.tarif_de=jo.getString("commande_tarif_rachat_erdf");
				//cdd.acces_util=jo.getString("");
				//cdd.nom_dutil=jo.getString("");
				//cdd.mot_de_passe=jo.getString("");
				if(jo.getString("intervenant_commercial_nom").length()==0){
					cdd.commercial=jo.getString("intervenant_commercial_prenom");
				}else{
					cdd.commercial=jo.getString("intervenant_commercial_nom")+" "+jo.getString("intervenant_commercial_prenom");
				}			
				cdd.telephone_commercial=jo.getString("intervenant_commercial_tel");
				if(jo.getString("intervenant_administratif_gestion_nom").length()==0){
					cdd.admin_gestion=jo.getString("intervenant_administratif_gestion_prenom");
				}else{
					cdd.admin_gestion=jo.getString("intervenant_administratif_gestion_nom")+" "+jo.getString("intervenant_administratif_gestion_prenom");
				}				
				cdd.tele_admin_gestion=jo.getString("intervenant_administratif_gestion_tel");
				if(jo.getString("intervenant_administratif_financier_nom").length()==0){
					cdd.admin_financier=jo.getString("intervenant_administratif_financier_prenom");;
				}else{
					cdd.admin_financier=jo.getString("intervenant_administratif_financier_nom")+" "+jo.getString("intervenant_administratif_financier_prenom");
				}				
				cdd.tele_admin_financier=jo.getString("intervenant_administratif_financier_tel");
				if(jo.getString("intervenant_equipe_technique_nom").length()==0){
					cdd.equipe_tech=jo.getString("intervenant_equipe_technique_prenom");
				}else{
					cdd.equipe_tech=jo.getString("intervenant_equipe_technique_nom")+" "+jo.getString("intervenant_equipe_technique_prenom");
				}				
				cdd.tele_equipe_tech=jo.getString("intervenant_equipe_technique_tel");
				cdd.etat_client=jo.getString("projet_etat_client");
				cdd.etat_finance=jo.getString("projet_etat_financement");
				cdd.finance_type=jo.getString("projet_type_financement");
				cdd.org_finance=jo.getString("projet_organisme_financement");
				cdd.date_denvoi=jo.getString("projet_date_envoi_org_financement");
				cdd.numero_de_recommend=jo.getString("projet_num_recom_org_financement");
				cdd.date_de_laccord=jo.getString("projet_date_accord_org_financement");
				cdd.etat_de_travaux=jo.getString("projet_etat_declaration_travaux");
				cdd.date_denvoi_recommend=jo.getString("projet_date_envoi_recommande");
				//cdd.date_de_rec_du=jo.getString("");
				cdd.date_de_rec_mairie=jo.getString("projet_date_reception_recepisse_recommande_mairie");
				cdd.comp_mairie_recu=jo.getString("projet_date_demande_complement_mairie");
				cdd.comp_des_abf=jo.getString("projet_date_demande_complement_abf");
				cdd.date_de_fin=jo.getString("projet_date_fin_instruction");
				cdd.numero_de_dp=jo.getString("projet_num_dp_client");
				
				cdd.projet_etat_installation=jo.getString("projet_etat_installation");
				cdd.date_remise_dossier_previsite=jo.getString("projet_date_dossier_previsite");
				//cdd.date_de_rendezvous_pour_devis=jo.getString(""); //tag not found
				cdd.date_de_reception_previsite=jo.getString("projet_date_previsite");
				cdd.date_d_installation=jo.getString("projet_date_installation");
				cdd.date_de_fin_travaux=jo.getString("projet_date_fin_travaux");
				cdd.projet_etat_raccordment=jo.getString("projet_etat_raccordement");
				cdd.projet_date_raccordment=jo.getString("projet_date_raccordement");
				cdd.projet_etat_mise_en_service=jo.getString("projet_etat_mise_en_service");
				//cdd.projet_date_mise_en_service=jo.getString("");//tag not found
				cdd.projet_etat_cloture=jo.getString("projet_etat_cloture");

				
				// projet data //
				cdd.projet_proprietaire=jo.getString("projet_proprio");
				cdd.projet_residence_principale=jo.getString("projet_residence_principale");
				cdd.projet_location=jo.getString("projet_location");
				cdd.projet_residence_professionalle=jo.getString("projet_residence_professionelle");
				cdd.projet_corporate=jo.getString("projet_coproprietaire");
				cdd.projet_date_of_achievement=jo.getString("projet_date_achevement_maison");
			
				// cdd.projet_exposition=jo.getString("projet_proprio");
				// cdd.projet_inclinasion=jo.getString("projet_proprio");
				// cdd.projet_type_de_cloture=jo.getString("projet_proprio");
				// cdd.projet_largeur_maison=jo.getString("projet_proprio");
				// cdd.projet_hauter_maison=jo.getString("projet_proprio");
				// cdd.projet_profondeur_maison=jo.getString("projet_proprio");
				// cdd.projet_superfice_maison=jo.getString("projet_proprio");
				// cdd.projet_superfice_toit=jo.getString("projet_proprio");
				// cdd.projet_superfice_terrain=jo.getString("projet_proprio");
				// cdd.projet_hauteur_du_sol=jo.getString("projet_proprio");

				try{
					cdd.lat=jo.getDouble("client_latitude");	
				}catch(JSONException e){
					cdd.lat=0;
				}
				try{
					cdd.lon=jo.getDouble("client_longitude");	
				}catch(JSONException e){
					cdd.lon=0;
				}
				status_flag=2;
			} catch (IOException e) {
				if(e.getMessage().equals("403")){
					status_flag=5;
				}else{
					status_flag=3;
				}
				e.printStackTrace();
			} catch (JSONException e) {
				status_flag=4;
			}catch (NullPointerException e) {
				status_flag=6;
			}
					 				 	 			 	
		     return null;
		}

		@Override
		public void onPostExecute(Void m_adapter){               	
			pd.dismiss();
			if(status_flag==2){
				header.setText(cdd.name+" "+cdd.first_name);
				if(getArguments().getString("is_from_alert").equals("photo")){ 
					photos.setBackgroundColor(getResources().getColor(R.color.theme_color));
					photos.setTextColor(getResources().getColor(R.color.white));
					resume.setBackgroundResource(0);
					resume.setTextColor(getResources().getColor(R.color.theme_color));
					Photos p=new Photos();
					Bundle b=new Bundle();
					b.putString("client_id", client_id);
					b.putString("projet_id", projet_id );
					b.putString("photo_calling", "alert");
					p.setArguments(b);
					getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.ll_client_sub_container, p, "client_photos").commit();
				}else if(getArguments().getString("is_from_alert").equals("ticket")){
					ticket.setBackgroundColor(getResources().getColor(R.color.theme_color));
					ticket.setTextColor(getResources().getColor(R.color.white));
					resume.setBackgroundResource(0);
					resume.setTextColor(getResources().getColor(R.color.theme_color));
					ClientTickets ct=new ClientTickets();
					Bundle b=new Bundle();
					b.putString("projet_id", projet_id);
					ct.setArguments(b);
					getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.ll_client_sub_container, ct, "client_tickets").commit(); 
				}else{
					System.out.println("+++++++++");
					getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.ll_client_sub_container, new ClientResume(), "client_resume").commit();
				}
				
				//getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.ll_client_sub_container, new ClientResume(), "client_resume").commit();
		 	}else if(status_flag==3){
		 		new AlertDialog.Builder(getActivity())
				.setMessage("Erreur de connexion. Veuillez réessayer à nouveau")
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int id) {							
						getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, new Clients(), "clients").commit();
					}
				}).setIcon(android.R.drawable.stat_notify_error).show();
		 	}else if(status_flag==4){
		 		new AlertDialog.Builder(getActivity())
				.setMessage("Erreur du serveur. Veuillez réessayer à nouveau")
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int id) {							
						getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, new Clients(), "clients").commit();		
					}
				}).setIcon(android.R.drawable.stat_notify_error).show();
		 	}else if(status_flag==5){
		 		new AlertDialog.Builder(getActivity())
				.setMessage("Paramètres de connexion incorrects")
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int id) {							
						getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, new Clients(), "clients").commit();
					}
				}).setIcon(android.R.drawable.stat_notify_error).show();
		 	}else if(status_flag==6){
		 		new AlertDialog.Builder(getActivity())
				.setMessage("Aucune donnée reçue . S'il vous plait réessayer plus tard.")
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int id) {							
						getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, new Clients(), "clients").commit();
					}
				}).setIcon(android.R.drawable.stat_notify_error).show();
		 	}
		}
	}

	@Override
	public void onClick(View v) {
		int id = v.getId();
		if (id == R.id.tv_back) {
			getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, new Clients(), "clients").commit();
		} else if (id == R.id.tv_resume) {
			setSelected(0);
		} else if (id == R.id.tv_projet) {
			setSelected(1);
		} else if (id == R.id.tv_photos) {
			setSelected(2);
		} else if (id == R.id.tv_tickets) {
			setSelected(3);
		} else if (id == R.id.tv_plan) {
			//setSelected(4);
			Plan cp= (Plan)getActivity().getSupportFragmentManager().findFragmentByTag("plan");
			if(cp==null){
				Plan p=new Plan();
				Bundle b=new Bundle();
				b.putString("from", "client");
				p.setArguments(b);
				getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.content_frame1, p, "plan").commit();
			}
		}
	}
	
	private void setSelected(int i){
		resume.setBackgroundResource(0);
		resume.setTextColor(getResources().getColor(R.color.theme_color));
		projet.setBackgroundResource(0);
		projet.setTextColor(getResources().getColor(R.color.theme_color));
		photos.setBackgroundResource(0);
		photos.setTextColor(getResources().getColor(R.color.theme_color));
		ticket.setBackgroundResource(0);
		ticket.setTextColor(getResources().getColor(R.color.theme_color));
		plan.setBackgroundResource(0);
		plan.setTextColor(getResources().getColor(R.color.theme_color));
		if(i==0){
			resume.setBackgroundResource(R.drawable.orange_solid_left_rounded);
			resume.setTextColor(getResources().getColor(R.color.white));
			ClientResume cr= (ClientResume)getActivity().getSupportFragmentManager().findFragmentByTag("client_resume");
			if(cr==null){
				getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.ll_client_sub_container, new ClientResume(), "client_resume").commit();
			}
		}else if(i==1){
			projet.setBackgroundColor(getResources().getColor(R.color.theme_color));
			projet.setTextColor(getResources().getColor(R.color.white));
			ClientProjet cr= (ClientProjet)getActivity().getSupportFragmentManager().findFragmentByTag("client_projet");
			if(cr==null){
				getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.ll_client_sub_container, new ClientProjet(), "client_projet").commit();
			}
		}else if(i==2){
			photos.setBackgroundColor(getResources().getColor(R.color.theme_color));
			photos.setTextColor(getResources().getColor(R.color.white));
			Photos cp= (Photos)getActivity().getSupportFragmentManager().findFragmentByTag("client_photos");
			if(cp==null){
				Photos p=new Photos();
				Bundle b=new Bundle();
				b.putString("client_id", client_id);
				b.putString("projet_id", projet_id);
				//b.putString("projet_id", getArguments().getString("projet_id"));
				b.putString("photo_calling", "client");
				p.setArguments(b);
				getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.ll_client_sub_container, p, "client_photos").commit();
			}
		}else if(i==3){
			ticket.setBackgroundColor(getResources().getColor(R.color.theme_color));
			ticket.setTextColor(getResources().getColor(R.color.white));
			ClientTickets ct= (ClientTickets)getActivity().getSupportFragmentManager().findFragmentByTag("client_tickets");
			if(ct==null){
				ct=new ClientTickets();
				Bundle b=new Bundle();
				b.putString("projet_id", projet_id);
				ct.setArguments(b);
				getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.ll_client_sub_container, ct, "client_tickets").commit(); 
			}
		}/*else if(i==4){
			plan.setBackgroundResource(R.drawable.orange_solid_right_rounded);
			plan.setTextColor(getResources().getColor(R.color.white));
			Plan cp= (Plan)getActivity().getSupportFragmentManager().findFragmentByTag("plan");
			if(cp==null){
				getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.ll_client_sub_container, new Plan(), "plan").commit();
			}
		}*/		

			
	}
}
