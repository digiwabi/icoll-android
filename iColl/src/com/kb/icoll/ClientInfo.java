//  iCollMobile
//
//  Created by Khaled Noui
//  Copyright (c) 2014 digiwabi. All rights reserved.
//

package com.kb.icoll;

import com.kb.icoll.utils.ClientDetailsData;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;

public class ClientInfo extends Fragment implements OnClickListener{
	TextView name,address,zip_ville,etape_etat,type_de_package,package_choisi,email,telephone;
	ClientDetailsData cdd;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.client_info, container,false);
		
		name=(TextView)view.findViewById(R.id.tv_name);
		address=(TextView)view.findViewById(R.id.tv_address);
		zip_ville=(TextView)view.findViewById(R.id.tv_zip_ville);
		etape_etat=(TextView)view.findViewById(R.id.tv_etape_etat);
		type_de_package=(TextView)view.findViewById(R.id.tv_type_of_package);
		package_choisi=(TextView)view.findViewById(R.id.tv_package_choisi);
		
		telephone=(TextView)view.findViewById(R.id.tv_telephone);
		telephone.setOnClickListener(this);
		
		email=(TextView)view.findViewById(R.id.tv_email);
		email.setOnClickListener(this);
		
		if(ClientDetails.cdd!=null){
			cdd=ClientDetails.cdd;
			name.setText(cdd.first_name+" "+cdd.name); 
			address.setText(cdd.address);
			zip_ville.setText(cdd.zip+" "+cdd.ville);
			etape_etat.setText("Dernier état modifié : "+cdd.projet_etape+" "+cdd.projet_etat);
			type_de_package.setText("Type de package : "+cdd.type_of_package);
			package_choisi.setText("Package choisi : "+cdd.package_name);
			email.setText("Email : "+cdd.email);
			telephone.setText("Téléphone fixe : "+cdd.telephone);
		}
		return view;
	}

	@Override
	public void onClick(View v) {
		int id = v.getId();
		if (id == R.id.tv_email) {
			if(!cdd.email.equalsIgnoreCase("non renseigné")){
				Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
			            "mailto",cdd.email.trim(), null));
				emailIntent.putExtra(Intent.EXTRA_SUBJECT, "iColl");
				startActivity(Intent.createChooser(emailIntent, "Send email..."));
			}
		} else if (id == R.id.tv_telephone) {
			if(cdd.telephone.length()>0){
				String uri = "tel:" + cdd.telephone ;
				Intent intent = new Intent(Intent.ACTION_DIAL);
				intent.setData(Uri.parse(uri));
				startActivity(intent);
			}
		}
	}
}
