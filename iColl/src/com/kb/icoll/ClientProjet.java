//  iCollMobile
//
//  Created by Khaled Noui
//  Copyright (c) 2014 digiwabi. All rights reserved.
//

package com.kb.icoll;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.FrameLayout;

public class ClientProjet extends Fragment implements OnClickListener{
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.client_projet, container,false);
		
		getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.ll_client_info_container, new ClientInfo(),"client_info").commit();
		
		FrameLayout information_du_projet=(FrameLayout)view.findViewById(R.id.fl_informations_client);
		information_du_projet.setOnClickListener(this);
		
		FrameLayout information_du_tech=(FrameLayout)view.findViewById(R.id.fl_informations_tech_du_projet);
		information_du_tech.setOnClickListener(this);
		
		FrameLayout sav=(FrameLayout)view.findViewById(R.id.fl_sav_reclamation);
		sav.setOnClickListener(this);
		return view;
	}

	@Override
	public void onClick(View v) {
		int id = v.getId();
		if (id == R.id.fl_informations_client) {
			getActivity().getSupportFragmentManager().beginTransaction()
			.replace(R.id.content_frame1, new ClientProjetInfoDuProjet(), "client_projet_info_du_projet").commit();
		} else if (id == R.id.fl_informations_tech_du_projet) {
			getActivity().getSupportFragmentManager().beginTransaction()
			.replace(R.id.content_frame1, new ClientProjetInfoTechDuProjet(), "client_projet_info_tech_du_projet").commit();
		} else if (id == R.id.fl_sav_reclamation) {
			getActivity().getSupportFragmentManager().beginTransaction()
			.replace(R.id.content_frame1, new ClientProjetSavReclamation(), "client_projet_sav_reclamation").commit();
		}
	}
}
