//  iCollMobile
//
//  Created by Khaled Noui
//  Copyright (c) 2014 digiwabi. All rights reserved.
//

package com.kb.icoll;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.TextView;

public class ClientProjetInfoDuProjet extends Fragment implements OnClickListener{
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.client_projet_informations_du_projet, container,false);
		
		getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.ll_client_info_container1, new ClientInfo(),"client_info").commit();
		
		TextView header=(TextView)view.findViewById(R.id.tv_header);
		header.setText(ClientDetails.cdd.name+" "+ClientDetails.cdd.first_name);
		
		TextView address=(TextView)view.findViewById(R.id.tv_address);
		address.setText(ClientDetails.cdd.address);
		
		TextView postal_code=(TextView)view.findViewById(R.id.tv_postal_code);
		postal_code.setText(ClientDetails.cdd.zip);
		
		TextView ville=(TextView)view.findViewById(R.id.tv_ville);
		ville.setText(ClientDetails.cdd.ville);
		
		TextView proprietaire=(TextView)view.findViewById(R.id.tv_proprio);
		proprietaire.setText(ClientDetails.cdd.projet_proprietaire);
		
		TextView res_principale=(TextView)view.findViewById(R.id.tv_residence_principale);
		res_principale.setText(ClientDetails.cdd.projet_residence_principale);
		
		TextView location=(TextView)view.findViewById(R.id.tv_location);
		location.setText(ClientDetails.cdd.projet_location);
		
		TextView res_professionelle=(TextView)view.findViewById(R.id.tv_residence_professionalle);
		res_professionelle.setText(ClientDetails.cdd.projet_residence_professionalle);
		
		TextView corporate=(TextView)view.findViewById(R.id.tv_corporate);
		corporate.setText(ClientDetails.cdd.projet_corporate);
		
		TextView date_of_achievement=(TextView)view.findViewById(R.id.tv_date_of_achievement);
		date_of_achievement.setText(ClientDetails.cdd.projet_date_of_achievement);
		
		TextView back=(TextView)view.findViewById(R.id.tv_back);
		back.setOnClickListener(this); 
		
		return view;
	}
	
	@Override
	public void onClick(View v) {
		int id = v.getId();
		if (id == R.id.tv_back) {
			getActivity().getSupportFragmentManager().beginTransaction().remove(this).commit();
		}
	}
}
