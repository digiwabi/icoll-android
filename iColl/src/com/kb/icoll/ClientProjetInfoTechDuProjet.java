//  iCollMobile
//
//  Created by Khaled Noui
//  Copyright (c) 2014 digiwabi. All rights reserved.
//

package com.kb.icoll;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.TextView;

public class ClientProjetInfoTechDuProjet extends Fragment implements OnClickListener{
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.client_projet_info_techniques_du_project, container,false);
		
		getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.ll_client_info_container1, new ClientInfo(),"client_info").commit();
		
		TextView header=(TextView)view.findViewById(R.id.tv_header);
		header.setText(ClientDetails.cdd.name+" "+ClientDetails.cdd.first_name);
		
		TextView back=(TextView)view.findViewById(R.id.tv_back);
		back.setOnClickListener(this); 
		
		return view;
	}
	
	@Override
	public void onClick(View v) {
		int id = v.getId();
		if (id == R.id.tv_back) {
			getActivity().getSupportFragmentManager().beginTransaction().remove(this).commit();
		}
	}
}
