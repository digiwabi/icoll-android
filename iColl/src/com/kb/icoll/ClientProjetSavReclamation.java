//  iCollMobile
//
//  Created by Khaled Noui
//  Copyright (c) 2014 digiwabi. All rights reserved.
//

package com.kb.icoll;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.kb.icoll.utils.RESTful;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class ClientProjetSavReclamation extends Fragment implements OnClickListener{
	
	ListView lv_sav_reclamation;
	ArrayList<SAVReclamationData> al_sav_rec_data;
	public static SAVReclamationData srd;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.client_projet_sav_reclamation, container,false);
		
		getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.ll_client_info_container1, new ClientInfo(),"client_info").commit();
		
		TextView header=(TextView)view.findViewById(R.id.tv_header);
		header.setText(ClientDetails.cdd.name+" "+ClientDetails.cdd.first_name);
		
		TextView back=(TextView)view.findViewById(R.id.tv_back);
		back.setOnClickListener(this);
		
		lv_sav_reclamation=(ListView)view.findViewById(R.id.lv_sav_reclamation);
		
		new GetSAVReclamationDetails().execute("");
		
		return view;
	}
	
	@Override
	public void onClick(View v) {
		int id = v.getId();
		if (id == R.id.tv_back) {
			getActivity().getSupportFragmentManager().beginTransaction().remove(this).commit();
		}
	}
	
	private class GetSAVReclamationDetails extends AsyncTask<String,Void,Void> {
		ProgressDialog pd;
		int status_flag;
		
		
		@Override
		public void onPreExecute(){
			pd=new ProgressDialog(getActivity());
			pd.setMessage(getResources().getString(R.string.PLEASE_WAIT));
			pd.setCanceledOnTouchOutside(false);
			pd.show();
		}

		@Override
		protected Void doInBackground(String... params1) {								
			try {
				String reply=new RESTful(getActivity()).queryServer(getResources().getString(R.string.api_end_point)+"incident?commande_id="+ClientDetails.cdd.commande_id, true);
		        System.out.println("json_cnst-1: " + reply);

		        reply=reply.replace(getResources().getString(R.string.json_cnst), "");
		        System.out.println("json_cnst-2: " + reply);

				JSONArray data = new JSONArray(reply);
				al_sav_rec_data=new ArrayList<ClientProjetSavReclamation.SAVReclamationData>();
				for(int i=0;i<data.length();i++){
					JSONObject jo=data.getJSONObject(i);					
					srd=new SAVReclamationData();
					srd.id_commande=jo.getString("id_commande");
					srd.date_creation=jo.getString("date_creation");
					srd.incident_id=jo.getString("incident_id");
					srd.incident_commentaire=jo.getString("incident_commentaire");
					srd.incident_commentaire_intervenant=jo.getString("incident_commentaire_intervenant");
					srd.incident_date_intervention=jo.getString("incident_date_intervention");
					srd.incident_etat=jo.getString("incident_etat");
					srd.incident_heure_intervention=jo.getString("incident_heure_intervention");
					srd.incident_intervenant=jo.getString("incident_intervenant");
					srd.incident_probleme=jo.getString("incident_probleme");
					srd.incident_type=jo.getString("incident_type");
					
					al_sav_rec_data.add(srd);
					
				}
				System.out.println("Size of sav array: "+al_sav_rec_data.size());
				status_flag=2;
			} catch (IOException e) {
				if(e.getMessage().equals("403")){
					status_flag=5;
				}else{
					status_flag=3;
				}
				e.printStackTrace();
			} catch (JSONException e) {
				status_flag=4;
			}catch (NullPointerException e) {
				status_flag=6;
			}										 				 	 			 	
			return null;
		}

		@Override
		public void onPostExecute(Void m_adapter){               	
			pd.dismiss();
			if(status_flag==2){
				
				implementListView(al_sav_rec_data);
				
		 	}else if(status_flag==3){
		 		new AlertDialog.Builder(getActivity())
				.setMessage("Erreur de connexion. Veuillez réessayer à nouveau")
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					
					public void onClick(DialogInterface dialog, int id) {							
									
					}
				}).setIcon(android.R.drawable.stat_notify_error).show();
		 	}else if(status_flag==4){
		 		new AlertDialog.Builder(getActivity())
				.setMessage("Erreur du serveur. Veuillez réessayer à nouveau")
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					
					public void onClick(DialogInterface dialog, int id) {							
									
					}
				}).setIcon(android.R.drawable.stat_notify_error).show();
		 	}else if(status_flag==5){
		 		new AlertDialog.Builder(getActivity())
				.setMessage("Paramètres de connexion incorrects")
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					
					public void onClick(DialogInterface dialog, int id) {							
									
					}
				}).setIcon(android.R.drawable.stat_notify_error).show();
		 	}else if(status_flag==6){
		 		new AlertDialog.Builder(getActivity())
				.setMessage("Aucune donnée reçue . S'il vous plait réessayer plus tard.")
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					
					public void onClick(DialogInterface dialog, int id) {							
									
					}
				}).setIcon(android.R.drawable.stat_notify_error).show();
		 	}
		}
	}
	
	public class SAVReclamationData implements Serializable{
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		String id_commande,date_creation,incident_id,incident_type,incident_probleme,incident_etat,incident_commentaire,incident_date_intervention,
		incident_heure_intervention,incident_intervenant,incident_commentaire_intervenant;
	}
	
	private void implementListView(ArrayList<SAVReclamationData> al) {
		ArrayAdapter<SAVReclamationData> ad = new MyAdapter(getActivity(),
				R.layout.ticket_cell, al);	
		lv_sav_reclamation.setAdapter(ad);
		ad.notifyDataSetChanged();
		lv_sav_reclamation.setTextFilterEnabled(true);
	}
	
	private class MyAdapter extends ArrayAdapter<SAVReclamationData>{
		ArrayList<SAVReclamationData> my_al;
		
		public MyAdapter(Context context, int textViewResourceId, ArrayList<SAVReclamationData> al) {
			super(context, textViewResourceId, al);	
			my_al=al;
		}
				
		@Override
		public View getView(final int position, View convertView, ViewGroup parent) {
			View v = convertView;
			
			if (v == null) {				
				LayoutInflater vi = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);				
		        v = vi.inflate(R.layout.client_sav_reclamation_cell, null);		        			        				
			}
			
			TextView sav=(TextView)v.findViewById(R.id.tv_sav);
			sav.setText(my_al.get(position).incident_type+" - "+my_al.get(position).incident_probleme); 
			
			
			v.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					ClientSAVReclamationDetails cl=new ClientSAVReclamationDetails();
					Bundle b=new Bundle();
					b.putSerializable("sav_data", my_al.get(position));
					cl.setArguments(b);
					getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.content_frame1, cl, "client_sav_reclamation_details").commit();
				}
			});
			return v;
		}
	}
	
}
