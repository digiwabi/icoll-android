//  iCollMobile
//
//  Created by Khaled Noui
//  Copyright (c) 2014 digiwabi. All rights reserved.
//

package com.kb.icoll;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

public class ClientResume extends Fragment implements OnClickListener{
	TextView name,address,zip_ville,etape_etat,type_de_package,package_choisi,email;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.client_resume, container,false);
		
		getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.ll_client_info_container, new ClientInfo(),"client_info").commit();
		
		FrameLayout informations_client=(FrameLayout)view.findViewById(R.id.fl_informations_client);
		informations_client.setOnClickListener(this);
		
		FrameLayout informations_de_commande=(FrameLayout)view.findViewById(R.id.fl_informations_de_commande);
		informations_de_commande.setOnClickListener(this);
		
		FrameLayout etats_du_projet=(FrameLayout)view.findViewById(R.id.fl_etats_du_projet);
		etats_du_projet.setOnClickListener(this);
		
		FrameLayout liste_des_intervenants=(FrameLayout)view.findViewById(R.id.fl_liste_des_intervenants);
		liste_des_intervenants.setOnClickListener(this);
		return view;
	}

	@Override
	public void onClick(View v) {
		int id = v.getId();
		if (id == R.id.fl_informations_client) {
			getActivity().getSupportFragmentManager().beginTransaction()
			.replace(R.id.content_frame1, new ClientResumeInformationClient(), "client_resume_info_client").commit();
		} else if (id == R.id.fl_informations_de_commande) {
			getActivity().getSupportFragmentManager().beginTransaction()
			.replace(R.id.content_frame1, new ClientResumeInfoDeCommande(), "client_resume_info_de_commande").commit();
		} else if (id == R.id.fl_etats_du_projet) {
			getActivity().getSupportFragmentManager().beginTransaction()
			.replace(R.id.content_frame1, new ClientResumeEtatsDuProjet(), "client_resume_info_du_projet").commit();
		} else if (id == R.id.fl_liste_des_intervenants) {
			getActivity().getSupportFragmentManager().beginTransaction()
			.replace(R.id.content_frame1, new ClientResumeListeDeIntervenants(), "client_resume_liste_des_intervenants").commit();
		}
	}

}
