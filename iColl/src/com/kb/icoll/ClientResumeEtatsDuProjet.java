//  iCollMobile
//
//  Created by Khaled Noui
//  Copyright (c) 2014 digiwabi. All rights reserved.
//

package com.kb.icoll;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;

public class ClientResumeEtatsDuProjet extends Fragment implements OnClickListener{ 
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.client_resume_etats_du_projet, container,false);
		
		getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.ll_client_info_container1, new ClientInfo(),"client_info").commit();
		
		TextView header=(TextView)view.findViewById(R.id.tv_header);
		header.setText(ClientDetails.cdd.name+" "+ClientDetails.cdd.first_name);
		
		TextView back=(TextView)view.findViewById(R.id.tv_back);
		back.setOnClickListener(this); 
		
		TextView etat_client=(TextView)view.findViewById(R.id.tv_etat_client);
		etat_client.setText(ClientDetails.cdd.etat_client);
		
		TextView etat_finance=(TextView)view.findViewById(R.id.tv_etat_finance);
		etat_finance.setText(ClientDetails.cdd.etat_finance);
		
		TextView type_de_finance=(TextView)view.findViewById(R.id.tv_type_de_finance);
		type_de_finance.setText(ClientDetails.cdd.finance_type);
		
		TextView org_finance=(TextView)view.findViewById(R.id.tv_org_finance);
		org_finance.setText(ClientDetails.cdd.org_finance); 
		TextView date_denvoi=(TextView)view.findViewById(R.id.tv_date_denvoi);
		if(ClientDetails.cdd.date_denvoi.length()==0){
			date_denvoi.setText("non renseigné");
		}else{
			date_denvoi.setText(ClientDetails.cdd.date_denvoi);
		}
		
		TextView numero_de=(TextView)view.findViewById(R.id.tv_numero_de);
		if(ClientDetails.cdd.numero_de_recommend.length()==0){
			numero_de.setText("non renseigné");
		}else{
			numero_de.setText(ClientDetails.cdd.numero_de_recommend);
		}		
		TextView date_de_laccord=(TextView)view.findViewById(R.id.tv_date_de_laccord);
		if(ClientDetails.cdd.date_de_laccord.length()==0){
			date_de_laccord.setText("non renseigné");
		}else{
			date_de_laccord.setText(ClientDetails.cdd.date_de_laccord);
		}
		TextView etat_de_la_dec_travaux=(TextView)view.findViewById(R.id.tv_etat_de_la_dec_travaux);
		if(ClientDetails.cdd.etat_de_travaux.length()==0){
			etat_de_la_dec_travaux.setText("non renseigné");
		}else{
			etat_de_la_dec_travaux.setText(ClientDetails.cdd.etat_de_travaux);
		}
		TextView date_denvoi_du_recommend=(TextView)view.findViewById(R.id.tv_date_denvoi_du_recommend);
		if(ClientDetails.cdd.date_denvoi_recommend.length()==0){
			date_denvoi_du_recommend.setText("non renseigné");
		}else{
			date_denvoi_du_recommend.setText(ClientDetails.cdd.date_denvoi_recommend);
		}
		@SuppressWarnings("unused")
		TextView date_de_rec_du=(TextView)view.findViewById(R.id.tv_date_de_rec_du);
//		if(ClientDetails.cdd.date_de_rec_du.length()==0){
//			date_de_rec_du.setText("non renseigné");
//		}else{
//			date_de_rec_du.setText(ClientDetails.cdd.date_de_rec_du);
//		}
		TextView date_de_rec_mairie=(TextView)view.findViewById(R.id.tv_date_de_rec_mairie);
		if(ClientDetails.cdd.date_de_rec_mairie.length()==0){
			date_de_rec_mairie.setText("non renseigné");
		}else{
			date_de_rec_mairie.setText(ClientDetails.cdd.date_de_rec_mairie);
		}
		TextView comp_mairie_recu=(TextView)view.findViewById(R.id.tv_comp_mairie_recu);
		if(ClientDetails.cdd.comp_mairie_recu.length()==0){
			comp_mairie_recu.setText("non renseigné");
		}else{
			comp_mairie_recu.setText(ClientDetails.cdd.comp_mairie_recu);
		}
		TextView comp_des_abf=(TextView)view.findViewById(R.id.tv_comp_des_abf);
		if(ClientDetails.cdd.comp_des_abf.length()==0){
			comp_des_abf.setText("non renseigné");
		}else{
			comp_des_abf.setText(ClientDetails.cdd.comp_des_abf);
		}
		TextView date_de_fin=(TextView)view.findViewById(R.id.tv_date_de_fin);
		if(ClientDetails.cdd.date_de_fin.length()==0){
			date_de_fin.setText("non renseigné");
		}else{
			date_de_fin.setText(ClientDetails.cdd.date_de_fin);
		}
		TextView numero_de_dp=(TextView)view.findViewById(R.id.tv_numero_de_dp);
		if(ClientDetails.cdd.numero_de_dp.length()==0){
			numero_de_dp.setText("non renseigné");
		}else{
			numero_de_dp.setText(ClientDetails.cdd.numero_de_dp);
		}
		
		
		TextView etat_d_installation=(TextView)view.findViewById(R.id.tv_etat_de_installation);
		if(ClientDetails.cdd.projet_etat_installation.length()==0){
			etat_d_installation.setText("non renseigné");
		}else{
			etat_d_installation.setText(ClientDetails.cdd.projet_etat_installation);
		}
		TextView date_remise_previsite=(TextView)view.findViewById(R.id.tv_date_de_remise);
		if(ClientDetails.cdd.date_remise_dossier_previsite.length()==0){
			date_remise_previsite.setText("non renseigné");
		}else{
			date_remise_previsite.setText(ClientDetails.cdd.date_remise_dossier_previsite);
		}
		@SuppressWarnings("unused")
		TextView date_rendezvous_pour_devis=(TextView)view.findViewById(R.id.tv_date_de_rendezvous);
//		if(ClientDetails.cdd.projet_etat_installation.length()==0){
//			etat_d_installation.setText("non renseigné");
//		}else{
//			etat_d_installation.setText(ClientDetails.cdd.projet_etat_installation);
//		}
		TextView date_de_reception_previsite=(TextView)view.findViewById(R.id.tv_date_de_reception);
		if(ClientDetails.cdd.date_de_reception_previsite.length()==0){
			date_de_reception_previsite.setText("non renseigné");
		}else{
			date_de_reception_previsite.setText(ClientDetails.cdd.date_de_reception_previsite);
		}
		TextView date_d_installation=(TextView)view.findViewById(R.id.tv_date_d_installation);
		if(ClientDetails.cdd.date_d_installation.length()==0){
			date_d_installation.setText("non renseigné");
		}else{
			date_d_installation.setText(ClientDetails.cdd.date_d_installation);
		}
		TextView date_de_fin_travaux=(TextView)view.findViewById(R.id.tv_date_de_fin_travaux);
		if(ClientDetails.cdd.date_de_fin_travaux.length()==0){
			date_de_fin_travaux.setText("non renseigné");
		}else{
			date_de_fin_travaux.setText(ClientDetails.cdd.date_de_fin_travaux);
		}
		
		TextView projet_etat_raccordment=(TextView)view.findViewById(R.id.tv_projet_etat_raccordment);
		if(ClientDetails.cdd.projet_etat_raccordment.length()==0){
			projet_etat_raccordment.setText("non renseigné");
		}else{
			projet_etat_raccordment.setText(ClientDetails.cdd.projet_etat_raccordment);
		}
		TextView projet_date_raccordment=(TextView)view.findViewById(R.id.tv_projet_date_raccordment);
		if(ClientDetails.cdd.projet_date_raccordment.length()==0){
			projet_date_raccordment.setText("non renseigné");
		}else{
			projet_date_raccordment.setText(ClientDetails.cdd.projet_date_raccordment);
		}
		
		TextView projet_etat_mise_en_service=(TextView)view.findViewById(R.id.tv_projet_etat_mise_en_service);
		if(ClientDetails.cdd.projet_etat_mise_en_service.length()==0){
			projet_etat_mise_en_service.setText("non renseigné");
		}else{
			projet_etat_mise_en_service.setText(ClientDetails.cdd.projet_etat_mise_en_service);
		}
		@SuppressWarnings("unused")
		TextView projet_date_mise_en_service=(TextView)view.findViewById(R.id.tv_projet_date_mise_en_service);
//		if(ClientDetails.cdd.projet_date_raccordment.length()==0){
//			projet_date_raccordment.setText("non renseigné");
//		}else{
//			projet_date_raccordment.setText(ClientDetails.cdd.projet_date_raccordment);
//		}
		
		TextView projet_etat_cloture=(TextView)view.findViewById(R.id.tv_projet_etat_cloture);
		if(ClientDetails.cdd.projet_etat_cloture.length()==0){
			projet_etat_cloture.setText("non renseigné");
		}else{
			projet_etat_cloture.setText(ClientDetails.cdd.projet_etat_cloture);
		}
		
		return view;
	}
	
	@Override
	public void onClick(View v) {
		int id = v.getId();
		if (id == R.id.tv_back) {
			getActivity().getSupportFragmentManager().beginTransaction().remove(this).commit();
		}
	}
}
