//  iCollMobile
//
//  Created by Khaled Noui
//  Copyright (c) 2014 digiwabi. All rights reserved.
//

package com.kb.icoll;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;

public class ClientResumeInfoDeCommande extends Fragment implements OnClickListener{
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.client_resume_info_de_commande, container,false);
		
		getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.ll_client_info_container1, new ClientInfo(),"client_info").commit();
		
		TextView header=(TextView)view.findViewById(R.id.tv_header);
		header.setText(ClientDetails.cdd.name+" "+ClientDetails.cdd.first_name);
		
		TextView back=(TextView)view.findViewById(R.id.tv_back);
		back.setOnClickListener(this); 
		
		TextView numero_de_commande=(TextView)view.findViewById(R.id.tv_numero_de_commande);
		numero_de_commande.setText(ClientDetails.cdd.no_commande); 
		TextView date_de_commande=(TextView)view.findViewById(R.id.tv_date_de_commende);
		date_de_commande.setText(ClientDetails.cdd.date_commande); 
		TextView categorie_du_package=(TextView)view.findViewById(R.id.tv_categorie_du_package);
		categorie_du_package.setText(ClientDetails.cdd.category_package); 
		TextView kit_choisi=(TextView)view.findViewById(R.id.tv_kit_choisi);
		kit_choisi.setText(ClientDetails.cdd.kit_choisi); 
		TextView total_wc=(TextView)view.findViewById(R.id.tv_total_wc);
		total_wc.setText(ClientDetails.cdd.total_wc); 
		TextView forfait_racco=(TextView)view.findViewById(R.id.tv_forfait_racco);
		forfait_racco.setText(ClientDetails.cdd.forfait_racco); 
		TextView montant_de_la=(TextView)view.findViewById(R.id.tv_montant_de_la);
		montant_de_la.setText(ClientDetails.cdd.montant_de_la); 
		TextView tarif_de=(TextView)view.findViewById(R.id.tv_tarif_de_rechat);
		tarif_de.setText(ClientDetails.cdd.tarif_de); 
		TextView access_util=(TextView)view.findViewById(R.id.tv_acces_util);
		TextView nom_dutil=(TextView)view.findViewById(R.id.tv_nom_dutil);
		TextView mot_de_passe=(TextView)view.findViewById(R.id.tv_mot_de_passe);
		return view;
	}
	
	@Override
	public void onClick(View v) {
		int id = v.getId();
		if (id == R.id.tv_back) {
			getActivity().getSupportFragmentManager().beginTransaction().remove(this).commit();
		}
	}
}
