//  iCollMobile
//
//  Created by Khaled Noui
//  Copyright (c) 2014 digiwabi. All rights reserved.
//

package com.kb.icoll;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;

public class ClientResumeInformationClient extends Fragment implements OnClickListener{

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.client_resume_informations_client, container,false);
		
		getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.ll_client_info_container1, new ClientInfo(),"client_info").commit();
		
		TextView back=(TextView)view.findViewById(R.id.tv_back);
		back.setOnClickListener(this); 
		
		TextView header=(TextView)view.findViewById(R.id.tv_header);
		header.setText(ClientDetails.cdd.name+" "+ClientDetails.cdd.first_name);
		
		TextView creation_date=(TextView)view.findViewById(R.id.tv_date_creation);
		creation_date.setText(ClientDetails.cdd.creation_date);
		
		//type de client missing...
		TextView situation_professionnelle=(TextView)view.findViewById(R.id.tv_situation_professionelle);
		situation_professionnelle.setText(ClientDetails.cdd.client_situation_professionnelle);
		
		TextView client_date_naissance=(TextView)view.findViewById(R.id.tv_date_naissance);
		client_date_naissance.setText(ClientDetails.cdd.client_date_naissance);
		
		TextView client_lieu_naissance=(TextView)view.findViewById(R.id.tv_lieu_naissance);
		if(ClientDetails.cdd.client_lieu_naissance.length()==0){
			client_lieu_naissance.setText("non renseigné");
		}else{
			client_lieu_naissance.setText(ClientDetails.cdd.client_lieu_naissance);
		}	
		
		TextView situation_maritale=(TextView)view.findViewById(R.id.tv_situation_maritale);
		situation_maritale.setText(ClientDetails.cdd.situation_maritale);
		
		TextView no_enfants=(TextView)view.findViewById(R.id.tv_no_enfants);
		no_enfants.setText(ClientDetails.cdd.no_enfant);
		
		TextView no_enfants_charge=(TextView)view.findViewById(R.id.tv_no_enfants_charge);
		no_enfants_charge.setText(ClientDetails.cdd.no_enfant_charge);
		
		TextView credit_impot=(TextView)view.findViewById(R.id.tv_credit_impot);
		credit_impot.setText(ClientDetails.cdd.credit_impot);
		
		TextView kit_fiscal=(TextView)view.findViewById(R.id.tv_kit_fiscal);
		kit_fiscal.setText(ClientDetails.cdd.kit_fiscal);
		
		TextView nom_conjoint=(TextView)view.findViewById(R.id.tv_nom_conjoint);
		if(ClientDetails.cdd.nom_conjoint.length()==0){
			nom_conjoint.setText("non renseigné");
		}else{
			nom_conjoint.setText(ClientDetails.cdd.nom_conjoint);
		}	
		
		TextView prenom_conjoint=(TextView)view.findViewById(R.id.tv_prenom_conjoint);
		if(ClientDetails.cdd.prenom_conjoint.length()==0){
			prenom_conjoint.setText("non renseigné");
		}else{
			prenom_conjoint.setText(ClientDetails.cdd.prenom_conjoint);
		}	
		
		TextView date_conjoint=(TextView)view.findViewById(R.id.tv_date_conjoint);
		date_conjoint.setText(ClientDetails.cdd.date_conjoint);
		
		TextView lieu_conjoint=(TextView)view.findViewById(R.id.tv_lieu_conjoint);
		if(ClientDetails.cdd.lieu_conjoint.length()==0){
			lieu_conjoint.setText("non renseigné");
		}else{
			lieu_conjoint.setText(ClientDetails.cdd.lieu_conjoint);
		}
		
		//nationality conjoint missing
		return view;
	}

	@Override
	public void onClick(View v) {
		int id = v.getId();
		if (id == R.id.tv_back) {
			getActivity().getSupportFragmentManager().beginTransaction().remove(this).commit();
		}
	}
}
