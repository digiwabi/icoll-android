//  iCollMobile
//
//  Created by Khaled Noui
//  Copyright (c) 2014 digiwabi. All rights reserved.
//

package com.kb.icoll;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;

public class ClientResumeListeDeIntervenants extends Fragment implements OnClickListener{
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.client_resume_list_intervenants, container,false);
		
		getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.ll_client_info_container1, new ClientInfo(),"client_info").commit();
		
		TextView header=(TextView)view.findViewById(R.id.tv_header);
		header.setText(ClientDetails.cdd.name+" "+ClientDetails.cdd.first_name);
		
		TextView back=(TextView)view.findViewById(R.id.tv_back);
		back.setOnClickListener(this); 
		
		TextView commercial=(TextView)view.findViewById(R.id.tv_commercial);
		if(ClientDetails.cdd.commercial.length()==0){
			commercial.setText("non renseigné");
		}else{
			commercial.setText(ClientDetails.cdd.commercial);
		}		
		TextView tele_commercial=(TextView)view.findViewById(R.id.tv_tele_commercial);
		if(ClientDetails.cdd.telephone_commercial.length()==0){
			tele_commercial.setText("non renseigné");
		}else{
			tele_commercial.setText(ClientDetails.cdd.telephone_commercial);
		}		
		TextView admin_gestion=(TextView)view.findViewById(R.id.tv_admin_gestion);
		if(ClientDetails.cdd.admin_gestion.length()==0){
			admin_gestion.setText("non renseigné");
		}else{
			admin_gestion.setText(ClientDetails.cdd.admin_gestion);
		}		
		TextView tele_admin_gestion=(TextView)view.findViewById(R.id.tv_tele_admin_gestion);
		if(ClientDetails.cdd.tele_admin_gestion.length()==0){
			tele_admin_gestion.setText("non renseigné");
		}else{
			tele_admin_gestion.setText(ClientDetails.cdd.tele_admin_gestion);
		}		
		TextView admin_financier=(TextView)view.findViewById(R.id.tv_admin_financier);
		if(ClientDetails.cdd.admin_financier.length()==0){
			admin_financier.setText("non renseigné");
		}else{
			admin_financier.setText(ClientDetails.cdd.admin_financier);
		}		
		TextView tele_admin_financier=(TextView)view.findViewById(R.id.tv_tele_admin_financier);
		if(ClientDetails.cdd.tele_admin_financier.length()==0){
			tele_admin_financier.setText("non renseigné");
		}else{
			tele_admin_financier.setText(ClientDetails.cdd.tele_admin_financier);
		}	
		TextView equipe_tech=(TextView)view.findViewById(R.id.tv_equipe_tech);
		if(ClientDetails.cdd.equipe_tech.length()==0){
			equipe_tech.setText("non renseigné");
		}else{
			equipe_tech.setText(ClientDetails.cdd.equipe_tech);
		}		
		TextView tele_equipe_tech=(TextView)view.findViewById(R.id.tv_tele_equipe_tech);
		if(ClientDetails.cdd.tele_equipe_tech.length()==0){
			tele_equipe_tech.setText("non renseigné");
		}else{
			tele_equipe_tech.setText(ClientDetails.cdd.tele_equipe_tech);
		}		
		return view;
	}
	
	@Override
	public void onClick(View v) {
		int id = v.getId();
		if (id == R.id.tv_back) {
			getActivity().getSupportFragmentManager().beginTransaction().remove(this).commit();
		}
	}
}
