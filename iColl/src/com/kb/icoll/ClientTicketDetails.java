//  iCollMobile
//
//  Created by Khaled Noui
//  Copyright (c) 2014 digiwabi. All rights reserved.
//

package com.kb.icoll;

import java.io.IOException;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.kb.icoll.utils.RESTful;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

public class ClientTicketDetails extends Fragment implements OnClickListener{
	ListView ticket_details;
	SharedPreferences my_pref;
	boolean b;
	TextView plus;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.client_ticket_details, container,false);
		
		ticket_details=(ListView)view.findViewById(R.id.lv_ticket_list);
		//status=(TextView)view.findViewById(R.id.tv_status);
		TextView header=(TextView)view.findViewById(R.id.tv_header);
		header.setText(ClientDetails.cdd.name+" "+ClientDetails.cdd.first_name);
		
		LinearLayout parent=(LinearLayout)view.findViewById(R.id.ll_parent);
		parent.setOnClickListener(this); 
		
		plus=(TextView)view.findViewById(R.id.tv_plus);
		plus.setOnClickListener(this); 
		plus.setVisibility(View.GONE); 
		
		my_pref=getActivity().getSharedPreferences(getResources().getString(R.string.APP_DATA), Activity.MODE_PRIVATE); 
		if(getArguments().getString("last_from").equals(my_pref.getString("user_id", ""))){
			plus.setVisibility(View.GONE); 
		}
		
		TextView back=(TextView)view.findViewById(R.id.tv_back);
		back.setOnClickListener(this); 
		new GetTicketDetails().execute("");
		return view;			
	}
	
	public void refreshData(){
		b=true;
		new GetTicketDetails().execute("");
	};
	
	private class GetTicketDetails extends AsyncTask<String,Void,Void> {
		ProgressDialog pd;
		int status_flag;
		ArrayList<MyTicketDetailsData> al;

		@Override
		public void onPreExecute(){
			pd=new ProgressDialog(getActivity());
			pd.setMessage(getResources().getString(R.string.PLEASE_WAIT));
			pd.setCanceledOnTouchOutside(false);
			pd.show();
		}

		@Override
		protected Void doInBackground(String... params1) {								
			try {
				String url = getResources().getString(R.string.api_end_point)+"ticket?projet_id="+getArguments().getString("projet_id")
						+"&ticket_id="+getArguments().getString("ticket_id"); 
					
				String reply=new RESTful(getActivity()).queryServer(url, true);
		        System.out.println("json_cnst-1: " + reply);

				reply=reply.replace(getResources().getString(R.string.json_cnst), "");
		        System.out.println("json_cnst-2: " + reply);

				JSONArray data = new JSONArray(reply);
				al=new ArrayList<MyTicketDetailsData>();
				
				for (int i=0;i<data.length();i++){
					JSONObject jo=data.getJSONObject(i);					
					MyTicketDetailsData pd=new MyTicketDetailsData();
					pd.from="De "+jo.getString("from");
					pd.date=jo.getString("date");
					pd.comment=jo.getString("comment");	
					pd.from_id=jo.getString("id_from");
					pd.sujet=jo.getString("sujet");
					al.add(pd);
				}				
				status_flag=2;
			} catch (IOException e) {
				if(e.getMessage().equals("403")){
					status_flag=5;
				}else if(e.getMessage().equals("204")){
					status_flag=7;
				}
				else{
					status_flag=3;
				}
				e.printStackTrace();
			} catch (JSONException e) {
				status_flag=8;
			}catch (NullPointerException e) {
				status_flag=6;
			}
					 				 	 			 	
		     return null;
		}

		@Override
		public void onPostExecute(Void m_adapter){               	
			pd.dismiss();
			if(status_flag==2){
				if(al.size()>0){
					if(!al.get(0).from_id.equalsIgnoreCase(my_pref.getString("user_id", ""))){
						plus.setVisibility(View.VISIBLE);
					}
				}else{
					plus.setVisibility(View.VISIBLE);
				}
				implementListView(al);
		 	}else if(status_flag==3){
		 		new AlertDialog.Builder(getActivity())
				.setMessage("Erreur de connexion. Veuillez  réessayer à nouveau")
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int id) {							
						
					}
				}).setIcon(android.R.drawable.stat_notify_error).show();
		 	}else if(status_flag==4){
		 		new AlertDialog.Builder(getActivity())
				.setMessage("Erreur du serveur. Veuillez réessayer à nouveau")
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int id) {							
						//getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, new Clients(), "clients").commit();		
					}
				}).setIcon(android.R.drawable.stat_notify_error).show();
		 	}else if(status_flag==5){
		 		new AlertDialog.Builder(getActivity())
				.setMessage("Paramètres de connexion incorrects")
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int id) {							
						//getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, new Clients(), "clients").commit();
					}
				}).setIcon(android.R.drawable.stat_notify_error).show();
		 	}else if(status_flag==6){		 		
		 		new AlertDialog.Builder(getActivity())
				.setMessage("Aucune donnée reçue . S'il vous plait réessayer plus tard.")
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int id) {							
						//getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, new Clients(), "clients").commit();
					}
				}).setIcon(android.R.drawable.stat_notify_error).show();
		 	}else if(status_flag==7){
		 		
		 	}else if(status_flag==8){
		 		plus.setVisibility(View.VISIBLE);
		 		al=new ArrayList<MyTicketDetailsData>();
		 		implementListView(al);
		 		new AlertDialog.Builder(getActivity())
		 		.setMessage("Aucune réponse à ce ticket")
		 		.setPositiveButton("OK", new DialogInterface.OnClickListener() {

		 		@Override
		 		public void onClick(DialogInterface dialog, int id) {

		 		}
		 		}).setIcon(android.R.drawable.stat_notify_error).show();
		 	}

		}
	}
	
	private void implementListView(ArrayList<MyTicketDetailsData> al_documents_non_rescue) {
		ArrayAdapter<MyTicketDetailsData> ad = new MyAdapter(getActivity(),
				R.layout.ticket_details_cell, al_documents_non_rescue);	
		View headerView = ((LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.ticket_details_header_cell, null, false);
		TextView msg=(TextView)headerView.findViewById(R.id.tv_message);
		msg.setText(getArguments().getString("message"));
		
		TextView su=(TextView)headerView.findViewById(R.id.tv_sujet);
		su.setText(getArguments().getString("sujet"));
		if(!b){
			ticket_details.addHeaderView(headerView);
		}		
		ticket_details.setAdapter(ad);
		ad.notifyDataSetChanged();
		ticket_details.setTextFilterEnabled(true);
	}
	
	private class MyAdapter extends ArrayAdapter<MyTicketDetailsData>{
		ArrayList<MyTicketDetailsData> my_al;
		
		public MyAdapter(Context context, int textViewResourceId, ArrayList<MyTicketDetailsData> al) {
			super(context, textViewResourceId, al);	
			my_al=al;
		}
				
		@Override
		public View getView(final int position, View convertView, ViewGroup parent) {
			View v = convertView;
			
			if (v == null) {				
				LayoutInflater vi = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);				
		        v = vi.inflate(R.layout.ticket_details_cell, null);		        			        				
			}
			
			TextView name=(TextView)v.findViewById(R.id.tv_name);
			name.setText(my_al.get(position).from); 
			
			TextView address=(TextView)v.findViewById(R.id.tv_date);
			address.setText(my_al.get(position).date);
			
			TextView comment=(TextView)v.findViewById(R.id.tv_comment);
			comment.setText(my_al.get(position).comment);
			
			return v;
		}
	}
	
	private class MyTicketDetailsData{
		String from,date,comment,from_id,sujet;
	}

	@Override
	public void onClick(View v) {
		int id = v.getId();
		if (id == R.id.tv_back) {
			getActivity().getSupportFragmentManager().beginTransaction().remove(this).commit();
		} else if (id == R.id.tv_plus) {
			SendMessage ctd=new SendMessage();
			Bundle b=new Bundle();
			b.putString("projet_id", getArguments().getString("projet_id"));
			b.putString("ticket_id", getArguments().getString("ticket_id"));
			b.putString("reponse_id", getArguments().getString("reponse_id"));
			ctd.setArguments(b);
			getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.content_frame2, ctd, "send_message").commit();
		}
	}
}
