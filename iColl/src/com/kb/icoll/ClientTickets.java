//  iCollMobile
//
//  Created by Khaled Noui
//  Copyright (c) 2014 digiwabi. All rights reserved.
//

package com.kb.icoll;

import java.io.IOException;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.kb.icoll.utils.RESTful;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class ClientTickets extends Fragment{
	ListView ticket_list;
	TextView status;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.client_tickets, container,false);
		
		ticket_list=(ListView)view.findViewById(R.id.lv_ticket_list);
		status=(TextView)view.findViewById(R.id.tv_status);
		new GetTickets().execute("");
		return view;			
	}
	
	private class GetTickets extends AsyncTask<String,Void,Void> {
		ProgressDialog pd;
		int status_flag;
		ArrayList<MyTicketData> al;

		@Override
		public void onPreExecute(){
			pd=new ProgressDialog(getActivity());
			pd.setMessage(getResources().getString(R.string.PLEASE_WAIT));
			pd.setCanceledOnTouchOutside(false);
			pd.show();
		}

		@Override
		protected Void doInBackground(String... params1) {								
			try {
				String url = getResources().getString(R.string.api_end_point)+"ticket?projet_id="+getArguments().getString("projet_id"); 
					
				String reply=new RESTful(getActivity()).queryServer(url, true);
		        System.out.println("json_cnst-1: " + reply);

				reply=reply.replace(getResources().getString(R.string.json_cnst), "");
		        System.out.println("json_cnst-2: " + reply);

				JSONArray data = new JSONArray(reply);
				al=new ArrayList<MyTicketData>();
				
				for (int i=0;i<data.length();i++){
					JSONObject jo=data.getJSONObject(i);					
					MyTicketData pd=new MyTicketData();
					pd.date=jo.getString("date");
					pd.sujet=jo.getString("sujet");
					pd.message=jo.getString("message");	
					pd.projet_id=jo.getString("id_projet");
					pd.ticket_id=jo.getString("id");
					pd.last_from=jo.getString("last_from");
					pd.reponse_id=jo.getString("reponse_id");
					al.add(pd);
				}				
				status_flag=2;
			} catch (IOException e) {
				if(e.getMessage().equals("403")){
					status_flag=5;
				}else if(e.getMessage().equals("204")){
					status_flag=7;
				}
				else{
					status_flag=3;
				}
				e.printStackTrace();
			} catch (JSONException e) {
				status_flag=4;
			}catch (NullPointerException e) {
				status_flag=6;
			}
					 				 	 			 	
		     return null;
		}

		@Override
		public void onPostExecute(Void m_adapter){               	
			pd.dismiss();
			if(status_flag==2){
				if(al.size()>0){
					implementListView(al);
				}else{
					status.setVisibility(View.VISIBLE);
				}				
		 	}else if(status_flag==3){
		 		new AlertDialog.Builder(getActivity())
				.setMessage("Erreur de connexion. Veuillez  réessayer à nouveau")
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int id) {							
						
					}
				}).setIcon(android.R.drawable.stat_notify_error).show();
		 	}else if(status_flag==4){
		 		new AlertDialog.Builder(getActivity())
				.setMessage("Erreur du serveur. Veuillez réessayer à nouveau")
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int id) {							
						//getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, new Clients(), "clients").commit();		
					}
				}).setIcon(android.R.drawable.stat_notify_error).show();
		 	}else if(status_flag==5){
		 		new AlertDialog.Builder(getActivity())
				.setMessage("Paramètres de connexion incorrects")
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int id) {							
						//getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, new Clients(), "clients").commit();
					}
				}).setIcon(android.R.drawable.stat_notify_error).show();
		 	}else if(status_flag==6){
		 		new AlertDialog.Builder(getActivity())
				.setMessage("Aucune donnée reçue . S'il vous plait réessayer plus tard.")
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int id) {							
						//getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, new Clients(), "clients").commit();
					}
				}).setIcon(android.R.drawable.stat_notify_error).show();
		 	}else if(status_flag==7){
		 		status.setVisibility(View.VISIBLE);
		 	}
		}
	}
	
	private void implementListView(ArrayList<MyTicketData> al_documents_non_rescue) {
		ArrayAdapter<MyTicketData> ad = new MyAdapter(getActivity(),
				R.layout.ticket_cell, al_documents_non_rescue);	
		ticket_list.setAdapter(ad);
		ad.notifyDataSetChanged();
		ticket_list.setTextFilterEnabled(true);
	}
	
	private class MyAdapter extends ArrayAdapter<MyTicketData>{
		ArrayList<MyTicketData> my_al;
		
		public MyAdapter(Context context, int textViewResourceId, ArrayList<MyTicketData> al) {
			super(context, textViewResourceId, al);	
			my_al=al;
		}
				
		@Override
		public View getView(final int position, View convertView, ViewGroup parent) {
			View v = convertView;
			
			if (v == null) {				
				LayoutInflater vi = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);				
		        v = vi.inflate(R.layout.ticket_cell, null);		        			        				
			}
			
			TextView name=(TextView)v.findViewById(R.id.tv_name);
			name.setText(my_al.get(position).sujet); 
			
			TextView address=(TextView)v.findViewById(R.id.tv_date);
			address.setText(my_al.get(position).date); 
			
			v.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					ClientTicketDetails ctd=new ClientTicketDetails();
					Bundle b=new Bundle();
					b.putString("projet_id", my_al.get(position).projet_id); 
					b.putString("ticket_id", my_al.get(position).ticket_id); 
					b.putString("sujet", my_al.get(position).sujet); 
					b.putString("message", my_al.get(position).message); 
					b.putString("last_from", my_al.get(position).last_from);
					b.putString("reponse_id", my_al.get(position).reponse_id);
					ctd.setArguments(b);
					getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.content_frame1, ctd, "client_ticket_details").commit();
				}
			});
			return v;
		}
	}
	
	private class MyTicketData{
		String date,sujet,message,ticket_id,projet_id,last_from,reponse_id;
	}
}
