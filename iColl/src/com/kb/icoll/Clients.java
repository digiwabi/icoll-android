//  iCollMobile
//
//  Created by Khaled Noui
//  Copyright (c) 2014 digiwabi. All rights reserved.
//

package com.kb.icoll;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.kb.icoll.utils.OnTabChangedListener;
import com.kb.icoll.utils.RESTful;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class Clients extends Fragment implements OnClickListener{
	OnTabChangedListener mListener;	
	ListView clients;
	private ValueFilter valueFilter;
	EditText search;
	ArrayAdapter<ClientData> ad;
	public static ArrayList<ClientData> al=new ArrayList<ClientData>();
	public static ArrayList<ClientData> al_map_lo=new ArrayList<ClientData>();
	TextView status;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.clients, container,false);
		mListener.onTabChanged(3);
		
		status=(TextView)view.findViewById(R.id.tv_status);
		clients=(ListView)view.findViewById(R.id.lv_clients);
		
		ImageView about_me=(ImageView)view.findViewById(R.id.iv_about_me);
		about_me.setOnClickListener(this);
		
		if(new RESTful(getActivity()).checkNetworkConnection()){
			new GetClientListing().execute("");
		}
		
		search=(EditText)view.findViewById(R.id.ed_search);
		search.addTextChangedListener(new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				System.out.println(s.toString());
				if(ad!=null){
					ad.getFilter().filter(s.toString());
					ad.notifyDataSetChanged();	
				}
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				
			}
			
			@Override
			public void afterTextChanged(Editable s) {
				
			}
		});
		
		return view;
	}
	
	private class GetClientListing extends AsyncTask<String,Void,Void> {
		ProgressDialog pd;
		@SuppressWarnings("unused")
		int status_flag,total_alert,new_alert,non_statues,no_projets_concernes,
			rendez_confirms,no_photos,no_ticket,no_changement,no_sav_du_jour;
		
		@Override
		public void onPreExecute(){
			pd=new ProgressDialog(getActivity());
			pd.setMessage(getResources().getString(R.string.PLEASE_WAIT));
			pd.setCanceledOnTouchOutside(false);
			pd.show();
		}

		@Override
		protected Void doInBackground(String... params1) {								
			try {
				String reply=new RESTful(getActivity()).queryServer(getResources().getString(R.string.api_end_point)+"client", true);
		        System.out.println("json_cnst-1: " + reply);

		        reply=reply.replace(getResources().getString(R.string.json_cnst), "");
		        System.out.println("json_cnst-2: " + reply);

				JSONArray data = new JSONArray(reply);
				al=new ArrayList<Clients.ClientData>();
				//al_map_loc=new ArrayList<Clients.ClientData>();
				for(int i=0;i<data.length();i++){
					JSONObject jo=data.getJSONObject(i);
					ClientData cd=new ClientData();
					//ClientData cdm=new ClientData();
					cd.first_name=jo.getString("prenom");
					cd.name=jo.getString("nom");
					cd.postal_code=jo.getString("code_postal");
					cd.ville=jo.getString("ville");
					cd.etape_projet=jo.getString("etape_projet");
					cd.etat_projet=jo.getString("etat_projet");
					cd.type_package=jo.getString("categorie_package_diminutif");
					cd.client_id=jo.getString("client_id");
					cd.projet_id=jo.getString("projet_id");
					cd.adresse=jo.getString("adresse");
					cd.tel_fixe=jo.getString("tel_fixe");
					cd.package_name=jo.getString("package_nom");
					cd.package_puissance=jo.getString("package_puissance");
					cd.projet_etat_installation=jo.getString("projet_etat_installation");
					cd.projet_date_installation=jo.getString("projet_date_installation");
					
					try{
						cd.client_latitude=jo.getDouble("client_latitude");
						cd.client_longitude=jo.getDouble("client_longitude");
					}catch(JSONException e){
						cd.client_latitude=0.0;
						cd.client_longitude=0.0;
					}
					al.add(cd);
					//al_map_loc.add(cdm);
				}
				status_flag=2;
			} catch (IOException e) {
				if(e.getMessage().equals("403")){
					status_flag=5;
				}else if(e.getMessage().equals("204")){
					status_flag=7;
				}
				else{
					status_flag=3;
				}
				e.printStackTrace();
			} catch (JSONException e) {
				e.printStackTrace();
				status_flag=4;
			}catch (NullPointerException e) {
				e.printStackTrace();
				status_flag=6;
			}					 				 	 			 	
			return null;
		}

		@Override
		public void onPostExecute(Void m_adapter){               	
			pd.dismiss();
			if(status_flag==2){
		 		Collections.sort(al, new CustomComparator());
	 			implementListView(al);
		 	}else if(status_flag==3){
		 		new AlertDialog.Builder(getActivity())
				.setMessage("Erreur de connexion. Veuillez  réessayer à nouveau")
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					
					public void onClick(DialogInterface dialog, int id) {							
									
					}
				}).setIcon(android.R.drawable.stat_notify_error).show();
		 	}else if(status_flag==4){
		 		new AlertDialog.Builder(getActivity())
				.setMessage("Erreur du serveur. Veuillez réessayer à nouveau")
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					
					public void onClick(DialogInterface dialog, int id) {							
									
					}
				}).setIcon(android.R.drawable.stat_notify_error).show();
		 	}else if(status_flag==5){
		 		new AlertDialog.Builder(getActivity())
				.setMessage("Paramètres de connexion incorrects")
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					
					public void onClick(DialogInterface dialog, int id) {							
									
					}
				}).setIcon(android.R.drawable.stat_notify_error).show();
		 	}else if(status_flag==6){
		 		new AlertDialog.Builder(getActivity())
				.setMessage("Aucune donnée reçue . S'il vous plait réessayer plus tard.")
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					
					public void onClick(DialogInterface dialog, int id) {							
									
					}
				}).setIcon(android.R.drawable.stat_notify_error).show();
		 	}else if(status_flag==7){
		 		status.setVisibility(View.VISIBLE);
		 	}
		}
	}
	
	private class CustomComparator implements Comparator<ClientData> { 

		@Override
		public int compare(ClientData lhs, ClientData rhs) {
			return (lhs.name+" "+lhs.first_name).compareTo(rhs.name+" "+rhs.first_name);
		}
	}
	
	private void implementListView(ArrayList<ClientData> al_ed) {
		ad = new MyAdapter(getActivity(),
				R.layout.client_cell, al_ed);	
		clients.setAdapter(ad);
		ad.notifyDataSetChanged();
		clients.setTextFilterEnabled(true);
	}
	
	private class MyAdapter extends ArrayAdapter<ClientData> implements Filterable{
		ArrayList<ClientData> my_al;
		
		public MyAdapter(Context context, int textViewResourceId, ArrayList<ClientData> al) {
			super(context, textViewResourceId, al);	
			my_al=al;
		}
				
		@Override
		public View getView(final int position, View convertView, ViewGroup parent) {
			View v = convertView;
			
			if (v == null) {				
				LayoutInflater vi = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);				
		        v = vi.inflate(R.layout.client_cell, null);		        			        				
			}
			
			TextView name=(TextView)v.findViewById(R.id.tv_name);
			name.setText(my_al.get(position).name+" "+my_al.get(position).first_name); 
			
			TextView address=(TextView)v.findViewById(R.id.tv_address);
			address.setText(my_al.get(position).postal_code+" "+my_al.get(position).ville); 
			
			TextView projet=(TextView)v.findViewById(R.id.tv_projet);
			projet.setText("Dernier état modifié : "+my_al.get(position).etape_projet+" "+my_al.get(position).etat_projet);
			
			TextView package_type=(TextView)v.findViewById(R.id.tv_type_of_package);
			package_type.setText("Type de package : "+my_al.get(position).type_package);
			
			v.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					ClientDetails cd=new ClientDetails();
					Bundle b=new Bundle();
					b.putString("client_id", my_al.get(position).client_id);
					b.putString("projet_id", my_al.get(position).projet_id);
					b.putString("is_from_alert", "no");
					cd.setArguments(b);
					getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, cd, "client_details").commit();
				}
			});
			return v;
		}
		
		@Override
		public Filter getFilter() {
			if(valueFilter==null){
				valueFilter=new ValueFilter();
			}
			return valueFilter;
		}
	}
	
	public class ClientData{
		String name,first_name,postal_code,ville,etape_projet,etat_projet,type_package,client_id,projet_id,adresse,tel_fixe,package_name,package_puissance,projet_etat_installation,projet_date_installation;
		double client_latitude,client_longitude;
	}
	
	 @Override
	 public void onAttach(Activity activity) {
		 super.onAttach(activity);
	     try {	       
	    	 mListener= (OnTabChangedListener) activity;
	     } catch (ClassCastException e) {
	    	 throw new ClassCastException(activity.toString()+ " must implement OnTabChangedListener");
	     }
	 }
	 
	 private class ValueFilter extends Filter {
		    
		    @Override
		    protected FilterResults performFiltering(CharSequence constraint) {

		        FilterResults results=new FilterResults();
		        
		        if(constraint!=null && constraint.length()>0){
		            ArrayList<ClientData> filterList=new ArrayList<ClientData>();
		            String sc=constraint.toString().toLowerCase();
		           	            	
		            	for(int i=0;i<al.size();i++){	            		
			                if(al.get(i).name.toLowerCase().contains(sc)) {		                	
			                    filterList.add(al.get(i));
			                }
			            }

		            results.count=filterList.size();
		            results.values=filterList;

		        }else{
		        	results.count=al.size();
			        results.values=al;
		        }
		        return results;
		    }

			@SuppressWarnings("unchecked")
			@Override
			protected void publishResults(CharSequence constraint,
					FilterResults results) {	
				ArrayList<ClientData> al=(ArrayList<ClientData>) results.values;
				implementListView(al);	
			}
		}

	@Override
	public void onClick(View v) {
		int id = v.getId();
		if (id == R.id.iv_about_me) {
			AboutMe am=new AboutMe();
			Bundle b=new Bundle();
			b.putString("about_me_call", "clients");
			am.setArguments(b);
			getActivity().getSupportFragmentManager()
			.beginTransaction().replace(R.id.content_frame1, am, "about_me").commit();
		}
	}
}
