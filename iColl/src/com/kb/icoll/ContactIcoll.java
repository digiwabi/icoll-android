//  iCollMobile
//
//  Created by Khaled Noui
//  Copyright (c) 2014 digiwabi. All rights reserved.
//

package com.kb.icoll;

import com.kb.icoll.utils.OnTabChangedListener;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract.CommonDataKinds.Email;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;

public class ContactIcoll extends Fragment implements OnClickListener {
	OnTabChangedListener mListener;
	TextView email,telephone;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.simulation_temp, container,false);
		mListener.onTabChanged(5);
		
		telephone=(TextView)view.findViewById(R.id.tv_telephone);
		telephone.setOnClickListener(this);
		
		email=(TextView)view.findViewById(R.id.tv_email);
		email.setOnClickListener(this);
		
		
		return view;
		
	}
	
	@Override
	 public void onAttach(Activity activity) {
		 super.onAttach(activity);
	     try {	       
	    	 mListener= (OnTabChangedListener) activity;
	     } catch (ClassCastException e) {
	    	 throw new ClassCastException(activity.toString()+ " must implement OnTabChangedListener");
	     }
	 }

	public void doBack() {
		getActivity().getSupportFragmentManager().beginTransaction().remove(this).commit();
	}

	@Override
	public void onClick(View v) {
		int id = v.getId();
		if (id == R.id.tv_telephone) {
			String uri = "tel:" + telephone.getText().toString();
			Intent intent = new Intent(Intent.ACTION_DIAL);
			intent.setData(Uri.parse(uri));
			startActivity(intent);
		} else if (id == R.id.tv_email) {
			Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
		            "mailto",email.getText().toString(), null));
			emailIntent.putExtra(Intent.EXTRA_SUBJECT, "iColl");
			startActivity(Intent.createChooser(emailIntent, "Send email..."));
		}
		
	}
	
}
