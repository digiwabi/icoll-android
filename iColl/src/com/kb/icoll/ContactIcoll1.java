//  iCollMobile
//
//  Created by Khaled Noui
//  Copyright (c) 2014 digiwabi. All rights reserved.
//

package com.kb.icoll;

import com.kb.icoll.utils.OnTabChangedListener;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;

public class ContactIcoll1 extends Fragment implements OnClickListener {
	OnTabChangedListener mListener;
	TextView email,telephone;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.contact_icoll, container,false);
		
		TextView back=(TextView)view.findViewById(R.id.tv_back);
		back.setOnClickListener(this);
		
		telephone=(TextView)view.findViewById(R.id.tv_telephone);
		telephone.setOnClickListener(this);
		
		email=(TextView)view.findViewById(R.id.tv_email);
		email.setOnClickListener(this);

		return view;
		
	}

	public void doBack() {
		getActivity().getSupportFragmentManager().beginTransaction().remove(this).commit();
	}

	@Override
	public void onClick(View v) {
		int id = v.getId();
		if (id == R.id.tv_back) {
			getActivity().getSupportFragmentManager().beginTransaction().remove(ContactIcoll1.this).commit();
		} else if (id == R.id.tv_telephone) {
			String uri = "tel:" + telephone.getText().toString();
			Intent intent = new Intent(Intent.ACTION_DIAL);
			intent.setData(Uri.parse(uri));
			startActivity(intent);
		} else if (id == R.id.tv_email) {
			Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
		            "mailto",email.getText().toString(), null));
			emailIntent.putExtra(Intent.EXTRA_SUBJECT, "iColl");
			startActivity(Intent.createChooser(emailIntent, "Send email..."));
		}
		
	}
	
}
