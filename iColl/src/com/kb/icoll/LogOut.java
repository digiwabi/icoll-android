//  iCollMobile
//
//  Created by Khaled Noui
//  Copyright (c) 2014 digiwabi. All rights reserved.
//

package com.kb.icoll;

import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;

public class LogOut extends DialogFragment implements OnClickListener {
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
		getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
		int width = getActivity().getResources().getDisplayMetrics().widthPixels;
		
		getDialog().getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
		Window window = getDialog().getWindow();
		WindowManager.LayoutParams wlp = window.getAttributes();

		wlp.gravity = Gravity.BOTTOM;
		//wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
		window.setAttributes(wlp);
		
		View view = inflater.inflate(R.layout.log_out, container);
		
		LinearLayout ll=(LinearLayout)view.findViewById(R.id.ll_parent);
		ll.setLayoutParams(new LinearLayout.LayoutParams(width, ViewGroup.LayoutParams.WRAP_CONTENT));	
		
		
		Button cancel=(Button)view.findViewById(R.id.bt_cancel);
		cancel.setOnClickListener(this);
		
		Button log_out=(Button)view.findViewById(R.id.bt_log_out);
		log_out.setOnClickListener(this);
		
		return view;
	}

	@Override
	public void onClick(View v) {
		int id = v.getId();
		if (id == R.id.bt_log_out) {
			Intent i=new Intent(getActivity(),SignIn.class);
			startActivity(i);
			getActivity().finish();
		} else if (id == R.id.bt_cancel) {
			dismiss();
		}
	}

}
