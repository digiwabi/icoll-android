//  iCollMobile
//
//  Created by Khaled Noui
//  Copyright (c) 2014 digiwabi. All rights reserved.
//

package com.kb.icoll;

import java.io.IOException;

import org.json.JSONException;
import org.json.JSONObject;

import com.kb.icoll.Alerts.OnNewAlertListener;
import com.kb.icoll.utils.LocationUpdateService;
import com.kb.icoll.utils.RESTful;
import com.kb.icoll.utils.LocationUpdateService.LocationBinder;
import com.kb.icoll.utils.OnTabChangedListener;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends FragmentActivity implements OnClickListener, OnTabChangedListener, OnNewAlertListener{
	TextView alerts,agenda,clients,prospects,simulation,alert_counter;
	boolean doubleBackToExitPressedOnce,mBound;
	LocationUpdateService mLoc;
	int total_alert,new_alert;
	Handler h;
	Runnable r;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		
		alerts=(TextView)findViewById(R.id.tv_alerts);
		alerts.setOnClickListener(this);
		agenda=(TextView)findViewById(R.id.tv_agenda);
		agenda.setOnClickListener(this);
		clients=(TextView)findViewById(R.id.tv_clients);
		clients.setOnClickListener(this);
		prospects=(TextView)findViewById(R.id.tv_prospects);
		prospects.setOnClickListener(this);
		simulation=(TextView)findViewById(R.id.tv_simulation);		
		simulation.setOnClickListener(this);
		alert_counter=(TextView)findViewById(R.id.tv_alert_counter);
		
		getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, new Alerts(), "alerts").commit();
		
		ImageView cross=(ImageView)findViewById(R.id.iv_cross);
		cross.setOnClickListener(this);
		
		Intent intent = new Intent(this, LocationUpdateService.class);
		bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
		
		
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		h=new Handler();
		r=new Runnable() {
			
			@Override
			public void run() {
				new GetNoOfAlerts().execute("");							
			}
		};
		h.post(r);
	}
	
	
	private ServiceConnection mConnection = new ServiceConnection() {

		@Override
		public void onServiceConnected(ComponentName name, IBinder service) {
			mBound=true;	
				LocationBinder binder = (LocationBinder) service;
				mLoc = binder.getService();
				mLoc.updateLocation();
			}
	
			@Override
			public void onServiceDisconnected(ComponentName name) {
				mBound=false;	
			}
	};


	@Override
	public void onClick(View v) {
		int id = v.getId();
		if (id == R.id.tv_alerts) {
			cleanContainer1();
			getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, new Alerts(), "alerts").commit();
		} else if (id == R.id.tv_agenda) {
			cleanContainer1();
			getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, new Agenda(), "agenda").commit();
		} else if (id == R.id.tv_clients) {
			cleanContainer1();
			getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, new Clients(), "clients").commit();
		} else if (id == R.id.tv_prospects) {
			cleanContainer1();
			getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, new Prospects(), "prospects").commit();
		} else if (id == R.id.tv_simulation) {
			cleanContainer1();
			//getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, new Simulation(), "simulation").commit();
			getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, new ContactIcoll(), "contact_icoll").commit();
		} else if (id == R.id.iv_cross) {
			LogOut lg=new LogOut();
			lg.show(MainActivity.this.getSupportFragmentManager(), "log_out");
		}
	}
	
	private void cleanContainer1(){
		Plan cp= (Plan)getSupportFragmentManager().findFragmentByTag("plan");
		if(cp!=null){
			getSupportFragmentManager().beginTransaction().remove(cp).commit();
		}
		ClientResumeInformationClient cp1= (ClientResumeInformationClient)getSupportFragmentManager().findFragmentByTag("client_resume_info_client");
		if(cp1!=null){
			getSupportFragmentManager().beginTransaction().remove(cp1).commit();
		}
		ClientResumeInfoDeCommande cp2= (ClientResumeInfoDeCommande)getSupportFragmentManager().findFragmentByTag("client_resume_info_de_commande");
		if(cp2!=null){
			getSupportFragmentManager().beginTransaction().remove(cp2).commit();
		}
		ClientResumeEtatsDuProjet cp3= (ClientResumeEtatsDuProjet)getSupportFragmentManager().findFragmentByTag("client_resume_info_du_projet");
		if(cp3!=null){
			getSupportFragmentManager().beginTransaction().remove(cp3).commit();
		}
		ClientResumeListeDeIntervenants cp4= (ClientResumeListeDeIntervenants)getSupportFragmentManager().findFragmentByTag("client_resume_liste_des_intervenants");
		if(cp4!=null){
			getSupportFragmentManager().beginTransaction().remove(cp4).commit();
		}
		AboutMe cp5= (AboutMe)getSupportFragmentManager().findFragmentByTag("about_me");
		if(cp5!=null){
			getSupportFragmentManager().beginTransaction().remove(cp5).commit();
		}
		ClientProjetInfoDuProjet cp6= (ClientProjetInfoDuProjet)getSupportFragmentManager().findFragmentByTag("client_projet_info_du_projet");
		if(cp6!=null){
			getSupportFragmentManager().beginTransaction().remove(cp6).commit();
		}
		ClientProjetInfoTechDuProjet cp7= (ClientProjetInfoTechDuProjet)getSupportFragmentManager().findFragmentByTag("client_projet_info_tech_du_projet");
		if(cp7!=null){
			getSupportFragmentManager().beginTransaction().remove(cp7).commit();
		}
		ClientProjetSavReclamation cp8= (ClientProjetSavReclamation)getSupportFragmentManager().findFragmentByTag("client_projet_sav_reclamation");
		if(cp8!=null){
			getSupportFragmentManager().beginTransaction().remove(cp8).commit();
		}
		AlertDetailsRDV cp9= (AlertDetailsRDV)getSupportFragmentManager().findFragmentByTag("alert_details_rdv");
		if(cp9!=null){
			getSupportFragmentManager().beginTransaction().remove(cp9).commit();
		}
		PhotoFullScreen cp10= (PhotoFullScreen)getSupportFragmentManager().findFragmentByTag("photo_full_screen");
		if(cp10!=null){
			getSupportFragmentManager().beginTransaction().remove(cp10).commit();
		}
		ClientTicketDetails cp11= (ClientTicketDetails)getSupportFragmentManager().findFragmentByTag("client_ticket_details");
		if(cp11!=null){
			getSupportFragmentManager().beginTransaction().remove(cp11).commit();
		}
		AlertListing cp12= (AlertListing)getSupportFragmentManager().findFragmentByTag("alert_listing");
		if(cp12!=null){
			getSupportFragmentManager().beginTransaction().remove(cp12).commit();
		}
		Agenda cp13= (Agenda)getSupportFragmentManager().findFragmentByTag("agenda");
		if(cp13!=null){
			getSupportFragmentManager().beginTransaction().remove(cp13).commit();
		}
		ContactIcoll cp14= (ContactIcoll)getSupportFragmentManager().findFragmentByTag("contact_icoll");
		if(cp14!=null){
			getSupportFragmentManager().beginTransaction().remove(cp14).commit();
		}
		AlertsProspectsAround ap= (AlertsProspectsAround)getSupportFragmentManager().findFragmentByTag("alert_prospects_around");
		if(ap!=null){
			getSupportFragmentManager().beginTransaction().remove(ap).commit();
		}
		CapturedPhoto cpp= (CapturedPhoto)getSupportFragmentManager().findFragmentByTag("captured_photo");
		if(cpp!=null){
			getSupportFragmentManager().beginTransaction().remove(cpp).commit();
		}
		InfoDetails id= (InfoDetails)getSupportFragmentManager().findFragmentByTag("info_details");
		if(id!=null){
			getSupportFragmentManager().beginTransaction().remove(id).commit();
		}
	}
	
//	@Override
//	public void onNewAlert(int new_alert) {
//		alert_counter.setText(""+new_alert);
//		System.out.println("No. of new alerts: "+new_alert);
//		if(new_alert==0){
//			alert_counter.setVisibility(View.GONE);
//		}else{
//			alert_counter.setVisibility(View.VISIBLE);
//		}
//	}

	
	private void setSelected(int i){
		alerts.setTextColor(getResources().getColor(R.color.grey));
		alerts.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.alert, 0, 0);
		agenda.setTextColor(getResources().getColor(R.color.grey));
		agenda.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.agenda, 0, 0);
		clients.setTextColor(getResources().getColor(R.color.grey));
		clients.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.clients, 0, 0);
		prospects.setTextColor(getResources().getColor(R.color.grey));
		prospects.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.prospects, 0, 0);
		simulation.setTextColor(getResources().getColor(R.color.grey));
		simulation.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.simulation, 0, 0);
		if(i==1){
			alerts.setTextColor(getResources().getColor(R.color.theme_color));
			alerts.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.alert_s, 0, 0);		
		}else if(i==2){
			agenda.setTextColor(getResources().getColor(R.color.theme_color));
			agenda.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.agenda_s, 0, 0);				
		}else if(i==3){
			clients.setTextColor(getResources().getColor(R.color.theme_color));
			clients.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.clients_s, 0, 0);				
		}else if(i==4){
			prospects.setTextColor(getResources().getColor(R.color.theme_color));
			prospects.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.prospects_s, 0, 0);				
		}else if(i==5){
			simulation.setTextColor(getResources().getColor(R.color.theme_color));
			simulation.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.simulation_s, 0, 0);				
		}
	}

	@Override
	public void onTabChanged(int i) {
		setSelected(i); 
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
		if(mBound){
			try{
				unbindService(mConnection);
			}catch(IllegalArgumentException e){
	
			}
		}
	}
	
	@Override
	protected void onStop() {
		super.onStop();
		h.removeCallbacks(r);
	}
	
	@Override
	public void onBackPressed() {
	    if (doubleBackToExitPressedOnce) {
	        super.onBackPressed();
	        return;
	    }

	    this.doubleBackToExitPressedOnce = true;
	    Toast.makeText(this, "Appuyez à nouveau pour quitter", Toast.LENGTH_SHORT).show();

	    new Handler().postDelayed(new Runnable() {

	        @Override
	        public void run() {
	            doubleBackToExitPressedOnce=false;                       
	        }
	    }, 2000);
	}
	
	private class GetNoOfAlerts extends AsyncTask<String,Void,Void> {
		
		int status_flag;

		@Override
		protected Void doInBackground(String... params1) {								
			try {
				String reply=new RESTful(MainActivity.this).queryServer(getResources().getString(R.string.api_end_point)+"user", true);
		        System.out.println("json_cnst-1: " + reply);

		        reply=reply.replace(getResources().getString(R.string.json_cnst), "");
		        System.out.println("json_cnst-2: " + reply);

				JSONObject data = new JSONObject(reply);				
				total_alert=data.getInt("alertes_total");
				new_alert=data.getInt("alertes_new");
			
				status_flag=2;
			} catch (IOException e) {
				e.printStackTrace();
			} catch (JSONException e) {
				e.printStackTrace();
			}catch (NullPointerException e) {
				e.printStackTrace();
			}
					 				 	 			 	
			return null;
		}

		@Override
		public void onPostExecute(Void m_adapter){
			h.postDelayed(r, 300000);
			if(status_flag==2){		 		
				System.out.println("New alert: "+new_alert);
				System.out.println("Total alert: "+total_alert);
				
				alert_counter.setText(""+new_alert);
				if(new_alert==0){
					alert_counter.setVisibility(View.GONE);
				}else{
					alert_counter.setVisibility(View.VISIBLE);
				}
		 	
		 	}
		}
	}

	@Override
	public void onNewAlert(int new_alert) {
		// TODO Auto-generated method stub
		
	}

}
