//  iCollMobile
//
//  Created by Khaled Noui
//  Copyright (c) 2014 digiwabi. All rights reserved.
//

package com.kb.icoll;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.Locale;

import com.kb.icoll.utils.CalendarAdapter;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

public class MonthView extends Fragment implements OnClickListener{
	GridView gridview;
	public static GregorianCalendar month;
	private CalendarAdapter adapter;
	public static String selectedGridDate; 
	TextView title;
	ListView event_list;
	ArrayAdapter<AgendaData> ad;
	ArrayList<AgendaData> my_al_agenda_events;
	ArrayList<AgendaData> al;
	ArrayList<String> al_date;
	String selected,today;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.month_view, container,false);
		gridview = (GridView) view.findViewById(R.id.gridview);
		
		event_list=(ListView)view.findViewById(R.id.lv_event_list);
		
		month = (GregorianCalendar) GregorianCalendar.getInstance();
		adapter = new CalendarAdapter(getActivity(), month);
		gridview.setAdapter(adapter);	
		
		title = (TextView) view.findViewById(R.id.title);
		SimpleDateFormat sdf=new SimpleDateFormat("MMMM yyyy", Locale.FRENCH);
		title.setText(sdf.format(month.getTime())); 
		
		ImageView previous = (ImageView) view.findViewById(R.id.iv_prev);
		previous.setOnClickListener(this);

		ImageView next = (ImageView) view.findViewById(R.id.iv_next);
		next.setOnClickListener(this);
		
		/////////////////////
		
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
		GregorianCalendar my_month = (GregorianCalendar) GregorianCalendar.getInstance();
		GregorianCalendar today_gc= (GregorianCalendar) my_month.clone();
		today=df.format(today_gc.getTime());
		
		System.out.println("now: "+today);
		
		if(selectedGridDate!=null){
			today=selectedGridDate;
		}
		
		selected=today;
		
		gridview.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> parent, View v,
					int position, long id) {
							
				((CalendarAdapter) parent.getAdapter()).setSelected(v);
				selectedGridDate = CalendarAdapter.dayString
						.get(position);
				selected=selectedGridDate;
				System.out.println(selectedGridDate);
				String[] separatedTime = selectedGridDate.split("-");
				String gridvalueString = separatedTime[2].replaceFirst("^0*","");
				int gridvalue = Integer.parseInt(gridvalueString);
												
				// navigate to next or previous month on clicking offdays.
				if ((gridvalue > 10) && (position < 8)) {
					setPreviousMonth();
					refreshCalendar();					
				} else if ((gridvalue < 7) && (position > 28)) {
					setNextMonth();
					refreshCalendar();
				}else{
					selectedGridDate=null;
				}
				refreshEventList();
			}		
		});
		return view;
	}
	
	private void refreshEventList() {
		//al=Agenda.al_agenda_list;
		al=new ArrayList<AgendaData>();
		for(int i=0;i<Agenda.al_agenda_list.size();i++){
			if(Agenda.al_agenda_list.get(i).statut_rdv_lb!=null){
				al.add(Agenda.al_agenda_list.get(i));
				//System.out.println("*** " +al.get(i).statut_rdv_lb);
			}
			
		}
		al_date=Agenda.al_new_ag_list;
		my_al_agenda_events=new ArrayList<AgendaData>();
		System.out.println("Selected: "+selected);
		
		for(int i=0;i<al_date.size();i++){
			//System.out.println("Selected value:  "+al_date.get(i).selected);
			if(al_date.get(i).equals(selected)){				
				my_al_agenda_events.add(al.get(i));
			}			
		}
		implementListView(my_al_agenda_events);
	}

	protected void setNextMonth() {
		if (month.get(GregorianCalendar.MONTH) == month
				.getActualMaximum(GregorianCalendar.MONTH)) {
			month.set((month.get(GregorianCalendar.YEAR) + 1),
					month.getActualMinimum(GregorianCalendar.MONTH), 1);
		} else {
			month.set(GregorianCalendar.MONTH,
					month.get(GregorianCalendar.MONTH) + 1);
		}

	}

	protected void setPreviousMonth() {
		if (month.get(GregorianCalendar.MONTH) == month
				.getActualMinimum(GregorianCalendar.MONTH)) {
			month.set((month.get(GregorianCalendar.YEAR) - 1),
					month.getActualMaximum(GregorianCalendar.MONTH), 1);
		} else {
			month.set(GregorianCalendar.MONTH,
					month.get(GregorianCalendar.MONTH) - 1);
		}

	}
	
	public void refreshCalendar() {
		adapter.refreshDays();
		adapter.notifyDataSetChanged();
		SimpleDateFormat sdf=new SimpleDateFormat("MMMM yyyy", Locale.FRENCH);
		title.setText(sdf.format(month.getTime())); 
	}

	@Override
	public void onClick(View v) {
		int id = v.getId();
		if (id == R.id.iv_prev) {
			selectedGridDate=null;
			setPreviousMonth();
			refreshCalendar();
		} else if (id == R.id.iv_next) {
			selectedGridDate=null;
			setNextMonth();
			refreshCalendar();
		}
	}
	
	private void implementListView(ArrayList<AgendaData> al_cd) {
		ad = new MyAdapter(getActivity(),
				R.layout.agenda_liste_cell_date, al_cd);	
		event_list.setAdapter(ad);
		ad.notifyDataSetChanged();
		event_list.setTextFilterEnabled(true);
	}
	
	private class MyAdapter extends ArrayAdapter<AgendaData>{
		ArrayList<AgendaData> my_al;
		
		public MyAdapter(Context context, int textViewResourceId, ArrayList<AgendaData> al) {
			super(context, textViewResourceId, al);	
			my_al=al;
		}
		
		@Override
		public View getView(final int position, View convertView, ViewGroup parent) {
			View v = convertView;
			
			if (v == null) {
				LayoutInflater vi = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				v = vi.inflate(R.layout.agenda_liste_cell_record, null);		
			}
			
			TextView name=(TextView)v.findViewById(R.id.tv_name);
	        name.setText(my_al.get(position).nom+" "+my_al.get(position).prenom);
	        
	        TextView time=(TextView)v.findViewById(R.id.tv_time);
	        time.setText(my_al.get(position).start_time);
	        
	        ImageView dot=(ImageView)v.findViewById(R.id.iv_indicator);
	        TextView hyphen=(TextView)v.findViewById(R.id.tv_hyphen);
	        
	        TextView cp=(TextView)v.findViewById(R.id.tv_cp);
	        cp.setText("("+my_al.get(position).cp+")");
	        
	        if(my_al.get(position).statut_rdv_lb.equalsIgnoreCase("Non statué")){
	        	name.setTextColor(Color.parseColor("#fc0301"));
	        	time.setTextColor(Color.parseColor("#fc0301"));
	        	cp.setTextColor(Color.parseColor("#fc0301"));
	        	hyphen.setTextColor(Color.parseColor("#fc0301"));
	        	dot.setVisibility(View.VISIBLE);
	        }else{
	        	name.setTextColor(Color.BLACK);
	        	time.setTextColor(Color.BLACK);
	        	cp.setTextColor(Color.BLACK);
	        	hyphen.setTextColor(Color.BLACK);
	        	dot.setVisibility(View.GONE);
	        }
			
			v.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					AgendaListe.ag_data=my_al.get(position);
					AlertDetailsRDV al=new AlertDetailsRDV();
					Bundle b=new Bundle();
					//b.putBoolean("is_alertdetails_via_agenda", true);
					b.putString("is_alertdetails_via_agenda","month_view");
					al.setArguments(b);
					getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.content_frame2, al, "alert_details_rdv").commit();
				}
			});
			return v;
		}
	}
	
	public void setCalendarEvents(){
		refreshEventList();
		adapter.notifyDataSetChanged();
	}
}
