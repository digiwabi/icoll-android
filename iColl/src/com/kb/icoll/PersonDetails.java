//  iCollMobile
//
//  Created by Khaled Noui
//  Copyright (c) 2014 digiwabi. All rights reserved.
//

package com.kb.icoll;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;

public class PersonDetails extends Fragment implements OnClickListener{

	TextView name,address,addresse,telephone,date_dernier,etat_dernier;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.clients, container,false);
		
		name=(TextView)view.findViewById(R.id.tv_name);
		address=(TextView)view.findViewById(R.id.tv_address);
		addresse=(TextView)view.findViewById(R.id.tv_addresse);
		telephone=(TextView)view.findViewById(R.id.tv_telephone);
		date_dernier=(TextView)view.findViewById(R.id.tv_date_dernier); //Date of last appointment
		etat_dernier=(TextView)view.findViewById(R.id.tv_etat_dernier); //Last state appointment 
		
		TextView itineraire_vers=(TextView)view.findViewById(R.id.tv_route_to_prospect); //Route to the prospect
		itineraire_vers.setOnClickListener(this);
		
		TextView voir_la_fiche=(TextView)view.findViewById(R.id.tv_more_about_this_prospect); //See more details about this prospect 
		voir_la_fiche.setOnClickListener(this);
		
		return view;
	}

	@Override
	public void onClick(View v) {
		int id = v.getId();
		if (id == R.id.tv_route_to_prospect) {
		} else if (id == R.id.tv_more_about_this_prospect) {
		}
	}
}
