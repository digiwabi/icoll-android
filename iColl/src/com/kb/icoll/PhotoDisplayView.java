//  iCollMobile
//
//  Created by Khaled Noui
//  Copyright (c) 2014 digiwabi. All rights reserved.
//

package com.kb.icoll;

import java.util.ArrayList;

import com.kb.icoll.utils.ExpandableHeightGridView;
import com.squareup.picasso.Picasso;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class PhotoDisplayView extends Fragment {
	
	ExpandableHeightGridView gv_photos;
	BaseAdapter b_adap;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.photo_display_view, container,false);
		
		gv_photos=(ExpandableHeightGridView)view.findViewById(R.id.gridView1);
		gv_photos.setExpanded(true);
		ArrayList<PhotoData> al=new ArrayList<PhotoData>();
		for(int i=0;i<Photos.al_photos.size();i++){
			if(Photos.al_photos.get(i).type_id==getArguments().getInt("photo_type_id")){
				al.add(Photos.al_photos.get(i));
			}
		}
		
		TextView no_photo=(TextView)view.findViewById(R.id.tv_no_photo_alert);
		if(al.size()==0){
			no_photo.setVisibility(View.VISIBLE);
		}else{
			no_photo.setVisibility(View.INVISIBLE);
			implementListView(al);
		}
				
		return view;
	}
	
	private class MyAdapter extends BaseAdapter{
		
		ArrayList<PhotoData> al;
		
		public MyAdapter(ArrayList<PhotoData> al2) {
			al=al2;
		}
		
		@Override
		public int getCount() {
			return al.size();
		}

		@Override
		public Object getItem(int position) {			
			return null;
		}

		@Override
		public long getItemId(int position) {			
			return 0;
		}

		@Override
		public View getView(final int position, View convertView, ViewGroup parent) {	
			View v = convertView;
			if (v == null) {
				LayoutInflater vi = (LayoutInflater)getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				v = vi.inflate(R.layout.photo_cell, null);
			}
			
			ImageView photo=(ImageView)v.findViewById(R.id.iv_photo);
			Picasso.with(getActivity()).load("http://"+al.get(position).path).resize(60,60).placeholder(R.drawable.loading).into(photo);
				
			
			v.setOnClickListener(new OnClickListener() {
	
				@Override
				public void onClick(View v) {
					PhotoFullScreen pf=new PhotoFullScreen();
					Bundle b=new Bundle();
					b.putString("url", "http://"+al.get(position).path);
					pf.setArguments(b);
					getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.content_frame1, pf, "photo_full_screen").commit();

				}					
			});				
			
			return v;
		}
	}
	
	private void implementListView(ArrayList<PhotoData> al2) {
		b_adap = new MyAdapter(al2);
		gv_photos.setAdapter(b_adap);
		b_adap.notifyDataSetChanged();
	}
	
}
