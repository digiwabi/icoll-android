//  iCollMobile
//
//  Created by Khaled Noui
//  Copyright (c) 2014 digiwabi. All rights reserved.
//

package com.kb.icoll;

import java.io.IOException;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.kb.icoll.utils.RESTful;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class Photos extends Fragment implements OnClickListener{
	
	TextView enviro_maison_counter,face_maison_counter,horizon_counter,photo_cablage_counter,photo_coffret_counter,photo_compteur_alignment_counter,photo_compteur_edf_counter,
	second_face_maison_counter,compositing_counter,autre_counter;
	ImageView iv_enviro_maison,iv_face_maison,iv_horizon,iv_photo_cablage,iv_photo_coffret,iv_photo_compteur_alignment,iv_photo_compteur_edf,iv_second_face_maison,iv_compositing,iv_autre;
	int env_flag=0,face_flag=0,horizon_flag=0,photo_cabblage_flag=0,photo_coffret_flag=0,photo_compteur_alignment_flag=0,photo_compteur_edf_flag=0,second_face_flag=0,compositing_flag=0,autre_flag=0;
	LinearLayout ll_enviro_maison,ll_face_maison,ll_horizon,ll_photo_cablage,ll_photo_coffret,ll_photo_compteur_alignment,ll_photo_compteur_edf,ll_second_face_maison,ll_compositing,ll_autre;
	//TextView env_no_photo_alert,face_maison_no_photo_alert,horizon_no_photo_alert,photo_cablage_no_photo_alert,photo_coffret_no_photo_alert,photo_compteur_alignment_no_photo_alert,
	//photo_compteur_edf_no_photo_alert,second_face_no_photo_alert,compositing_no_photo_alert,autre_no_photo_alert;
	static String client_id,projet_id;
	static ArrayList<PhotoData> al_photos;
	int currently_selected=0;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.photos, container,false);
		
		
		if(getArguments().getString("photo_calling").equalsIgnoreCase("client")){
			client_id=getArguments().getString("client_id");
			projet_id=getArguments().getString("projet_id");
		}else if(getArguments().getString("photo_calling").equalsIgnoreCase("prospects")){
			client_id=getArguments().getString("id");
			projet_id="0";
			System.out.println("Entering here");
		}else{
			client_id=getArguments().getString("client_id");
			projet_id=getArguments().getString("projet_id");
		}
		
		ImageView take_photo=(ImageView)view.findViewById(R.id.iv_take_photo);
		take_photo.setOnClickListener(this);
		
		FrameLayout fl_enviro_maison=(FrameLayout)view.findViewById(R.id.fl_enviro_maison);
		fl_enviro_maison.setOnClickListener(this);
		
		FrameLayout fl_face_maison=(FrameLayout)view.findViewById(R.id.fl_face_maison);
		fl_face_maison.setOnClickListener(this);
		
		FrameLayout horizon=(FrameLayout)view.findViewById(R.id.fl_horizon);
		horizon.setOnClickListener(this);
		
		FrameLayout photo_cablage=(FrameLayout)view.findViewById(R.id.fl_photo_cablage);
		photo_cablage.setOnClickListener(this);
		
		FrameLayout photo_coffret=(FrameLayout)view.findViewById(R.id.fl_photo_coffret);
		photo_coffret.setOnClickListener(this);
		
		FrameLayout photo_compteur_alignment=(FrameLayout)view.findViewById(R.id.fl_photo_compteur_alignment);
		photo_compteur_alignment.setOnClickListener(this);
		
		FrameLayout photo_compteur_edf=(FrameLayout)view.findViewById(R.id.fl_photo_compteur_edf);
		photo_compteur_edf.setOnClickListener(this);
		
		FrameLayout second_face_maison=(FrameLayout)view.findViewById(R.id.fl_second_face_maison);
		second_face_maison.setOnClickListener(this);
		
		FrameLayout compositing=(FrameLayout)view.findViewById(R.id.fl_compositing);
		compositing.setOnClickListener(this);
		
		FrameLayout autre=(FrameLayout)view.findViewById(R.id.fl_autre);
		autre.setOnClickListener(this);
		
		enviro_maison_counter=(TextView)view.findViewById(R.id.tv_enviro_maison);
		face_maison_counter=(TextView)view.findViewById(R.id.tv_face_maison);
		horizon_counter=(TextView)view.findViewById(R.id.tv_horizon);
		photo_cablage_counter=(TextView)view.findViewById(R.id.tv_photo_cablage);
		photo_coffret_counter=(TextView)view.findViewById(R.id.tv_photo_coffret);
		photo_compteur_alignment_counter=(TextView)view.findViewById(R.id.tv_photo_compteur_alignment);
		photo_compteur_edf_counter=(TextView)view.findViewById(R.id.tv_photo_compteur_edf);
		second_face_maison_counter=(TextView)view.findViewById(R.id.tv_second_face_maison);
		compositing_counter=(TextView)view.findViewById(R.id.tv_compositing);
		autre_counter=(TextView)view.findViewById(R.id.tv_autre);
		
		iv_enviro_maison=(ImageView)view.findViewById(R.id.iv_enviro_maison);
		iv_face_maison=(ImageView)view.findViewById(R.id.iv_face_maison);
		iv_horizon=(ImageView)view.findViewById(R.id.iv_horizon);
		iv_photo_cablage=(ImageView)view.findViewById(R.id.iv_photo_cablage);
		iv_photo_coffret=(ImageView)view.findViewById(R.id.iv_photo_coffret);
		iv_photo_compteur_alignment=(ImageView)view.findViewById(R.id.iv_photo_compteur_alignment);
		iv_photo_compteur_edf=(ImageView)view.findViewById(R.id.iv_photo_compteur_edf);
		iv_second_face_maison=(ImageView)view.findViewById(R.id.iv_second_face_maison);
		iv_compositing=(ImageView)view.findViewById(R.id.iv_compositing);
		iv_autre=(ImageView)view.findViewById(R.id.iv_autre);
		
		ll_enviro_maison=(LinearLayout)view.findViewById(R.id.ll_enviro_maison);
		ll_face_maison=(LinearLayout)view.findViewById(R.id.ll_face_maison);
		ll_horizon=(LinearLayout)view.findViewById(R.id.ll_horizon);
		ll_photo_cablage=(LinearLayout)view.findViewById(R.id.ll_photo_cablage);
		ll_photo_coffret=(LinearLayout)view.findViewById(R.id.ll_photo_coffret);
		ll_photo_compteur_alignment=(LinearLayout)view.findViewById(R.id.ll_photo_compteur_alignment);
		ll_photo_compteur_edf=(LinearLayout)view.findViewById(R.id.ll_photo_compteur_edf);
		ll_second_face_maison=(LinearLayout)view.findViewById(R.id.ll_second_face_maison);
		ll_compositing=(LinearLayout)view.findViewById(R.id.ll_compositing);
		ll_autre=(LinearLayout)view.findViewById(R.id.ll_autre);
		
//		env_no_photo_alert=(TextView)view.findViewById(R.id.tv_env_maison_photo_alert);
//		face_maison_no_photo_alert=(TextView)view.findViewById(R.id.tv_face_maison_photo_alert);
//		horizon_no_photo_alert=(TextView)view.findViewById(R.id.tv_horizon_photo_alert);
//		photo_cablage_no_photo_alert=(TextView)view.findViewById(R.id.tv_photo_cablage_photo_alert);
//		photo_coffret_no_photo_alert=(TextView)view.findViewById(R.id.tv_photo_coffret_photo_alert);
//		photo_compteur_alignment_no_photo_alert=(TextView)view.findViewById(R.id.tv_photo_compteur_alignment_photo_alert);
//		photo_compteur_edf_no_photo_alert=(TextView)view.findViewById(R.id.tv_photo_compteur_edf_photo_alert);
//		second_face_no_photo_alert=(TextView)view.findViewById(R.id.tv_second_face_maison_photo_alert);
//		compositing_no_photo_alert=(TextView)view.findViewById(R.id.tv_compositing_photo_alert);
//		autre_no_photo_alert=(TextView)view.findViewById(R.id.tv_autre_photo_alert);
		
		new GetPhotos(0).execute("");
		
		return view;
	}

	@Override
	public void onClick(View v) {
		int id = v.getId();
		if (id == R.id.iv_take_photo) {
			PhotoDialog pd=new PhotoDialog();
			pd.show(getActivity().getSupportFragmentManager(), "take_photo");
		} else if (id == R.id.fl_enviro_maison) {
			if(env_flag==0){
				ll_enviro_maison.setVisibility(View.VISIBLE);
				iv_enviro_maison.setImageResource(R.drawable.arrow1_down);
				env_flag=1;
				PhotoDisplayView pdv=new PhotoDisplayView();
				Bundle b=new Bundle();
				b.putInt("photo_type_id", 1);
				pdv.setArguments(b);
				getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.ll_enviro_maison, pdv,"enviro_maison_photo").commit();				
			}else if(env_flag==1){
				ll_enviro_maison.setVisibility(View.GONE);
				iv_enviro_maison.setImageResource(R.drawable.arrow1);
				env_flag=0;
			}
		} else if (id == R.id.fl_face_maison) {
			if(face_flag==0){
				ll_face_maison.setVisibility(View.VISIBLE);
				iv_face_maison.setImageResource(R.drawable.arrow1_down);
				face_flag=1;
				PhotoDisplayView pdv=new PhotoDisplayView();
				Bundle b=new Bundle();
				b.putInt("photo_type_id", 2);
				pdv.setArguments(b);
				getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.ll_face_maison, pdv,"face_maison").commit();				
			}else if(face_flag==1){
				ll_face_maison.setVisibility(View.GONE);
				iv_face_maison.setImageResource(R.drawable.arrow1);
				face_flag=0;
			}
		} else if (id == R.id.fl_horizon) {
			if(horizon_flag==0){
				ll_horizon.setVisibility(View.VISIBLE);
				iv_horizon.setImageResource(R.drawable.arrow1_down);
				horizon_flag=1;
				PhotoDisplayView pdv=new PhotoDisplayView();
				Bundle b=new Bundle();
				b.putInt("photo_type_id", 3);
				pdv.setArguments(b);
				getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.ll_horizon, pdv,"horizon").commit();				
			}else if(horizon_flag==1){
				ll_horizon.setVisibility(View.GONE);
				iv_horizon.setImageResource(R.drawable.arrow1);
				horizon_flag=0;
			}
		} else if (id == R.id.fl_photo_cablage) {
			if(photo_cabblage_flag==0){
				ll_photo_cablage.setVisibility(View.VISIBLE);
				iv_photo_cablage.setImageResource(R.drawable.arrow1_down);
				photo_cabblage_flag=1;
				PhotoDisplayView pdv=new PhotoDisplayView();
				Bundle b=new Bundle();
				b.putInt("photo_type_id", 4);
				pdv.setArguments(b);
				getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.ll_photo_cablage, pdv,"photo_cablage").commit();				
			}else if(photo_cabblage_flag==1){
				ll_photo_cablage.setVisibility(View.GONE);
				iv_photo_cablage.setImageResource(R.drawable.arrow1);
				photo_cabblage_flag=0;
			}
		} else if (id == R.id.fl_photo_coffret) {
			if(photo_coffret_flag==0){
				ll_photo_coffret.setVisibility(View.VISIBLE);
				iv_photo_coffret.setImageResource(R.drawable.arrow1_down);
				photo_coffret_flag=1;
				PhotoDisplayView pdv=new PhotoDisplayView();
				Bundle b=new Bundle();
				b.putInt("photo_type_id", 5);
				pdv.setArguments(b);
				getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.ll_photo_coffret, pdv,"photo_coffret").commit();				
			}else if(photo_coffret_flag==1){
				ll_photo_coffret.setVisibility(View.GONE);
				iv_photo_coffret.setImageResource(R.drawable.arrow1);
				photo_coffret_flag=0;
			}
		} else if (id == R.id.fl_photo_compteur_alignment) {
			if(photo_compteur_alignment_flag==0){
				ll_photo_compteur_alignment.setVisibility(View.VISIBLE);
				iv_photo_compteur_alignment.setImageResource(R.drawable.arrow1_down);
				photo_compteur_alignment_flag=1;
				PhotoDisplayView pdv=new PhotoDisplayView();
				Bundle b=new Bundle();
				b.putInt("photo_type_id", 6);
				pdv.setArguments(b);
				getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.ll_photo_compteur_alignment, pdv,"photo_compteur_alignment").commit();				
			}else if(photo_compteur_alignment_flag==1){
				ll_photo_compteur_alignment.setVisibility(View.GONE);
				iv_photo_compteur_alignment.setImageResource(R.drawable.arrow1);
				photo_compteur_alignment_flag=0;
			}
		} else if (id == R.id.fl_photo_compteur_edf) {
			if(photo_compteur_edf_flag==0){
				ll_photo_compteur_edf.setVisibility(View.VISIBLE);
				iv_photo_compteur_edf.setImageResource(R.drawable.arrow1_down);
				photo_compteur_edf_flag=1;
				PhotoDisplayView pdv=new PhotoDisplayView();
				Bundle b=new Bundle();
				b.putInt("photo_type_id", 7);
				pdv.setArguments(b);
				getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.ll_photo_compteur_edf, pdv,"photo_compteur_edf").commit();				
			}else if(photo_compteur_edf_flag==1){
				ll_photo_compteur_edf.setVisibility(View.GONE);
				iv_photo_compteur_edf.setImageResource(R.drawable.arrow1);
				photo_compteur_edf_flag=0;
			}
		} else if (id == R.id.fl_second_face_maison) {
			if(second_face_flag==0){
				ll_second_face_maison.setVisibility(View.VISIBLE);
				iv_second_face_maison.setImageResource(R.drawable.arrow1_down);
				second_face_flag=1;
				PhotoDisplayView pdv=new PhotoDisplayView();
				Bundle b=new Bundle();
				b.putInt("photo_type_id", 8);
				pdv.setArguments(b);
				getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.ll_second_face_maison, pdv,"second_face_maison").commit();	
			}else if(second_face_flag==1){
				ll_second_face_maison.setVisibility(View.GONE);
				iv_second_face_maison.setImageResource(R.drawable.arrow1);
				second_face_flag=0;
			}
		} else if (id == R.id.fl_compositing) {
			if(compositing_flag==0){
				ll_compositing.setVisibility(View.VISIBLE);
				iv_compositing.setImageResource(R.drawable.arrow1_down);
				compositing_flag=1;
				PhotoDisplayView pdv=new PhotoDisplayView();
				Bundle b=new Bundle();
				b.putInt("photo_type_id", 9);
				pdv.setArguments(b);
				getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.ll_compositing, pdv,"compositing").commit();	
			}else if(compositing_flag==1){
				ll_compositing.setVisibility(View.GONE);
				iv_compositing.setImageResource(R.drawable.arrow1);
				compositing_flag=0;
			}
		} else if (id == R.id.fl_autre) {
			if(autre_flag==0){
				ll_autre.setVisibility(View.VISIBLE);
				iv_autre.setImageResource(R.drawable.arrow1_down);
				autre_flag=1;
				PhotoDisplayView pdv=new PhotoDisplayView();
				Bundle b=new Bundle();
				b.putInt("photo_type_id", 10);
				pdv.setArguments(b);
				getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.ll_autre, pdv,"autre").commit();	
			}else if(autre_flag==1){
				ll_autre.setVisibility(View.GONE);
				iv_autre.setImageResource(R.drawable.arrow1);
				autre_flag=0;
			}
		}
	}

	private class GetPhotos extends AsyncTask<String,Void,Void> {
		ProgressDialog pd;
		@SuppressWarnings("unused")
		int my_option;
		int status_flag,i1;

		public GetPhotos(int i) {
			System.out.println("_______"+i);
			this.i1=i;
		}

		@Override
		public void onPreExecute(){
			pd=new ProgressDialog(getActivity());
			pd.setMessage(getResources().getString(R.string.PLEASE_WAIT));
			pd.setCanceledOnTouchOutside(false);
			pd.show();
		}

		@Override
		protected Void doInBackground(String... params1) {								
			try {
				String url = null;
//				if(getArguments().getString("photo_calling").equalsIgnoreCase("client")){
//					url=getResources().getString(R.string.api_end_point)+"photo?client_id="+client_id+"&"+"projet_id="+projet_id+"&width=100";
//				}else if(getArguments().getString("photo_calling").equalsIgnoreCase("prospects")){
//					url=getResources().getString(R.string.api_end_point)+"photo?client_id="+id+"&"+"projet_id=0&width=100";
//				}else{
//					url=getResources().getString(R.string.api_end_point)+"photo?client_id="+client_id+"&"+"projet_id="+projet_id+"&width=100";
//				}
				
				DisplayMetrics metrics = new DisplayMetrics();
				getActivity().getWindowManager().getDefaultDisplay().getMetrics(metrics);
				int wi=metrics.widthPixels;//1000;
				if(getArguments().getString("photo_calling").equalsIgnoreCase("client")){
				url=getResources().getString(R.string.api_end_point)+"photo?client_id="+client_id+"&"+"projet_id="+projet_id+"&width="+wi;
				}else if(getArguments().getString("photo_calling").equalsIgnoreCase("prospects")){
				url=getResources().getString(R.string.api_end_point)+"photo?client_id="+client_id+"&"+"projet_id=0&width="+wi;
				}else{
				url=getResources().getString(R.string.api_end_point)+"photo?client_id="+client_id+"&"+"projet_id="+projet_id+"&width="+wi;
				}
	
				String reply=new RESTful(getActivity()).queryServer(url, true);
		        System.out.println("json_cnst-1: " + reply);

		        reply=reply.replace(getResources().getString(R.string.json_cnst), "");
		        System.out.println("json_cnst-2: " + reply);

				JSONArray data = new JSONArray(reply);
				al_photos=new ArrayList<PhotoData>();
				
				for (int i=0;i<data.length();i++){
					JSONObject jo=data.getJSONObject(i);					
					PhotoData pd=new PhotoData();
					pd.type_id=jo.getInt("type_id");
					pd.type=jo.getString("type");
					pd.path=jo.getString("path").replace("app.icoll.fr", "devel.app.logicoll.nbs-test.com");				
					al_photos.add(pd);
				}				
				status_flag=2;
			} catch (IOException e) {
				if(e.getMessage().equals("403")){
					status_flag=5;
				}else if(e.getMessage().equals("204")){
					status_flag=7;
				}
				else{
					status_flag=3;
				}
				e.printStackTrace();
			} catch (JSONException e) {
				status_flag=4;
			}catch (NullPointerException e) {
				status_flag=6;
			}
					 				 	 			 	
		     return null;
		}

		@Override
		public void onPostExecute(Void m_adapter){               	
			pd.dismiss();
			if(status_flag==2){
				int a = 0,b = 0,c = 0,d = 0,e = 0,f = 0,g = 0,h = 0,i = 0,j = 0;
				for(int x=0;x<al_photos.size();x++){
					if(al_photos.get(x).type_id==1){
						a++;
					}else if(al_photos.get(x).type_id==2){
						b++;
					}else if(al_photos.get(x).type_id==3){
						c++;
					}else if(al_photos.get(x).type_id==4){
						d++;
					}else if(al_photos.get(x).type_id==5){
						e++;
					}else if(al_photos.get(x).type_id==6){
						f++;
					}else if(al_photos.get(x).type_id==7){
						g++;
					}else if(al_photos.get(x).type_id==8){
						h++;
					}else if(al_photos.get(x).type_id==9){
						i++;
					}else if(al_photos.get(x).type_id==10){
						j++;
					}
				}
				enviro_maison_counter.setText(""+a);
				face_maison_counter.setText(""+b);
				horizon_counter.setText(""+c);
				photo_cablage_counter.setText(""+d);
				photo_coffret_counter.setText(""+e);
				photo_compteur_alignment_counter.setText(""+f);
				photo_compteur_edf_counter.setText(""+g);
				second_face_maison_counter.setText(""+h);
				compositing_counter.setText(""+i);
				autre_counter.setText(""+j);
				
				if(i1==1){
					PhotoDisplayView pdv=new PhotoDisplayView();
					Bundle b1=new Bundle();
					b1.putInt("photo_type_id", 1);
					pdv.setArguments(b1);
					getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.ll_enviro_maison, pdv,"enviro_maison_photo").commit();				
				}else if(i1==2){
					PhotoDisplayView pdv=new PhotoDisplayView();
					Bundle b1=new Bundle();
					b1.putInt("photo_type_id", 2);
					pdv.setArguments(b1);
					getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.ll_face_maison, pdv,"face_maison").commit();				
				}else if(i1==3){
					PhotoDisplayView pdv=new PhotoDisplayView();
					Bundle b1=new Bundle();
					b1.putInt("photo_type_id", 3);
					pdv.setArguments(b1);
					getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.ll_horizon, pdv,"horizon").commit();
				}else if(i1==4){
					PhotoDisplayView pdv=new PhotoDisplayView();
					Bundle b1=new Bundle();
					b1.putInt("photo_type_id", 4);
					pdv.setArguments(b1);
					getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.ll_photo_cablage, pdv,"photo_cablage").commit();				
				}else if(i1==5){
					PhotoDisplayView pdv=new PhotoDisplayView();
					Bundle b1=new Bundle();
					b1.putInt("photo_type_id", 5);
					pdv.setArguments(b1);
					getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.ll_photo_coffret, pdv,"photo_coffret").commit();
				}else if(i1==6){
					PhotoDisplayView pdv=new PhotoDisplayView();
					Bundle b1=new Bundle();
					b1.putInt("photo_type_id", 6);
					pdv.setArguments(b1);
					getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.ll_photo_compteur_alignment, pdv,"photo_compteur_alignment").commit();							
				}else if(i1==7){
					PhotoDisplayView pdv=new PhotoDisplayView();
					Bundle b1=new Bundle();
					b1.putInt("photo_type_id", 7);
					pdv.setArguments(b1);
					getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.ll_photo_compteur_edf, pdv,"photo_compteur_edf").commit();				
				}else if(i1==8){
					PhotoDisplayView pdv=new PhotoDisplayView();
					Bundle b1=new Bundle();
					b1.putInt("photo_type_id", 8);
					pdv.setArguments(b1);
					getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.ll_second_face_maison, pdv,"second_face_maison").commit();				
				}else if(i1==9){
					PhotoDisplayView pdv=new PhotoDisplayView();
					Bundle b1=new Bundle();
					b1.putInt("photo_type_id", 9);
					pdv.setArguments(b1);
					getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.ll_compositing, pdv,"compositing").commit();	
				}else if(i1==10){
					PhotoDisplayView pdv=new PhotoDisplayView();
					Bundle b1=new Bundle();
					b1.putInt("photo_type_id", 10);
					pdv.setArguments(b1);
					getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.ll_autre, pdv,"autre").commit();					
				}
		 	}else if(status_flag==3){
		 		new AlertDialog.Builder(getActivity())
				.setMessage("Erreur de connexion. Veuillez  réessayer à nouveau")
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int id) {							
						//getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, new Clients(), "clients").commit();
					}
				}).setIcon(android.R.drawable.stat_notify_error).show();
		 	}else if(status_flag==4){
		 		new AlertDialog.Builder(getActivity())
				.setMessage("Erreur du serveur. Veuillez réessayer à nouveau")
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int id) {							
						//getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, new Clients(), "clients").commit();		
					}
				}).setIcon(android.R.drawable.stat_notify_error).show();
		 	}else if(status_flag==5){
		 		new AlertDialog.Builder(getActivity())
				.setMessage("Paramètres de connexion incorrects")
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int id) {							
						//getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, new Clients(), "clients").commit();
					}
				}).setIcon(android.R.drawable.stat_notify_error).show();
		 	}else if(status_flag==6){
		 		new AlertDialog.Builder(getActivity())
				.setMessage("Aucune donnée reçue . S'il vous plait réessayer plus tard.")
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int id) {							
						//getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, new Clients(), "clients").commit();
					}
				}).setIcon(android.R.drawable.stat_notify_error).show();
		 	}else if(status_flag==7){
		 		new AlertDialog.Builder(getActivity())
				.setMessage("Aucune photo trouvée.")
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int id) {							
						
					}
				}).setIcon(android.R.drawable.stat_notify_error).show();
		 	}
		}
	}

	public void refreshPhotos(int i) {
		System.out.println("_______"+i);
		new GetPhotos(i).execute("");
	}
}
