//  iCollMobile
//
//  Created by Khaled Noui
//  Copyright (c) 2014 digiwabi. All rights reserved.
//

package com.kb.icoll;

import java.util.HashMap;
import java.util.Map;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.GoogleMap.InfoWindowAdapter;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class Plan extends Fragment implements OnClickListener, LocationListener{
	//private static View view;
	private GoogleMap mMap;
	double lat,lon;
	double current_lat,current_lon;
	String provider="network";
	Location location;
	Handler h;
	Runnable r;
	Map<Marker, InfoWindowContent> markersContent = new HashMap<Marker, InfoWindowContent>();
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
//		if (view != null) {
//			ViewGroup parent = (ViewGroup) view.getParent();
//		if (parent != null)
//			parent.removeView(view);
//		}
		View view = inflater.inflate(R.layout.plan, container, false);
//		try {
//			
//			
//			
//		} catch (InflateException e) {
//			e.printStackTrace();
//		}
		
		TextView back=(TextView)view.findViewById(R.id.tv_back);
		back.setOnClickListener(this);
		
		if(getArguments().getString("from").equals("client")){
			lat=ClientDetails.cdd.lat;
			lon=ClientDetails.cdd.lon;			
		}else if(getArguments().getString("from").equals("prospect")){
			lat=ProspectDetails.pdd.lat;
			lon=ProspectDetails.pdd.lon;
		}else if(getArguments().getString("from").equals("agenda")){
			lat=AgendaListe.ag_data.latitude;
			lon=AgendaListe.ag_data.longitude;
		}else{
			lat=AlertListing.rsd.latitude;
			lon=AlertListing.rsd.longitude;
		}
		
		if (mMap == null) {
			mMap = ((SupportMapFragment)getChildFragmentManager().findFragmentById(R.id.map1)).getMap();
			if (mMap != null) {
				mMap.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
				mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(
		                new LatLng(lat, lon), 13));
				//mMap.addMarker(new MarkerOptions().position(new LatLng(lat, lon)));
				
				if(getArguments().getString("from").equals("prospect")){
					Marker venue_loc = mMap.addMarker(new MarkerOptions().position(new LatLng(lat, lon)));							
					InfoWindowContent markerContent = new InfoWindowContent();
					markerContent.client_name=ProspectDetails.pdd.first_name+" "+ProspectDetails.pdd.name;
					markerContent.addresse=ProspectDetails.pdd.address;
					markerContent.address=ProspectDetails.pdd.zip+" "+ProspectDetails.pdd.ville;
					markerContent.date=ProspectDetails.pdd.date;
					markerContent.last_rdv_confirmed_statut=ProspectDetails.pdd.rdv_statut;
					markerContent.tel_fixe=ProspectDetails.pdd.telephone;
			        markersContent.put(venue_loc, markerContent);
				}else if(getArguments().getString("from").equals("client")){
					Marker venue_loc = mMap.addMarker(new MarkerOptions().position(new LatLng(lat, lon)));							
					InfoWindowContent markerContent = new InfoWindowContent();
					markerContent.client_name=ClientDetails.cdd.first_name+" "+ClientDetails.cdd.name;
					markerContent.addresse=ClientDetails.cdd.address;
					markerContent.address=ClientDetails.cdd.zip+" "+ClientDetails.cdd.ville;
					markerContent.type_de_package=ClientDetails.cdd.package_name;
					markerContent.puissance=ClientDetails.cdd.total_wc;
					markerContent.etat_de_install=ClientDetails.cdd.projet_etat_installation;
					if(ClientDetails.cdd.date_d_installation.length()==0){
						markerContent.date_de_install="non renseignée";
					}else{
						markerContent.date_de_install=ClientDetails.cdd.date_d_installation;
					}
					
			        markersContent.put(venue_loc, markerContent);
				}else if(getArguments().getString("from").equals("agenda")){
					Marker venue_loc = mMap.addMarker(new MarkerOptions().position(new LatLng(lat, lon)));							
					InfoWindowContent markerContent = new InfoWindowContent();
					markerContent.client_name=AgendaListe.ag_data.prenom+" "+AgendaListe.ag_data.nom;
					markerContent.addresse=AgendaListe.ag_data.addresse;
					markerContent.address=AgendaListe.ag_data.address;
					markerContent.date=AgendaListe.ag_data.start_date;
					markerContent.last_rdv_confirmed_statut=AgendaListe.ag_data.statut_rdv_lb;
					markerContent.tel_fixe=AgendaListe.ag_data.telephone;
					
			        markersContent.put(venue_loc, markerContent);
				}else{
					Marker venue_loc = mMap.addMarker(new MarkerOptions().position(new LatLng(lat, lon)));							
					InfoWindowContent markerContent = new InfoWindowContent();
					markerContent.client_name=AlertListing.rsd.first_name+" "+AlertListing.rsd.name;
					markerContent.addresse=AlertListing.rsd.adresse;
					markerContent.address=AlertListing.rsd.address;
					markerContent.date=AlertListing.rsd.start_date;
					markerContent.last_rdv_confirmed_statut=AlertListing.rsd.statut;
					markerContent.tel_fixe=AlertListing.rsd.telephone;
					
			        markersContent.put(venue_loc, markerContent);
				}
				
				// Getting the current location
				LocationManager lm = (LocationManager)getActivity().getSystemService(Context.LOCATION_SERVICE);
		 		lm.requestLocationUpdates(provider, 1000L, 1.0f, this);
		 		location = lm.getLastKnownLocation(provider);
		    	
		 		
		 		h=new Handler();
				r =new Runnable() {
						
					public void run() {
						if(location!=null){							
							current_lat=location.getLatitude();
							current_lon=location.getLongitude();
							System.out.println("Current lat is: "+current_lat+" Current lon is: "+current_lon);
						}else{
							h.postDelayed(r, 3000);
						}
					}
				};
				h.post(r);
			}
		}
		
		TextView itineraire_directions=(TextView)view.findViewById(R.id.tv_itineraire);
		itineraire_directions.setOnClickListener(this);
		
		mMap.setInfoWindowAdapter(new InfoWindowAdapter() {

	         @Override
	         public View getInfoWindow(Marker arg0) {
	             return null;
	         }

	         @Override
	         public View getInfoContents(Marker arg0) { 
	             View v = getActivity().getLayoutInflater().inflate(R.layout.map_details_cell_prospects, null);
	             InfoWindowContent markerContent = markersContent.get(arg0);
	             
	             ImageView info=(ImageView)v.findViewById(R.id.iv_info);
	             info.setVisibility(View.GONE); 

	             LinearLayout ll_package_type=(LinearLayout)v.findViewById(R.id.ll_package_type);
	             LinearLayout ll_puissance=(LinearLayout)v.findViewById(R.id.ll_puissance);
	             LinearLayout ll_etat_installation=(LinearLayout)v.findViewById(R.id.ll_etat_installation);
	             LinearLayout ll_date_installation=(LinearLayout)v.findViewById(R.id.ll_date_installation);
	             LinearLayout ll_date_dernier=(LinearLayout)v.findViewById(R.id.ll_date_dernier);
	             LinearLayout ll_statut_dernier=(LinearLayout)v.findViewById(R.id.ll_statut_dernier);
	             	            
	             TextView client_name = (TextView)v.findViewById(R.id.tv_client_name);
	             client_name.setText(markerContent.client_name);
	             
	             TextView add = (TextView)v.findViewById(R.id.tv_addresse);
	             add.setText(markerContent.addresse);
	             
	             TextView add1= (TextView)v.findViewById(R.id.tv_address);
	             add1.setText(markerContent.address);
	             
	             if(getArguments().getString("from").equals("prospect")){
	            	 ll_date_dernier.setVisibility(View.VISIBLE);
	            	 ll_statut_dernier.setVisibility(View.VISIBLE);
	            	 ll_package_type.setVisibility(View.GONE);
	            	 ll_puissance.setVisibility(View.GONE);
	            	 ll_etat_installation.setVisibility(View.GONE);
	            	 ll_date_installation.setVisibility(View.GONE);
	            	 
	            	 TextView date= (TextView)v.findViewById(R.id.tv_date_du_dernier_rdv);
		             date.setText(markerContent.date);
		             
		             TextView last_rdv_confirmed_statut= (TextView)v.findViewById(R.id.tv_statut_dernier_rdv);
		             last_rdv_confirmed_statut.setText(markerContent.last_rdv_confirmed_statut);		      	            	 
	            	 
	             }else if(getArguments().getString("from").equals("client")){
	            	 ll_date_dernier.setVisibility(View.GONE);
	            	 ll_statut_dernier.setVisibility(View.GONE);
	            	 ll_package_type.setVisibility(View.VISIBLE);
	            	 ll_puissance.setVisibility(View.VISIBLE);
	            	 ll_etat_installation.setVisibility(View.VISIBLE);
	            	 ll_date_installation.setVisibility(View.VISIBLE);
	            	 
	            	 TextView type= (TextView)v.findViewById(R.id.tv_package_type);
		             type.setText(markerContent.type_de_package);
		             
		             TextView pui= (TextView)v.findViewById(R.id.tv_puissance_installation);
		             pui.setText(markerContent.puissance);	
		             
		             TextView etat= (TextView)v.findViewById(R.id.tv_etat_installation);
		             etat.setText(markerContent.etat_de_install);
		             
		             TextView date= (TextView)v.findViewById(R.id.tv_date_installation);
		             date.setText(markerContent.date_de_install);
	            	 
	             }else if(getArguments().getString("from").equals("alert")){
	            	 ll_date_dernier.setVisibility(View.VISIBLE);
	            	 ll_statut_dernier.setVisibility(View.VISIBLE);
	            	 ll_package_type.setVisibility(View.GONE);
	            	 ll_puissance.setVisibility(View.GONE);
	            	 ll_etat_installation.setVisibility(View.GONE);
	            	 ll_date_installation.setVisibility(View.GONE);
	            	 
	            	 TextView date= (TextView)v.findViewById(R.id.tv_date_du_dernier_rdv);
		             date.setText(markerContent.date);
		             
		             TextView last_rdv_confirmed_statut= (TextView)v.findViewById(R.id.tv_statut_dernier_rdv);
		             last_rdv_confirmed_statut.setText(markerContent.last_rdv_confirmed_statut);	            	 
	             }else if(getArguments().getString("from").equals("agenda")){
	            	 ll_date_dernier.setVisibility(View.VISIBLE);
	            	 ll_statut_dernier.setVisibility(View.VISIBLE);
	            	 ll_package_type.setVisibility(View.GONE);
	            	 ll_puissance.setVisibility(View.GONE);
	            	 ll_etat_installation.setVisibility(View.GONE);
	            	 ll_date_installation.setVisibility(View.GONE);
	            	 
	            	 TextView date= (TextView)v.findViewById(R.id.tv_date_du_dernier_rdv);
		             date.setText(markerContent.date);
		             
		             TextView last_rdv_confirmed_statut= (TextView)v.findViewById(R.id.tv_statut_dernier_rdv);
		             last_rdv_confirmed_statut.setText(markerContent.last_rdv_confirmed_statut);	            	 
	             }
//	             else{
//	            	 ll_date_dernier.setVisibility(View.GONE);
//	            	 ll_statut_dernier.setVisibility(View.GONE);
//	            	 ll_package_type.setVisibility(View.VISIBLE);
//	            	 ll_puissance.setVisibility(View.VISIBLE);
//	            	 ll_etat_installation.setVisibility(View.VISIBLE);
//	            	 ll_date_installation.setVisibility(View.VISIBLE);
//	            	 
//	            	 TextView package_type= (TextView)v.findViewById(R.id.tv_package_type);
//	            	 package_type.setText(markerContent.package_name);
//		             
//		             TextView puissance_installation= (TextView)v.findViewById(R.id.tv_puissance_installation);
//		             puissance_installation.setText(markerContent.package_puissance);
//		             
//		             TextView etat_installation= (TextView)v.findViewById(R.id.tv_etat_installation);
//		             etat_installation.setText(markerContent.projet_etat_installation);
//		             
//		             TextView date_installation= (TextView)v.findViewById(R.id.tv_date_installation);
//		             if(markerContent.projet_date_installation.length()==0){
//		            	 date_installation.setText("non renseigné");
//		             }else{
//		            	 date_installation.setText(markerContent.projet_date_installation);
//		             }
//	             }

	             return v;

	         }
	     });
		
		return view;
	}
	
	
	
	private class InfoWindowContent{
		String client_name,addresse,address,date,last_rdv_confirmed_statut,tel_fixe,package_name,package_puissance,
		projet_etat_installation,projet_date_installation,type_de_package,puissance,etat_de_install,date_de_install;
	}

	@Override
	public void onClick(View v) {
		int id = v.getId();
		if (id == R.id.tv_back) {
			getActivity().getSupportFragmentManager().beginTransaction().remove(this).commit();
		} else if (id == R.id.tv_itineraire) {
			System.out.println("Destination Lat: "+lat);
			System.out.println("Destination Lon: "+lon);
			//double test_d_lat=23.323636,test_d_lon=88.323696;
			Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.google.com/maps?saddr="+current_lat+","+current_lon+"&daddr="+lat+","+lon));
			startActivity(browserIntent);
		}
	}

	@Override
	public void onLocationChanged(Location location) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onProviderEnabled(String provider) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onProviderDisabled(String provider) {
		// TODO Auto-generated method stub
		
	}
}
