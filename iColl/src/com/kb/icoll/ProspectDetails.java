//  iCollMobile
//
//  Created by Khaled Noui
//  Copyright (c) 2014 digiwabi. All rights reserved.
//

package com.kb.icoll;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.kb.icoll.utils.ProspectDetailsData;
import com.kb.icoll.utils.RESTful;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;

public class ProspectDetails extends Fragment implements OnClickListener{

	TextView header,information,photos,plan;
	static ProspectDetailsData pdd;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.prospect_details, container,false);
		
		header=(TextView)view.findViewById(R.id.tv_header);
		
		information=(TextView)view.findViewById(R.id.tv_informations);
		information.setOnClickListener(this);
		photos=(TextView)view.findViewById(R.id.tv_photos);
		photos.setOnClickListener(this);
		plan=(TextView)view.findViewById(R.id.tv_plan);
		plan.setOnClickListener(this); 
		
		TextView back=(TextView)view.findViewById(R.id.tv_back);
		back.setOnClickListener(this);
		
		if(new RESTful(getActivity()).checkNetworkConnection()){
			new GetProspectDetails().execute("");
		}
		return view;
	}
	
	private class GetProspectDetails extends AsyncTask<String,Void,Void> {
		ProgressDialog pd;
		@SuppressWarnings("unused")
		int status_flag,total_alert,new_alert,non_statues,no_projets_concernes,
			rendez_confirms,no_photos,no_ticket,no_changement,no_sav_du_jour;

		@Override
		public void onPreExecute(){
			pd=new ProgressDialog(getActivity());
			pd.setMessage(getResources().getString(R.string.PLEASE_WAIT));
			pd.setCanceledOnTouchOutside(false);
			pd.show();
		}

		@Override
		protected Void doInBackground(String... params1) {								
			try {
				Map<String, String> params = new HashMap<String, String>(); 
				params.put("type_alerte_id", "3");
				String reply=new RESTful(getActivity()).queryServer(getResources().getString(R.string.api_end_point)+"prospect?client_id="+getArguments().getString("id"), true);
		        System.out.println("json_cnst-1: " + reply);

		        reply=reply.replace(getResources().getString(R.string.json_cnst), "");
		        System.out.println("json_cnst-2: " + reply);

				JSONArray data = new JSONArray(reply);
				JSONObject jo=data.getJSONObject(0);
				pdd=new ProspectDetailsData();
				pdd.name=jo.getString("nom");
				pdd.first_name=jo.getString("prenom");
				pdd.address=jo.getString("adresse");
				pdd.zip=jo.getString("cp");
				pdd.ville=jo.getString("ville");
				pdd.statut=jo.getString("statut");
				pdd.rdv_statut=jo.getString("last_rdv_confirmed_statut");
				pdd.rdv_commentaire=jo.getString("last_rdv_confirmed_commentaire");
				pdd.rdv_start_date=jo.getString("last_rdv_confirmed_start_date");
				pdd.rdv_confirmed_by=jo.getString("last_rdv_confirmed_confirmed_by");
				pdd.date=jo.getString("date");
				pdd.lat=jo.getDouble("latitude");
				pdd.lon=jo.getDouble("longitude");
				if(jo.getString("email").length()>0){
					pdd.email=jo.getString("email");
				}else{
					pdd.email="non renseigné";
				}				
				pdd.telephone=jo.getString("tel_fixe");
			
				status_flag=2;
			} catch (IOException e) {
				if(e.getMessage().equals("403")){
					status_flag=5;
				}else{
					status_flag=3;
				}
				e.printStackTrace();
			} catch (JSONException e) {
				status_flag=4;
			}catch (NullPointerException e) {
				status_flag=6;
			}
					 				 	 			 	
		     return null;
		}

		@Override
		public void onPostExecute(Void m_adapter){               	
			pd.dismiss();
			if(status_flag==2){
				header.setText(pdd.first_name+" "+pdd.name);
				getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.ll_prospect_sub_container, new ProspectInformations(), "prospect_informations").commit();
		 	}else if(status_flag==3){
		 		new AlertDialog.Builder(getActivity())
				.setMessage("Erreur de connexion . S'il vous plait réessayer plus tard.")
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int id) {							
						getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, new Prospects(), "prospects").commit();
					}
				}).setIcon(android.R.drawable.stat_notify_error).show();
		 	}else if(status_flag==4){
		 		new AlertDialog.Builder(getActivity())
				.setMessage("Erreur du serveur. Veuillez réessayer à nouveau")
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int id) {							
						getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, new Prospects(), "prospects").commit();
					}
				}).setIcon(android.R.drawable.stat_notify_error).show();
		 	}else if(status_flag==5){
		 		new AlertDialog.Builder(getActivity())
				.setMessage("Paramètres de connexion incorrects")
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int id) {							
						getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, new Prospects(), "prospects").commit();
					}
				}).setIcon(android.R.drawable.stat_notify_error).show();
		 	}else if(status_flag==6){
		 		new AlertDialog.Builder(getActivity())
				.setMessage("Aucune donnée reçue . S'il vous plait réessayer plus tard.")
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int id) {							
						getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, new Prospects(), "prospects").commit();
					}
				}).setIcon(android.R.drawable.stat_notify_error).show();
		 	}
		}
	}

	@Override
	public void onClick(View v) {
		int id = v.getId();
		if (id == R.id.tv_back) {
			if(getArguments().getString("prospect_details_via")==null){
				getActivity().getSupportFragmentManager().beginTransaction().remove(this).commit();
			}else
			if(getArguments().getString("prospect_details_via").equalsIgnoreCase("prospects"))
				getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, new Prospects(), "prospects").commit();
			else /*if(getArguments().getString("prospect_details_via").equalsIgnoreCase("alert"))*/{
				AlertDetailsRDV al=new AlertDetailsRDV();
				Bundle b=new Bundle();
				if(getArguments().getString("prospect_details_via").equalsIgnoreCase("agenda_liste")){
					b.putString("is_alertdetails_via_agenda","agenda_liste");
				}else if(getArguments().getString("prospect_details_via").equalsIgnoreCase("month_view")){
					b.putString("is_alertdetails_via_agenda","month_view");
				}else{
					b.putString("is_alertdetails_via_agenda","alert_listing");
				}
				al.setArguments(b);
				getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.content_frame1, al, "alert_details_rdv").commit();
				//getActivity().getSupportFragmentManager().beginTransaction().remove(this).commit();
			}
		} else if (id == R.id.tv_informations) {
			setSelected(0);
		} else if (id == R.id.tv_photos) {
			setSelected(1);
		} else if (id == R.id.tv_plan) {
			//setSelected(2);
			Plan cp= (Plan)getActivity().getSupportFragmentManager().findFragmentByTag("plan");
			if(cp==null){
				Plan p=new Plan();
				Bundle b=new Bundle();
				b.putString("from", "prospect");
				p.setArguments(b);
				getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.content_frame1, p, "plan").commit();
			}
		}
	}
	
	private void setSelected(int i){
		information.setBackgroundResource(0);
		information.setTextColor(getResources().getColor(R.color.theme_color));
		photos.setBackgroundResource(0);
		photos.setTextColor(getResources().getColor(R.color.theme_color));
		plan.setBackgroundResource(0);
		plan.setTextColor(getResources().getColor(R.color.theme_color));
		if(i==0){
			information.setBackgroundResource(R.drawable.orange_solid_left_rounded);
			information.setTextColor(getResources().getColor(R.color.white));
			ProspectInformations cr= (ProspectInformations)getActivity().getSupportFragmentManager().findFragmentByTag("prospect_informations");
			if(cr==null){
				getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.ll_prospect_sub_container, new ProspectInformations(), "prospect_informations").commit();
			}
		}else if(i==1){
			photos.setBackgroundColor(getResources().getColor(R.color.theme_color));
			photos.setTextColor(getResources().getColor(R.color.white));
			Photos cp= (Photos)getActivity().getSupportFragmentManager().findFragmentByTag("prospect_photos");
			if(cp==null){
				Photos p=new Photos();
				Bundle b=new Bundle();
				b.putString("id", getArguments().getString("id"));
				b.putString("photo_calling", "prospects");
				//b.putString("projet_id", getArguments().getString("projet_id"));
				p.setArguments(b);
				getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.ll_prospect_sub_container, p, "prospect_photos").commit();
			}
		}/*else if(i==2){
			plan.setBackgroundResource(R.drawable.orange_solid_right_rounded);
			plan.setTextColor(getResources().getColor(R.color.white));
			Plan cp= (Plan)getActivity().getSupportFragmentManager().findFragmentByTag("client_plan");
			if(cp==null){
				getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.ll_prospect_sub_container, new Plan(), "plan").commit();
			}
		}*/		
	}
}
