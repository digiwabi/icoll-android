//  iCollMobile
//
//  Created by Khaled Noui
//  Copyright (c) 2014 digiwabi. All rights reserved.
//

package com.kb.icoll;

import com.kb.icoll.utils.ProspectDetailsData;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;

public class ProspectInformations extends Fragment implements OnClickListener{
	TextView name,address,zip_ville,etape_de,rdv_confirmed_statut,rdv_commentaire,rdv_start_date,rdv_confirmed_by,date,email,telephone;
	ProspectDetailsData pdd;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.prospect_informations, container,false);
		
		name=(TextView)view.findViewById(R.id.tv_name);
		address=(TextView)view.findViewById(R.id.tv_address);
		zip_ville=(TextView)view.findViewById(R.id.tv_zip_ville);
		etape_de=(TextView)view.findViewById(R.id.tv_etape_de_saisie);
		rdv_confirmed_statut=(TextView)view.findViewById(R.id.tv_rdv_confirmed_statut);
		rdv_commentaire=(TextView)view.findViewById(R.id.tv_rdv_confirmed_commentaire);
		rdv_start_date=(TextView)view.findViewById(R.id.tv_rdv_start_date);
		rdv_confirmed_by=(TextView)view.findViewById(R.id.tv_rdv_confirmed_by);
		date=(TextView)view.findViewById(R.id.tv_date);
		
		email=(TextView)view.findViewById(R.id.tv_email);
		email.setOnClickListener(this);
		
		telephone=(TextView)view.findViewById(R.id.tv_telephone);
		telephone.setOnClickListener(this);
		
		if(ProspectDetails.pdd!=null){
			pdd=ProspectDetails.pdd;
			name.setText(pdd.first_name+" "+pdd.name); 
			address.setText(pdd.address);
			zip_ville.setText(pdd.zip+" "+pdd.ville);
			etape_de.setText("Etape de saisia : "+pdd.statut);
			rdv_confirmed_statut.setText("Statut dernier rdv : "+pdd.rdv_statut);
			rdv_commentaire.setText("Commentaire : "+pdd.rdv_commentaire);
			rdv_start_date.setText("Date dernier RDV : "+pdd.rdv_start_date);
			rdv_confirmed_by.setText("Confirmé par : "+pdd.rdv_confirmed_by);
			date.setText("Date de confirmation : "+pdd.date);
			email.setText("Email : "+pdd.email);
			telephone.setText("Téléphone fixe : "+pdd.telephone);
		}
		return view;
	}

	@Override
	public void onClick(View v) {
		int id = v.getId();
		if (id == R.id.tv_telephone) {
			String uri = "tel:" + pdd.telephone;
			Intent intent = new Intent(Intent.ACTION_DIAL);
			intent.setData(Uri.parse(uri));
			startActivity(intent);
		} else if (id == R.id.tv_email) {
			if(!pdd.email.equalsIgnoreCase("non renseigné")){
				Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
			            "mailto",pdd.email.trim(), null));
				emailIntent.putExtra(Intent.EXTRA_SUBJECT, "iColl");
				startActivity(Intent.createChooser(emailIntent, "Send email..."));
			}
		}
		
	}
}
