//  iCollMobile
//
//  Created by Khaled Noui
//  Copyright (c) 2014 digiwabi. All rights reserved.
//

package com.kb.icoll;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class ProspectPhotos extends Fragment implements OnClickListener{
	
	TextView enviro_maison_counter,face_maison_counter,horizon_counter,photo_cablage_counter,photo_coffret_counter,photo_compteur_alignment_counter,photo_compteur_edf_counter,
	second_face_maison_counter,compositing_counter,autre_counter;
	ImageView iv_enviro_maison,iv_face_maison,iv_horizon,iv_photo_cablage,iv_photo_coffret,iv_photo_compteur_alignment,iv_photo_compteur_edf,iv_second_face_maison,iv_compositing,iv_autre;
	int env_flag=0,face_flag=0,horizon_flag=0,photo_cabblage_flag=0,photo_coffret_flag=0,photo_compteur_alignment_flag=0,photo_compteur_edf_flag=0,second_face_flag=0,compositing_flag=0,autre_flag=0;
	LinearLayout ll_enviro_maison,ll_face_maison,ll_horizon,ll_photo_cablage,ll_photo_coffret,ll_photo_compteur_alignment,ll_photo_compteur_edf,ll_second_face_maison,ll_compositing,ll_autre;
	TextView env_no_photo_alert,face_maison_no_photo_alert,horizon_no_photo_alert,photo_cablage_no_photo_alert,photo_coffret_no_photo_alert,photo_compteur_alignment_no_photo_alert,
	photo_compteur_edf_no_photo_alert,second_face_no_photo_alert,compositing_no_photo_alert,autre_no_photo_alert;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.prospect_photos, container,false);
		
		ImageView take_photo=(ImageView)view.findViewById(R.id.iv_take_photo);
		take_photo.setOnClickListener(this);
		
		FrameLayout fl_enviro_maison=(FrameLayout)view.findViewById(R.id.fl_enviro_maison);
		fl_enviro_maison.setOnClickListener(this);
		
		FrameLayout fl_face_maison=(FrameLayout)view.findViewById(R.id.fl_face_maison);
		fl_face_maison.setOnClickListener(this);
		
		FrameLayout horizon=(FrameLayout)view.findViewById(R.id.fl_horizon);
		horizon.setOnClickListener(this);
		
		FrameLayout photo_cablage=(FrameLayout)view.findViewById(R.id.fl_photo_cablage);
		photo_cablage.setOnClickListener(this);
		
		FrameLayout photo_coffret=(FrameLayout)view.findViewById(R.id.fl_photo_coffret);
		photo_coffret.setOnClickListener(this);
		
		FrameLayout photo_compteur_alignment=(FrameLayout)view.findViewById(R.id.fl_photo_compteur_alignment);
		photo_compteur_alignment.setOnClickListener(this);
		
		FrameLayout photo_compteur_edf=(FrameLayout)view.findViewById(R.id.fl_photo_compteur_edf);
		photo_compteur_edf.setOnClickListener(this);
		
		FrameLayout second_face_maison=(FrameLayout)view.findViewById(R.id.fl_second_face_maison);
		second_face_maison.setOnClickListener(this);
		
		FrameLayout compositing=(FrameLayout)view.findViewById(R.id.fl_compositing);
		compositing.setOnClickListener(this);
		
		FrameLayout autre=(FrameLayout)view.findViewById(R.id.fl_autre);
		autre.setOnClickListener(this);
		
		enviro_maison_counter=(TextView)view.findViewById(R.id.tv_enviro_maison);
		face_maison_counter=(TextView)view.findViewById(R.id.tv_face_maison);
		horizon_counter=(TextView)view.findViewById(R.id.tv_horizon);
		photo_cablage_counter=(TextView)view.findViewById(R.id.tv_photo_cablage);
		photo_coffret_counter=(TextView)view.findViewById(R.id.tv_photo_coffret);
		photo_compteur_alignment_counter=(TextView)view.findViewById(R.id.tv_photo_compteur_alignment);
		photo_compteur_edf_counter=(TextView)view.findViewById(R.id.tv_photo_compteur_edf);
		second_face_maison_counter=(TextView)view.findViewById(R.id.tv_second_face_maison);
		compositing_counter=(TextView)view.findViewById(R.id.tv_compositing);
		autre_counter=(TextView)view.findViewById(R.id.tv_autre);
		
		iv_enviro_maison=(ImageView)view.findViewById(R.id.iv_enviro_maison);
		iv_face_maison=(ImageView)view.findViewById(R.id.iv_face_maison);
		iv_horizon=(ImageView)view.findViewById(R.id.iv_horizon);
		iv_photo_cablage=(ImageView)view.findViewById(R.id.iv_photo_cablage);
		iv_photo_coffret=(ImageView)view.findViewById(R.id.iv_photo_coffret);
		iv_photo_compteur_alignment=(ImageView)view.findViewById(R.id.iv_photo_compteur_alignment);
		iv_photo_compteur_edf=(ImageView)view.findViewById(R.id.iv_photo_compteur_edf);
		iv_second_face_maison=(ImageView)view.findViewById(R.id.iv_second_face_maison);
		iv_compositing=(ImageView)view.findViewById(R.id.iv_compositing);
		iv_autre=(ImageView)view.findViewById(R.id.iv_autre);
		
		ll_enviro_maison=(LinearLayout)view.findViewById(R.id.ll_enviro_maison);
		ll_face_maison=(LinearLayout)view.findViewById(R.id.ll_face_maison);
		ll_horizon=(LinearLayout)view.findViewById(R.id.ll_horizon);
		ll_photo_cablage=(LinearLayout)view.findViewById(R.id.ll_photo_cablage);
		ll_photo_coffret=(LinearLayout)view.findViewById(R.id.ll_photo_coffret);
		ll_photo_compteur_alignment=(LinearLayout)view.findViewById(R.id.ll_photo_compteur_alignment);
		ll_photo_compteur_edf=(LinearLayout)view.findViewById(R.id.ll_photo_compteur_edf);
		ll_second_face_maison=(LinearLayout)view.findViewById(R.id.ll_second_face_maison);
		ll_compositing=(LinearLayout)view.findViewById(R.id.ll_compositing);
		ll_autre=(LinearLayout)view.findViewById(R.id.ll_autre);
		
		env_no_photo_alert=(TextView)view.findViewById(R.id.tv_env_maison_photo_alert);
		face_maison_no_photo_alert=(TextView)view.findViewById(R.id.tv_face_maison_photo_alert);
		horizon_no_photo_alert=(TextView)view.findViewById(R.id.tv_horizon_photo_alert);
		photo_cablage_no_photo_alert=(TextView)view.findViewById(R.id.tv_photo_cablage_photo_alert);
		photo_coffret_no_photo_alert=(TextView)view.findViewById(R.id.tv_photo_coffret_photo_alert);
		photo_compteur_alignment_no_photo_alert=(TextView)view.findViewById(R.id.tv_photo_compteur_alignment_photo_alert);
		photo_compteur_edf_no_photo_alert=(TextView)view.findViewById(R.id.tv_photo_compteur_edf_photo_alert);
		second_face_no_photo_alert=(TextView)view.findViewById(R.id.tv_second_face_maison_photo_alert);
		compositing_no_photo_alert=(TextView)view.findViewById(R.id.tv_compositing_photo_alert);
		autre_no_photo_alert=(TextView)view.findViewById(R.id.tv_autre_photo_alert);
		
		return view;
	}

	@Override
	public void onClick(View v) {
		int id = v.getId();
		if (id == R.id.iv_take_photo) {
			PhotoDialog pd=new PhotoDialog();
			pd.show(getActivity().getSupportFragmentManager(), "take_photo");
		} else if (id == R.id.fl_enviro_maison) {
			if(env_flag==0){
				ll_enviro_maison.setVisibility(View.VISIBLE);
				iv_enviro_maison.setImageResource(R.drawable.arrow1_down);
				env_flag=1;
			}else if(env_flag==1){
				ll_enviro_maison.setVisibility(View.GONE);
				iv_enviro_maison.setImageResource(R.drawable.arrow1);
				env_flag=0;
			}
		} else if (id == R.id.fl_face_maison) {
			if(face_flag==0){
				ll_face_maison.setVisibility(View.VISIBLE);
				iv_face_maison.setImageResource(R.drawable.arrow1_down);
				face_flag=1;
			}else if(face_flag==1){
				ll_face_maison.setVisibility(View.GONE);
				iv_face_maison.setImageResource(R.drawable.arrow1);
				face_flag=0;
			}
		} else if (id == R.id.fl_horizon) {
			if(horizon_flag==0){
				ll_horizon.setVisibility(View.VISIBLE);
				iv_horizon.setImageResource(R.drawable.arrow1_down);
				horizon_flag=1;
			}else if(horizon_flag==1){
				ll_horizon.setVisibility(View.GONE);
				iv_horizon.setImageResource(R.drawable.arrow1);
				horizon_flag=0;
			}
		} else if (id == R.id.fl_photo_cablage) {
			if(photo_cabblage_flag==0){
				ll_photo_cablage.setVisibility(View.VISIBLE);
				iv_photo_cablage.setImageResource(R.drawable.arrow1_down);
				photo_cabblage_flag=1;
			}else if(photo_cabblage_flag==1){
				ll_photo_cablage.setVisibility(View.GONE);
				iv_photo_cablage.setImageResource(R.drawable.arrow1);
				photo_cabblage_flag=0;
			}
		} else if (id == R.id.fl_photo_coffret) {
			if(photo_coffret_flag==0){
				ll_photo_coffret.setVisibility(View.VISIBLE);
				iv_photo_coffret.setImageResource(R.drawable.arrow1_down);
				photo_coffret_flag=1;
			}else if(photo_coffret_flag==1){
				ll_photo_coffret.setVisibility(View.GONE);
				iv_photo_coffret.setImageResource(R.drawable.arrow1);
				photo_coffret_flag=0;
			}
		} else if (id == R.id.fl_photo_compteur_alignment) {
			if(photo_compteur_alignment_flag==0){
				ll_photo_compteur_alignment.setVisibility(View.VISIBLE);
				iv_photo_compteur_alignment.setImageResource(R.drawable.arrow1_down);
				photo_compteur_alignment_flag=1;
			}else if(photo_compteur_alignment_flag==1){
				ll_photo_compteur_alignment.setVisibility(View.GONE);
				iv_photo_compteur_alignment.setImageResource(R.drawable.arrow1);
				photo_compteur_alignment_flag=0;
			}
		} else if (id == R.id.fl_photo_compteur_edf) {
			if(photo_compteur_edf_flag==0){
				ll_photo_compteur_edf.setVisibility(View.VISIBLE);
				iv_photo_compteur_edf.setImageResource(R.drawable.arrow1_down);
				photo_compteur_edf_flag=1;
			}else if(photo_compteur_edf_flag==1){
				ll_photo_compteur_edf.setVisibility(View.GONE);
				iv_photo_compteur_edf.setImageResource(R.drawable.arrow1);
				photo_compteur_edf_flag=0;
			}
		} else if (id == R.id.fl_second_face_maison) {
			if(second_face_flag==0){
				ll_second_face_maison.setVisibility(View.VISIBLE);
				iv_second_face_maison.setImageResource(R.drawable.arrow1_down);
				second_face_flag=1;
			}else if(second_face_flag==1){
				ll_second_face_maison.setVisibility(View.GONE);
				iv_second_face_maison.setImageResource(R.drawable.arrow1);
				second_face_flag=0;
			}
		} else if (id == R.id.fl_compositing) {
			if(compositing_flag==0){
				ll_compositing.setVisibility(View.VISIBLE);
				iv_compositing.setImageResource(R.drawable.arrow1_down);
				compositing_flag=1;
			}else if(compositing_flag==1){
				ll_compositing.setVisibility(View.GONE);
				iv_compositing.setImageResource(R.drawable.arrow1);
				compositing_flag=0;
			}
		} else if (id == R.id.fl_autre) {
			if(autre_flag==0){
				ll_autre.setVisibility(View.VISIBLE);
				iv_autre.setImageResource(R.drawable.arrow1_down);
				autre_flag=1;
			}else if(autre_flag==1){
				ll_autre.setVisibility(View.GONE);
				iv_autre.setImageResource(R.drawable.arrow1);
				autre_flag=0;
			}
		}
	}
}
