//  iCollMobile
//
//  Created by Khaled Noui
//  Copyright (c) 2014 digiwabi. All rights reserved.
//

package com.kb.icoll;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.kb.icoll.utils.OnTabChangedListener;
import com.kb.icoll.utils.RESTful;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class Prospects extends Fragment implements OnClickListener{
	OnTabChangedListener mListener;
	ListView prospects;
	private ValueFilter valueFilter;
	EditText search;
	ArrayAdapter<ProspectData> ad;
	public static ArrayList<ProspectData> al=new ArrayList<ProspectData>();
	TextView status;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.prospects, container,false);
		mListener.onTabChanged(4);
		
		status=(TextView)view.findViewById(R.id.tv_status);
		prospects=(ListView)view.findViewById(R.id.lv_prospects);
		
		ImageView about_me=(ImageView)view.findViewById(R.id.iv_about_me);
		about_me.setOnClickListener(this);
		
		search=(EditText)view.findViewById(R.id.ed_search);
		search.addTextChangedListener(new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				System.out.println(s.toString());
				if(ad!=null){
					ad.getFilter().filter(s.toString());
					ad.notifyDataSetChanged();	
				}				
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				
			}
			
			@Override
			public void afterTextChanged(Editable s) {
				
			}
		});
		
		if(new RESTful(getActivity()).checkNetworkConnection()){
			new GetProspectListing().execute("");
		}
		return view;
	}
	
	private class GetProspectListing extends AsyncTask<String,Void,Void> {
		ProgressDialog pd;
		@SuppressWarnings("unused")
		int status_flag,total_alert,new_alert,non_statues,no_projets_concernes,
			rendez_confirms,no_photos,no_ticket,no_changement,no_sav_du_jour;

		@Override
		public void onPreExecute(){
			pd=new ProgressDialog(getActivity());
			pd.setMessage(getResources().getString(R.string.PLEASE_WAIT));
			pd.setCanceledOnTouchOutside(false);
			pd.show();
		}

		@Override
		protected Void doInBackground(String... params1) {								
			try {
				String reply=new RESTful(getActivity()).queryServer(getResources().getString(R.string.api_end_point)+"prospect", true);
		        System.out.println("json_cnst-1: " + reply);

		        reply=reply.replace(getResources().getString(R.string.json_cnst), "");
		        System.out.println("json_cnst-2: " + reply);

				JSONArray data = new JSONArray(reply);
				al=new ArrayList<ProspectData>();
				for(int i=0;i<data.length();i++){
					JSONObject jo=data.getJSONObject(i);
					ProspectData cd=new ProspectData();
					cd.first_name=jo.getString("prenom");
					cd.name=jo.getString("nom");
					cd.postal_code=jo.getString("cp");
					cd.ville=jo.getString("ville");
					cd.statut=jo.getString("statut");
					cd.id=jo.getString("id");
					cd.adresse=jo.getString("adresse");
					cd.last_rdv_confirmed_start_date=jo.getString("last_rdv_confirmed_start_date");
					cd.last_rdv_confirmed_statut=jo.getString("last_rdv_confirmed_statut");
					cd.tel_fixe=jo.getString("tel_fixe");
					try{
						cd.prospect_latitude=jo.getDouble("latitude");
						cd.prospect_longitude=jo.getDouble("longitude");
					}catch(JSONException e){
						cd.prospect_latitude=0.0;
						cd.prospect_longitude=0.0;
					}
					
					al.add(cd);
				}
				status_flag=2;
			} catch (IOException e) {
				if(e.getMessage().equals("403")){
					status_flag=5;
				}else if(e.getMessage().equals("204")){
					status_flag=7;
				}
				else{
					status_flag=3;
				}
				e.printStackTrace();
			} catch (JSONException e) {
				status_flag=4;
			}catch (NullPointerException e) {
				status_flag=6;
			}					 				 	 			 	
			return null;
		}

		@Override
		public void onPostExecute(Void m_adapter){               	
			pd.dismiss();
			if(status_flag==2){
		 		Collections.sort(al, new CustomComparator());
	 			implementListView(al);
		 	}else if(status_flag==3){
		 		new AlertDialog.Builder(getActivity())
				.setMessage("Erreur de connexion. Veuillez  réessayer à nouveau")
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					
					public void onClick(DialogInterface dialog, int id) {							
									
					}
				}).setIcon(android.R.drawable.stat_notify_error).show();
		 	}else if(status_flag==4){
		 		new AlertDialog.Builder(getActivity())
				.setMessage("Erreur du serveur. Veuillez réessayer à nouveau")
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					
					public void onClick(DialogInterface dialog, int id) {							
									
					}
				}).setIcon(android.R.drawable.stat_notify_error).show();
		 	}else if(status_flag==5){
		 		new AlertDialog.Builder(getActivity())
				.setMessage("Paramètres de connexion incorrects")
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					
					public void onClick(DialogInterface dialog, int id) {							
									
					}
				}).setIcon(android.R.drawable.stat_notify_error).show();
		 	}else if(status_flag==6){
		 		new AlertDialog.Builder(getActivity())
				.setMessage("Aucune donnée reçue . S'il vous plait réessayer plus tard.")
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					
					public void onClick(DialogInterface dialog, int id) {							
									
					}
				}).setIcon(android.R.drawable.stat_notify_error).show();
		 	}else if(status_flag==7){
		 		status.setVisibility(View.VISIBLE);
		 	}
		}
	}
	
	private class CustomComparator implements Comparator<ProspectData> { 

		@Override
		public int compare(ProspectData lhs, ProspectData rhs) {
			return (lhs.name+" "+lhs.first_name).compareTo(rhs.name+" "+rhs.first_name);
		}
	}
	
	private void implementListView(ArrayList<ProspectData> al_ed) {
		ad = new MyAdapter(getActivity(),
				R.layout.prospects_cell, al_ed);	
		prospects.setAdapter(ad);
		ad.notifyDataSetChanged();
		prospects.setTextFilterEnabled(true);
	}
	
	private class MyAdapter extends ArrayAdapter<ProspectData> implements Filterable{
		ArrayList<ProspectData> my_al;
		
		public MyAdapter(Context context, int textViewResourceId, ArrayList<ProspectData> al) {
			super(context, textViewResourceId, al);	
			my_al=al;
		}
				
		@Override
		public View getView(final int position, View convertView, ViewGroup parent) {
			View v = convertView;
			
			if (v == null) {				
				LayoutInflater vi = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);				
		        v = vi.inflate(R.layout.prospects_cell, null);		        			        				
			}
			
			TextView name=(TextView)v.findViewById(R.id.tv_name);
			name.setText(my_al.get(position).name+" "+my_al.get(position).first_name); 
			
			TextView address=(TextView)v.findViewById(R.id.tv_address);
			address.setText(my_al.get(position).postal_code+" "+my_al.get(position).ville); 
			
			TextView etape=(TextView)v.findViewById(R.id.tv_etape);
			etape.setText("Etape de saisie : "+my_al.get(position).statut);			
			
			v.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					ProspectDetails cd=new ProspectDetails();
					Bundle b=new Bundle();
					b.putString("id", my_al.get(position).id);
					b.putString("prospect_details_via", "prospects");
					cd.setArguments(b);
					getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, cd, "prospect_details").commit();
				}
			});
			return v;
		}
		
		@Override
		public Filter getFilter() {
			if(valueFilter==null){
				valueFilter=new ValueFilter();
			}
			return valueFilter;
		}
	}
	
	public class ProspectData{
		String name,first_name,postal_code,ville,statut,id,adresse,last_rdv_confirmed_start_date,last_rdv_confirmed_statut,tel_fixe;
		double prospect_latitude,prospect_longitude;
	}
	
	 @Override
	 public void onAttach(Activity activity) {
		 super.onAttach(activity);
	     try {	       
	    	 mListener= (OnTabChangedListener) activity;
	     } catch (ClassCastException e) {
	    	 throw new ClassCastException(activity.toString()+ " must implement OnTabChangedListener");
	     }
	 }
	 
	 private class ValueFilter extends Filter {
		    
		    @Override
		    protected FilterResults performFiltering(CharSequence constraint) {

		        FilterResults results=new FilterResults();
		        
		        if(constraint!=null && constraint.length()>0){
		            ArrayList<ProspectData> filterList=new ArrayList<ProspectData>();
		            String sc=constraint.toString().toLowerCase();
		           	            	
		            	for(int i=0;i<al.size();i++){	            		
			                if(al.get(i).name.toLowerCase().contains(sc)) {		                	
			                    filterList.add(al.get(i));
			                }
			            }

		            results.count=filterList.size();
		            results.values=filterList;

		        }else{
		        	results.count=al.size();
			        results.values=al;
		        }
		        return results;
		    }

			@SuppressWarnings("unchecked")
			@Override
			protected void publishResults(CharSequence constraint,
					FilterResults results) {	
				ArrayList<ProspectData> al=(ArrayList<ProspectData>) results.values;
				implementListView(al);	
			}
		}

	@Override
	public void onClick(View v) {
		int id = v.getId();
		if (id == R.id.iv_about_me) {
			AboutMe am=new AboutMe();
			Bundle b=new Bundle();
			b.putString("about_me_call", "prospects");
			am.setArguments(b);
			getActivity().getSupportFragmentManager()
			.beginTransaction().replace(R.id.content_frame1, am, "about_me").commit();
		}
	}
}
