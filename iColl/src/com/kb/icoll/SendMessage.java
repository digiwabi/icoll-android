//  iCollMobile
//
//  Created by Khaled Noui
//  Copyright (c) 2014 digiwabi. All rights reserved.
//

package com.kb.icoll;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import com.kb.icoll.utils.RESTful;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

public class SendMessage extends Fragment implements OnClickListener{
	EditText msg;
	SharedPreferences my_pref;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.send_message, container,false);
		
		my_pref=getActivity().getSharedPreferences(getResources().getString(R.string.APP_DATA), Activity.MODE_PRIVATE); 
		
		TextView header=(TextView)view.findViewById(R.id.tv_header);
		header.setText(ClientDetails.cdd.name+" "+ClientDetails.cdd.first_name);
		
		TextView back=(TextView)view.findViewById(R.id.tv_back);
		back.setOnClickListener(this); 
		
		TextView cancel=(TextView)view.findViewById(R.id.tv_cancel);
		cancel.setOnClickListener(this);
		
		TextView done=(TextView)view.findViewById(R.id.tv_ajouter_la_response);
		done.setOnClickListener(this);
		
		msg=(EditText)view.findViewById(R.id.ed_message);
		return view;			
	}

	@Override
	public void onClick(View v) {
		int id = v.getId();
		if (id == R.id.tv_ajouter_la_response) {
			if(msg.getText().length()>0){
				if(new RESTful(getActivity()).checkNetworkConnection()){
					new SendMessageData().execute("");
				}			
			}
		} else if (id == R.id.tv_back) {
			ClientTicketDetails ctd=(ClientTicketDetails)getActivity().getSupportFragmentManager().findFragmentByTag("client_ticket_details");
			if(ctd!=null){
				getActivity().getSupportFragmentManager().beginTransaction().remove(ctd).commit();
			}
			getActivity().getSupportFragmentManager().beginTransaction().remove(this).commit();
		} else if (id == R.id.tv_cancel) {
			getActivity().getSupportFragmentManager().beginTransaction().remove(this).commit();
		}
	}
	
	private class SendMessageData extends AsyncTask<String,Void,Void> {
		ProgressDialog pd;
		int status_flag;

		@Override
		public void onPreExecute(){
			pd=new ProgressDialog(getActivity());
			pd.setMessage(getResources().getString(R.string.PLEASE_WAIT));
			pd.setCanceledOnTouchOutside(false);
			pd.show();
		}

		@Override
		protected Void doInBackground(String... params1) {								
			try {
				JSONObject jo=new JSONObject();
				jo.put("projet_id", getArguments().getString("projet_id"));
				jo.put("ticket_id", getArguments().getString("ticket_id"));
				jo.put("reponse_id", getArguments().getString("reponse_id"));
				jo.put("id_from", my_pref.getString("user_id", ""));
				jo.put("message", msg.getText().toString());
				String url = getResources().getString(R.string.api_end_point)+"ticket"; 
				
				List <NameValuePair> nvps = new ArrayList <NameValuePair>();
				nvps.add(new BasicNameValuePair("projet_id", getArguments().getString("projet_id")));
				nvps.add(new BasicNameValuePair("ticket_id", getArguments().getString("ticket_id")));
				nvps.add(new BasicNameValuePair("reponse_id", getArguments().getString("reponse_id")));
				nvps.add(new BasicNameValuePair("id_from", my_pref.getString("user_id", "")));
				nvps.add(new BasicNameValuePair("message", "\""+msg.getText().toString()+"\""));			
					
				String reply=new RESTful(getActivity()).postServer(url, nvps);
							
				status_flag=2;
			} catch (IOException e) {
				if(e.getMessage().equals("403")){
					status_flag=2;
				}else{
					status_flag=3;
				}
				e.printStackTrace();
			} catch (JSONException e) {
				status_flag=4;
			}catch (NullPointerException e) {
				status_flag=6;
			}
					 				 	 			 	
		     return null;
		}

		@Override
		public void onPostExecute(Void m_adapter){               	
			pd.dismiss();
			if(status_flag==2){
				getActivity().getSupportFragmentManager().beginTransaction().remove(SendMessage.this).commit();
				ClientTicketDetails ct=(ClientTicketDetails)getActivity().getSupportFragmentManager().findFragmentByTag("client_ticket_details");
				if(ct!=null){
					ct.refreshData();
				}				
		 	}else if(status_flag==3){
		 		new AlertDialog.Builder(getActivity())
				.setMessage("Erreur de connexion. Veuillez  réessayer à nouveau")
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int id) {							
						
					}
				}).setIcon(android.R.drawable.stat_notify_error).show();
		 	}else if(status_flag==4){
		 		new AlertDialog.Builder(getActivity())
				.setMessage("Erreur du serveur. Veuillez réessayer à nouveau")
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int id) {							
						//getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, new Clients(), "clients").commit();		
					}
				}).setIcon(android.R.drawable.stat_notify_error).show();
		 	}else if(status_flag==5){
		 		new AlertDialog.Builder(getActivity())
				.setMessage("Paramètres de connexion incorrects")
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int id) {							
						//getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, new Clients(), "clients").commit();
					}
				}).setIcon(android.R.drawable.stat_notify_error).show();
		 	}else if(status_flag==6){
		 		new AlertDialog.Builder(getActivity())
				.setMessage("Aucune donnée reçue . S'il vous plait réessayer plus tard.")
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int id) {							
						//getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, new Clients(), "clients").commit();
					}
				}).setIcon(android.R.drawable.stat_notify_error).show();
		 	}
		}
	}
}
