//  iCollMobile
//
//  Created by Khaled Noui
//  Copyright (c) 2014 digiwabi. All rights reserved.
//

package com.kb.icoll;

import java.io.IOException;

import org.json.JSONException;
import org.json.JSONObject;

import com.kb.icoll.utils.RESTful;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

public class SignIn extends FragmentActivity implements OnClickListener, LocationListener{
	EditText email,login,pass;
	SharedPreferences my_pref;
	boolean doubleBackToExitPressedOnce;
	CheckBox cb_rem_pass;
	Handler h,h1;
	Runnable r,r1;
	double lat, lon;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.sign_in);
		
		my_pref=getSharedPreferences(getResources().getString(R.string.APP_DATA), Activity.MODE_PRIVATE); 
		
		email=(EditText)findViewById(R.id.ed_email);
		email.setText(my_pref.getString("email", ""));
		
		login=(EditText)findViewById(R.id.ed_login);
		login.setText(my_pref.getString("login", ""));
		
		pass=(EditText)findViewById(R.id.ed_password);
		pass.setText(my_pref.getString("pass", "")); 
		
		Button sign_in=(Button)findViewById(R.id.bt_sign_in);
		sign_in.setOnClickListener(this);
		
		Button contact_icoll=(Button)findViewById(R.id.bt_contact_icoll);
		contact_icoll.setOnClickListener(this);
		
		cb_rem_pass=(CheckBox)findViewById(R.id.cb_rem_password);
		cb_rem_pass.setChecked(my_pref.getBoolean("check_stat", false));
		
		updateLocation();
	}
	
	public String getDeviceName() {
	    String manufacturer = Build.MANUFACTURER;
	    String model = Build.MODEL;
	    if (model.startsWith(manufacturer)) {
	        return model;
	    } else {
	        return manufacturer + " " + model;
	    }
	}

	@Override
	public void onBackPressed() {
		//super.onBackPressed();
		ContactIcoll1 ci= (ContactIcoll1)getSupportFragmentManager().findFragmentByTag("contact_icoll");
		if(ci!=null){
			ci.doBack();
		}else{
			if (doubleBackToExitPressedOnce) {
		        super.onBackPressed();
		        return;
		    }

		    this.doubleBackToExitPressedOnce = true;
		    Toast.makeText(this, "Press again to exit", Toast.LENGTH_SHORT).show();

		    new Handler().postDelayed(new Runnable() {

		        @Override
		        public void run() {
		            doubleBackToExitPressedOnce=false;                       
		        }
		    }, 2000);
		}
	}
	
	@Override
	public void onClick(View v) {
		InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
		if(getCurrentFocus()!=null){
			imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
		}
		int id = v.getId();
		if (id == R.id.bt_sign_in) {
			if(email.getText().length()==0){
				new AlertDialog.Builder(this)
				.setMessage("Paramètres de connexion incorrects")
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						
					}
				}).show();
			}else if(!android.util.Patterns.EMAIL_ADDRESS.matcher(email.getText().toString()).matches()){
				new AlertDialog.Builder(this)
				.setMessage("Adresse email invalide")
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						
					}
				}).show();
			}else if(login.getText().length()==0){
				new AlertDialog.Builder(this)
				.setMessage("Paramètres de connexion incorrects")
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						
					}
				}).show();
			}else if(pass.getText().length()==0){
				new AlertDialog.Builder(this)
				.setMessage("Paramètres de connexion incorrects")
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						
					}
				}).show();
			}else{
				if(new RESTful(this).checkNetworkConnection()){
					new SignInService().execute("");
				}				
			}
		} else if (id == R.id.bt_contact_icoll) {
			getSupportFragmentManager().beginTransaction().replace(R.id.ll_signin_container, new ContactIcoll1(), "contact_icoll").commit();
		}
	}
	
	public void updateLocation() {
		LocationManager lm = (LocationManager)this.getSystemService(Context.LOCATION_SERVICE);
 		lm.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 1000L, 1.0f, this);
 		final Location location = lm.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
 		
 		h=new Handler();
 		r=new Runnable() {
			
			@Override
			public void run() {
				h1=new Handler();
				r1 =new Runnable() {
						
					public void run() {
						if(location!=null){							
							lat=location.getLatitude();
							lon=location.getLongitude();
						}else{
							h1.postDelayed(r1, 30000);
						}
						
					}
				};
				h1.post(r1);
			}
		};
		
		h.post(r);

	}
	
	@Override
	public void onStop() {
		super.onStop();
		h.removeCallbacks(r);
		h1.removeCallbacks(r1);
	}
	
	private class SignInService extends AsyncTask<String,Void,Void> {
		ProgressDialog pd;
		int status_flag;
		String societe_id,societe_nom,first_name,last_name,user_id,group_id,role_id,session_id;

		@Override
		public void onPreExecute(){
			pd=new ProgressDialog(SignIn.this);
			pd.setMessage(getResources().getString(R.string.PLEASE_WAIT));
			pd.setCanceledOnTouchOutside(false);
			pd.show();
		}

		@Override
		protected Void doInBackground(String... params1) {								
			try {
				String reply=new RESTful(SignIn.this).queryServer(getResources().getString(R.string.api_end_point)+"Login?username="+Uri.encode(login.getText().toString())+
						"&mdp="+Uri.encode(pass.getText().toString())+"&email="+Uri.encode(email.getText().toString())+"&user_latitude="+lat+"&user_longitude="+lon+"&user_device="+Uri.encode(getDeviceName())+"&user_os="+Uri.encode("Android "+Build.VERSION.RELEASE),false);
				
				JSONObject data = new JSONObject(reply);
				societe_id=data.getString("societe_id");
				societe_nom=data.getString("societe_nom");
				first_name=data.getString("user_prenom");
				last_name=data.getString("user_nom");
				user_id=data.getString("user_id");
				group_id=data.getString("group_id");
				role_id=data.getString("role_id");
				session_id=data.getString("session_id");
				status_flag=2;
			} catch (IOException e) {
				if(e.getMessage().equals("403")){
					status_flag=5;
				}else{
					status_flag=3;
				}
				e.printStackTrace();
			} catch (JSONException e) {
				status_flag=4;
			}catch (NullPointerException e) {
				status_flag=6;
			}
					 				 	 			 	
		     return null;
		}

		@Override
		public void onPostExecute(Void m_adapter){               	
			pd.dismiss();
			if(status_flag==2){				
		 		SharedPreferences.Editor ed=my_pref.edit();
		 		if(cb_rem_pass.isChecked()){
		 			ed.putString("pass", pass.getText().toString());
		 			ed.putBoolean("check_stat", true);
				}else{
					ed.putString("pass", "");
					ed.putBoolean("check_stat", false);
				}
		 		ed.putString("email", email.getText().toString());
		 		ed.putString("login", login.getText().toString());
		 		ed.putString("societe_id", societe_id);
		 		ed.putString("societe_nom", societe_nom);
		 		ed.putString("first_name", first_name);
		 		ed.putString("last_name", last_name);
		 		ed.putString("user_id", user_id);
		 		ed.putString("group_id", group_id);
		 		ed.putString("role_id", role_id);
		 		ed.putString("session_id", session_id);
		 		ed.commit();
		 		Intent i=new Intent(SignIn.this, MainActivity.class);
		 		startActivity(i);
		 		finish(); 
		 	}else if(status_flag==3){
		 		new AlertDialog.Builder(SignIn.this)
				.setMessage("Erreur de connexion. Veuillez réessayer à nouveau")
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					
					public void onClick(DialogInterface dialog, int id) {							
									
					}
				}).setIcon(android.R.drawable.stat_notify_error).show();
		 	}else if(status_flag==4){
		 		new AlertDialog.Builder(SignIn.this)
				.setMessage("Paramètres de connexion incorrects")
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					
					public void onClick(DialogInterface dialog, int id) {							
									
					}
				}).setIcon(android.R.drawable.stat_notify_error).show();
		 	}else if(status_flag==5){
		 		new AlertDialog.Builder(SignIn.this)
				.setMessage("Erreur du serveur. Veuillez réessayer à nouveau")
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					
					public void onClick(DialogInterface dialog, int id) {							
									
					}
				}).setIcon(android.R.drawable.stat_notify_error).show();
		 	}else if(status_flag==6){
		 		new AlertDialog.Builder(SignIn.this)
				.setMessage("Aucune donnée reçue. Veuillez réessayer à nouveau")
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					
					public void onClick(DialogInterface dialog, int id) {							
									
					}
				}).setIcon(android.R.drawable.stat_notify_error).show();
		 	}
		}
	}

	@Override
	public void onLocationChanged(Location location) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onProviderEnabled(String provider) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onProviderDisabled(String provider) {
		// TODO Auto-generated method stub
		
	}

}
