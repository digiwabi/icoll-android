//  iCollMobile
//
//  Created by Khaled Noui
//  Copyright (c) 2014 digiwabi. All rights reserved.
//

package com.kb.icoll;

import com.kb.icoll.utils.OnTabChangedListener;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;

public class Simulation extends Fragment implements OnClickListener{
	OnTabChangedListener mListener;
	TextView client,installation,finances;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.simulation, container,false);
		mListener.onTabChanged(5);
		
		client=(TextView)view.findViewById(R.id.tv_client);
		client.setOnClickListener(this);
		
		installation=(TextView)view.findViewById(R.id.tv_installation);
		installation.setOnClickListener(this);
		
		finances=(TextView)view.findViewById(R.id.tv_finances);
		finances.setOnClickListener(this);
		
		setSelectedTab(1);
		
		return view;
	}
	
	 @Override
	 public void onAttach(Activity activity) {
		 super.onAttach(activity);
	     try {	       
	    	 mListener= (OnTabChangedListener) activity;
	     } catch (ClassCastException e) {
	    	 throw new ClassCastException(activity.toString()+ " must implement OnTabChangedListener");
	     }
	 }

	@Override
	public void onClick(View v) {
		int id = v.getId();
		if (id == R.id.tv_client) {
			setSelectedTab(1);
		} else if (id == R.id.tv_installation) {
			setSelectedTab(2);
		} else if (id == R.id.tv_finances) {
			setSelectedTab(3);
		}
	}
	
	public void setSelectedTab(int i){
		client.setBackgroundResource(0);
		finances.setBackgroundResource(0);
		installation.setBackgroundResource(0);
		client.setTextColor(getResources().getColor(R.color.theme_color));
		installation.setTextColor(getResources().getColor(R.color.theme_color));
		finances.setTextColor(getResources().getColor(R.color.theme_color));
		if(i==1){
			client.setBackgroundResource(R.drawable.orange_solid_left_rounded);
			client.setTextColor(getResources().getColor(R.color.white));
			getActivity().getSupportFragmentManager().beginTransaction()
			.replace(R.id.ll_simulation_sub_container, new SimulationClient(), "simulation_client")
			.commit();
		}else if(i==2){
			installation.setBackgroundColor(getResources().getColor(R.color.theme_color));
			installation.setTextColor(getResources().getColor(R.color.white));
			getActivity().getSupportFragmentManager().beginTransaction()
			.replace(R.id.ll_simulation_sub_container, new SimulationInstallation(), "simulation_client")
			.commit();
		}else if(i==3){
			finances.setBackgroundResource(R.drawable.orange_solid_right_rounded);
			finances.setTextColor(getResources().getColor(R.color.white));
			getActivity().getSupportFragmentManager().beginTransaction()
			.replace(R.id.ll_simulation_sub_container, new SimulationFinances(), "simulation_client")
			.commit();
		}
	}
}
