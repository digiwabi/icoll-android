//  iCollMobile
//
//  Created by Khaled Noui
//  Copyright (c) 2014 digiwabi. All rights reserved.
//

package com.kb.icoll;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;

public class SimulationInstallation extends Fragment implements OnClickListener {

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.simulation_installation, container,false);
		//mListener.onTabChanged(2);
		
		TextView suivant=(TextView)view.findViewById(R.id.tv_suivant);
		suivant.setOnClickListener(this);
		
		return view;
	}

	@Override
	public void onClick(View v) {
		int id = v.getId();
		if (id == R.id.tv_suivant) {
			Simulation sm=(Simulation)getActivity().getSupportFragmentManager().findFragmentByTag("simulation");
			if(sm!=null){
				sm.setSelectedTab(3);
			}
		}
	}
}
