package com.kb.icoll.utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;

import com.kb.icoll.Agenda;
import com.kb.icoll.MonthView;
import com.kb.icoll.R;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class CalendarAdapter extends BaseAdapter {
	private Context mContext;

	private java.util.Calendar month;
	public GregorianCalendar pmonth; // calendar instance for previous month
	/**
	 * calendar instance for previous month for getting complete view
	 */
	public GregorianCalendar pmonthmaxset;
	private GregorianCalendar selectedDate,today_gc;
	int firstDay;
	int maxWeeknumber;
	int maxP;
	int calMaxP;
	int lastWeekDay;
	int leftDays;
	int mnthlength;
	String itemvalue, curentDateString;
	static String today;
	DateFormat df;

	//private ArrayList<String> items;
	public static List<String> dayString;
	private View previousView,previousView1,today_view;

	public CalendarAdapter(Context c, GregorianCalendar monthCalendar) {
		CalendarAdapter.dayString = new ArrayList<String>();
		Locale.setDefault(Locale.US);		
		month = monthCalendar;
		//month.setFirstDayOfWeek(GregorianCalendar.SUNDAY);
		selectedDate = (GregorianCalendar) monthCalendar.clone();
		df = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
		
		GregorianCalendar my_month = (GregorianCalendar) GregorianCalendar.getInstance();
		today_gc= (GregorianCalendar) my_month.clone();
		today=df.format(today_gc.getTime());

		mContext = c;
		month.set(GregorianCalendar.DAY_OF_MONTH, 1); 
		//this.items = new ArrayList<String>();
		
		curentDateString = df.format(selectedDate.getTime());
		refreshDays();

	}

//	public void setItems(ArrayList<String> items) {
//		for (int i = 0; i != items.size(); i++) {
//			if (items.get(i).length() == 1) {
//				items.set(i, "0" + items.get(i));
//			}
//		}
//		this.items = items;
//	}

	public int getCount() {
		return dayString.size();
	}

	public Object getItem(int position) {
		return dayString.get(position);
	}

	public long getItemId(int position) {
		return 0;
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		View v = convertView;
		TextView dayView;
		
		if (convertView == null) { 
			LayoutInflater vi = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			v = vi.inflate(R.layout.calendar_item, null);
		}
		
		ImageView iw = (ImageView) v.findViewById(R.id.date_icon);
		dayView = (TextView) v.findViewById(R.id.date);				
		
		// separates daystring into parts.
		String[] separatedTime = dayString.get(position).split("-");
		// taking last part of date. ie; 2 from 2012-12-02
		String gridvalue = separatedTime[2].replaceFirst("^0*", "");
		// checking whether the day is in current month or not.
		if ((Integer.parseInt(gridvalue) > 1) && (position < firstDay)) {
			// setting offdays to white color.
			dayView.setTextColor(Color.parseColor("#b1b1b1"));
			dayView.setClickable(false);
			dayView.setFocusable(false);
		} else if ((Integer.parseInt(gridvalue) < 7) && (position > 28)) {
			dayView.setTextColor(Color.parseColor("#b1b1b1"));
			dayView.setClickable(false);
			dayView.setFocusable(false);
		} else {
			// setting current month's days in Dark Grey color.
			dayView.setTextColor(Color.DKGRAY);
		}
		
		if (dayString.get(position).equals(today)) {
			//setSelected(v);
			v.setBackgroundResource(R.drawable.deep_grey_cell);
			dayView.setTextColor(Color.WHITE);
			iw.setBackgroundResource(R.drawable.white_dot);
			previousView1 = v;
			today_view=v; 			
		} else {
			v.setBackgroundResource(R.drawable.grey_cell1);	
			iw.setBackgroundResource(R.drawable.bg_dot);
		}
		
		if(dayString.get(position).equals(MonthView.selectedGridDate)){
			v.setBackgroundResource(R.drawable.bg_cell);
			dayView.setTextColor(Color.WHITE); 
			iw.setBackgroundResource(R.drawable.white_dot);
			previousView = v;
		}
		
		dayView.setText(gridvalue);			

//		// create date string for comparison
//		String date = dayString.get(position);
//
//		if (date.length() == 1) {
//			date = "0" + date;
//		}
//		String monthStr = "" + (month.get(GregorianCalendar.MONTH) + 1);
//		if (monthStr.length() == 1) {
//			monthStr = "0" + monthStr;
//		}
		
		

		// show icon if date is not empty and it exists in the items array
		
//		if (date.length() > 0 && items != null && items.contains(date)) {
//			iw.setVisibility(View.VISIBLE);
//		} else {
//			iw.setVisibility(View.INVISIBLE);
//		}
		
		
		if(Agenda.al_new_ag_list!=null){
			if(Agenda.al_new_ag_list.contains(dayString.get(position))){			
				iw.setVisibility(View.VISIBLE);
			}else{
				iw.setVisibility(View.INVISIBLE);
			}
		}
		return v;
	}

	public View setSelected(View view) {
		if (previousView != null) {
			previousView.setBackgroundResource(R.drawable.grey_cell1);
			TextView dayView1 = (TextView) previousView.findViewById(R.id.date);
			dayView1.setTextColor(Color.DKGRAY);
			
			ImageView iw = (ImageView) previousView.findViewById(R.id.date_icon);
			iw.setBackgroundResource(R.drawable.bg_dot);
		}
		
		if (previousView1 != null) {
			previousView1.setBackgroundResource(R.drawable.grey_cell1);
			TextView dayView1 = (TextView) previousView1.findViewById(R.id.date);
			dayView1.setTextColor(Color.DKGRAY);
			
			ImageView iw = (ImageView) previousView1.findViewById(R.id.date_icon);
			iw.setBackgroundResource(R.drawable.bg_dot);
		}
		
		previousView = view;

		TextView dayView = (TextView) view.findViewById(R.id.date);		
		view.setBackgroundResource(R.drawable.bg_cell);		
		dayView.setTextColor(Color.WHITE);
		ImageView iw = (ImageView) view.findViewById(R.id.date_icon);
		iw.setBackgroundResource(R.drawable.white_dot);
		
		if(today_view!=null){
			today_view.setBackgroundResource(R.drawable.deep_grey_cell);
			TextView dayView2= (TextView) today_view.findViewById(R.id.date);
			dayView2.setTextColor(Color.WHITE);
			
			ImageView iw1 = (ImageView) today_view.findViewById(R.id.date_icon);
			iw1.setBackgroundResource(R.drawable.white_dot);
		}
		
		if(view.equals(today_view)){
			view.setBackgroundResource(R.drawable.bg_cell);		
			dayView.setTextColor(Color.WHITE);
			iw.setBackgroundResource(R.drawable.white_dot);
		}
		
		return view;
	}

	public void refreshDays() {		
		//System.out.println("week = "+month.getActualMaximum(GregorianCalendar.WEEK_OF_MONTH));
		//System.out.println(month.get(GregorianCalendar.MONTH));
		previousView=null;
		previousView1=null;
		dayString.clear();
		Locale.setDefault(Locale.US);
		today_view=null;
		pmonth = (GregorianCalendar) month.clone();
		// month start day. ie; sun, mon, etc
		firstDay = month.get(GregorianCalendar.DAY_OF_WEEK);
		// finding number of weeks in current month.
		maxWeeknumber = month.getActualMaximum(GregorianCalendar.WEEK_OF_MONTH);
		// allocating maximum row number for the gridview.
		mnthlength = maxWeeknumber * 7;
		maxP = getMaxP(); // previous month maximum day 31,30....
		calMaxP = maxP - (firstDay - 1);// calendar offday starting 24,25 ...
		/**
		 * Calendar instance for getting a complete gridview including the three
		 * month's (previous,current,next) dates.
		 */
		pmonthmaxset = (GregorianCalendar) pmonth.clone();
		/**
		 * setting the start date as previous month's required date.
		 */
		pmonthmaxset.set(GregorianCalendar.DAY_OF_MONTH, calMaxP + 1);

		/**
		 * filling calendar gridview.
		 */
		for (int n = 0; n < mnthlength; n++) {
			itemvalue = df.format(pmonthmaxset.getTime());
			pmonthmaxset.add(GregorianCalendar.DATE, 1);
			//System.out.println(itemvalue);
			dayString.add(itemvalue);

		}
	}

	private int getMaxP() {
		int maxP;
		if (month.get(GregorianCalendar.MONTH) == month
				.getActualMinimum(GregorianCalendar.MONTH)) {
			pmonth.set((month.get(GregorianCalendar.YEAR) - 1),
					month.getActualMaximum(GregorianCalendar.MONTH), 1);
		} else {
			pmonth.set(GregorianCalendar.MONTH,
					month.get(GregorianCalendar.MONTH) - 1);
		}
		maxP = pmonth.getActualMaximum(GregorianCalendar.DAY_OF_MONTH);

		return maxP;
	}

}