package com.kb.icoll.utils;

public class ClientDetailsData{
	public String name,first_name,address,zip,ville,projet_etape,projet_etat,type_of_package,package_name,email,
		creation_date,client_situation_professionnelle,client_date_naissance,client_lieu_naissance,situation_maritale,no_enfant,no_enfant_charge,
		credit_impot,kit_fiscal,nom_conjoint,prenom_conjoint,date_conjoint,lieu_conjoint,nationality_conjoint,no_commande,date_commande,
		category_package,kit_choisi,total_wc,forfait_racco,montant_de_la,tarif_de,acces_util,nom_dutil,mot_de_passe,commercial,telephone_commercial,
		admin_gestion,tele_admin_gestion,admin_financier,tele_admin_financier,equipe_tech,tele_equipe_tech,etat_client,etat_finance,finance_type,org_finance,
		date_denvoi,numero_de_recommend,date_de_laccord,etat_de_travaux,date_denvoi_recommend,date_de_rec_du,date_de_rec_mairie,comp_mairie_recu,comp_des_abf,
		date_de_fin,numero_de_dp,telephone;
	public String projet_proprietaire,projet_residence_principale,projet_location,projet_residence_professionalle,projet_corporate,projet_date_of_achievement,
		projet_exposition,projet_inclinasion,projet_type_de_cloture,projet_largeur_maison,projet_hauter_maison,projet_profondeur_maison,projet_superfice_maison,
		projet_superfice_toit,projet_superfice_terrain,projet_hauteur_du_sol;
	public String date_de_fin_travaux,date_d_installation,date_de_reception_previsite,projet_etat_installation,date_remise_dossier_previsite,projet_date_raccordment,
		projet_etat_raccordment,projet_etat_mise_en_service,projet_etat_cloture,commande_id;

	public double lat,lon;
}
