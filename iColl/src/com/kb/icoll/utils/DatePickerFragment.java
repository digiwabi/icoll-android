package com.kb.icoll.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.widget.DatePicker;
import android.widget.TextView;

public class DatePickerFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener{

	TextView tv;
	int date,month,year;
	String req_date_format;
	boolean b;

	public DatePickerFragment(TextView event_date, String format, boolean need_current_date) {
		tv=event_date;
		req_date_format=format;
		b=need_current_date;
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		final Calendar c = Calendar.getInstance();
				
		if(!b){
			c.set(Calendar.YEAR, 1990);
			c.set(Calendar.MONTH, 0);
			c.set(Calendar.DATE, 1);
		}
		
		int year = c.get(Calendar.YEAR);
		int month = c.get(Calendar.MONTH);
		int day = c.get(Calendar.DAY_OF_MONTH);

		return new DatePickerDialog(getActivity(), this, year, month, day);
	}

	public void onDateSet(DatePicker view, int my_year, int my_month, int my_day) {
		year=my_year;
		month=my_month;
		date=my_day;
		
		String s=my_year+"-"+(month+1)+"-"+date;
		SimpleDateFormat sdf_default=new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
		Date d;
		try {
			d = sdf_default.parse(s);
			SimpleDateFormat new_sdf=new SimpleDateFormat(req_date_format, Locale.getDefault());			
			tv.setText(new_sdf.format(d));	
			//tv.setTextSize(15);
	        //tv.setTextColor(Color.parseColor("#4f4f4f"));
		} catch (ParseException e) {			
			e.printStackTrace();
		}			
		}
	
	public static String UTCToLocalTime(String s, String pattern) throws ParseException{
		SimpleDateFormat sourceFormat = new SimpleDateFormat(pattern);
		sourceFormat.setTimeZone(TimeZone.getTimeZone("UTC"));	

		Date parsed = sourceFormat.parse(s);		 

		SimpleDateFormat destFormat = new SimpleDateFormat(pattern);
		destFormat.setTimeZone(TimeZone.getDefault());

		String result = destFormat.format(parsed);
		return result;
	}
	
	public static String LocalTimeToUTC(String s, String pattern) throws ParseException{
		SimpleDateFormat sourceFormat = new SimpleDateFormat(pattern);
		sourceFormat.setTimeZone(TimeZone.getDefault());
		
		Date parsed = sourceFormat.parse(s); 

		SimpleDateFormat destFormat = new SimpleDateFormat(pattern);
		destFormat.setTimeZone(TimeZone.getTimeZone("UTC"));

		String result = destFormat.format(parsed);
		return result;
	}
}
