package com.kb.icoll.utils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import com.kb.icoll.R;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Binder;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;

public class LocationUpdateService extends Service implements LocationListener{

	String provider="network";
	Location location;
	Handler h1,h;
	Runnable r1,r;
	double current_lat,current_lon;
	IBinder mBinder;
	WakeLock mWakeLock;
	
	@Override
	public IBinder onBind(Intent intent) {
		
		return mBinder;
	}
	
	@Override
	public void onCreate() {
		super.onCreate();
		PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
		if(mWakeLock==null){
			mWakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "My Tag");
		}
		if(!mWakeLock.isHeld()){	
			mWakeLock.acquire();
		}
		mBinder = new LocationBinder();
	}

	public class LocationBinder extends Binder{
		public LocationUpdateService getService() {
			return LocationUpdateService.this;
		}
	}

	public void updateLocation() {
		
		// Getting the current location
		LocationManager lm = (LocationManager)this.getSystemService(Context.LOCATION_SERVICE);
 		lm.requestLocationUpdates(provider, 1000L, 1.0f, this);
 		location = lm.getLastKnownLocation(provider);
 		
 		h=new Handler();
 		r=new Runnable() {
			
			@Override
			public void run() {
				h1=new Handler();
				r1 =new Runnable() {
						
					public void run() {
						if(location!=null){							
							current_lat=location.getLatitude();
							current_lon=location.getLongitude();
							System.out.println("Current lat is: "+current_lat+" Current lon is: "+current_lon);
							new UpdateLocationPeriodic(current_lat,current_lon).execute("");
						}else{
							h1.postDelayed(r1, 30000);
						}
						
					}
				};
				h1.post(r1);
			}
		};
		
		h.post(r);

	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		h.removeCallbacks(r);
		h1.removeCallbacks(r1);
		
		if(mWakeLock.isHeld()){
			mWakeLock.release();
		}
	}

	@Override
	public void onLocationChanged(Location location) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onProviderEnabled(String provider) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onProviderDisabled(String provider) {
		// TODO Auto-generated method stub
		
	}
	
	private class UpdateLocationPeriodic extends AsyncTask<String,Void,Void> {

		//int status_flag;
		double my_lat,my_lon;
		
		public UpdateLocationPeriodic(double current_lat, double current_lon) {
			my_lat=current_lat;
			my_lon=current_lon;
		}

		@Override
		protected Void doInBackground(String... params1) {								
			try {
				
				List <NameValuePair> nvps = new ArrayList <NameValuePair>();
				nvps.add(new BasicNameValuePair("user_latitude", ""+my_lat));
				nvps.add(new BasicNameValuePair("user_longitude", ""+my_lon));
				
				@SuppressWarnings("unused")
				String reply=new RESTful(LocationUpdateService.this).postServer(getResources().getString(R.string.api_end_point)+"user", nvps);

			} catch (IOException e) {
				//e.printStackTrace();
			} catch (NullPointerException e) {
				e.printStackTrace();
			}										 				 	 			 	
			return null;
		}
		
		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			h.postDelayed(r, 1800000); //300000
		}
	}

}
