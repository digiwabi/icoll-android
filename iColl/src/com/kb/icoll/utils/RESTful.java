package com.kb.icoll.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.AbstractHttpEntity;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.ByteArrayBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import com.kb.icoll.R;

public class RESTful {
	String reply;
	Context my_con;
	SharedPreferences my_pref;
	
	public RESTful(Context con){
		my_con=con;
		my_pref=my_con.getSharedPreferences(my_con.getResources().getString(R.string.APP_DATA), Activity.MODE_PRIVATE);
	}
	
	public boolean checkNetworkConnection(){
		ConnectivityManager cm =(ConnectivityManager)my_con.getSystemService(Context.CONNECTIVITY_SERVICE);		 
	 	NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
	 	
	 	boolean isConnected	= activeNetwork != null && activeNetwork.isConnectedOrConnecting();
	 	if(!isConnected){
	 		new AlertDialog.Builder(my_con)
			.setMessage("Connexion Internet n\'est pas disponible!")
			.setPositiveButton("OK", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int id) {
					
				}
			}).show();
	 	}	 	
		return isConnected;	 	
	}
	
	public String postServer(String endpoint, Map<String, String> params,  boolean is_logged_in)
	        throws IOException {
	    URL url;
	    try {
	        url = new URL(endpoint);
	    } catch (MalformedURLException e) {
	        throw new IllegalArgumentException("invalid url: " + endpoint);
	    }
	    StringBuilder bodyBuilder = new StringBuilder();
	    Iterator<Entry<String, String>> iterator = params.entrySet().iterator();
	    // constructs the POST body using the parameters
	    while (iterator.hasNext()) {
	        Entry<String, String> param = iterator.next();
	        bodyBuilder.append(param.getKey()).append('=')
	                .append(param.getValue());
	        if (iterator.hasNext()) {
	            bodyBuilder.append('&');
	        }
	    }
	    String body = bodyBuilder.toString();
	    Log.v("POST", "Posting '" + body + "' to " + url);
	    byte[] bytes = body.getBytes();
	    HttpURLConnection conn = null;
	    try {
	        conn = (HttpURLConnection) url.openConnection();
	        conn.setDoOutput(true);
	        conn.setUseCaches(false);
	        conn.setFixedLengthStreamingMode(bytes.length);
	        conn.setRequestMethod("POST");
	        conn.setRequestProperty("Api-Key", "4251");
	        conn.setRequestProperty("Content-Type",
	                "application/x-www-form-urlencoded");
	        if(is_logged_in){
	        	System.out.println("societe_id: "+my_pref.getString("societe_id", ""));
	        	System.out.println("user_id: "+my_pref.getString("user_id", ""));
	        	conn.setRequestProperty("societe-id", my_pref.getString("societe_id", ""));
	        	conn.setRequestProperty("user-id", my_pref.getString("user_id", ""));
	        }
	        conn.setRequestProperty("Connection", "close");
	        // post the request
	        OutputStream out = conn.getOutputStream();
	        out.write(bytes);
	        out.close();
	        // handle the response
	        int status = conn.getResponseCode();
	        
	        if (status != 200) {
	          throw new IOException("Post failed with error code " + status);
	        }
	        
	        InputStream in = conn.getInputStream();
	        StringBuffer sb = new StringBuffer();
	        try {
	            int chr;
	            while ((chr = in.read()) != -1) {
	                sb.append((char) chr);
	            }
	            reply = sb.toString();
	            System.out.println(reply);	           	            
	        } finally {
	            in.close();
	        }
	        
	    } finally {
	        if (conn != null) {
	            conn.disconnect();
	        }
	    }
		return reply;
	}
	
	public String postServer(String endpoint, boolean is_logged_in)
	        throws IOException {
	    URL url;
	    try {
	        url = new URL(endpoint);
	    } catch (MalformedURLException e) {
	        throw new IllegalArgumentException("invalid url: " + endpoint);
	    }

	    Log.v("POST", "Quering to: "  + url);
	    HttpURLConnection conn = null;
	    try {
	        conn = (HttpURLConnection) url.openConnection();
	        conn.setDoOutput(true);
	        conn.setUseCaches(false);
	        conn.setRequestMethod("POST"); 
	        conn.setRequestProperty("Api-Key", "4251");
	        conn.setRequestProperty("Content-Type",
	                "application/x-www-form-urlencoded");
	        if(is_logged_in){
	        	System.out.println("societe_id: "+my_pref.getString("societe_id", ""));
	        	System.out.println("user_id: "+my_pref.getString("user_id", ""));
	        	conn.setRequestProperty("societe-id", my_pref.getString("societe_id", ""));
	        	conn.setRequestProperty("user-id", my_pref.getString("user_id", ""));
	        }
	        conn.setRequestProperty("Connection", "close");

	        int status = conn.getResponseCode();
	        
	        if (status != 200) {
	        	InputStream in = conn.getErrorStream();
		        StringBuffer sb = new StringBuffer();
		        try {
		            int chr;
		            while ((chr = in.read()) != -1) {
		                sb.append((char) chr);
		            }
		            reply = sb.toString();
		            System.out.println(reply);
		            
		        } finally {
		            in.close();
		        }
	          throw new IOException(""+status);
	        }
	        
	        InputStream in = conn.getInputStream();
	        StringBuffer sb = new StringBuffer();
	        try {
	            int chr;
	            while ((chr = in.read()) != -1) {
	                sb.append((char) chr);
	            }
	            reply = sb.toString();
	            System.out.println(reply);
	            
	        } finally {
	            in.close();
	        }
	        
	    } finally {
	        if (conn != null) {
	            conn.disconnect();
	        }
	    }
		return reply;
		}
	
		public String postMultyPartData(String endpoint,MultipartEntityBuilder entity)
				throws IOException {
			String reply;

			HttpClient httpclient = new DefaultHttpClient();
			HttpPost httppost;
	
			httppost = new HttpPost(endpoint); 	 
			httppost.setHeader("Api-Key", "4251");
			System.out.println("societe_id: "+my_pref.getString("societe_id", ""));
        	System.out.println("user_id: "+my_pref.getString("user_id", ""));
			httppost.setHeader("societe-id", my_pref.getString("societe_id", ""));
			httppost.setHeader("user-id", my_pref.getString("user_id", ""));
			httppost.setEntity(entity.build());		
			Log.v("Data", entity.build().toString());
			HttpResponse response = httpclient.execute(httppost);
			HttpEntity entity1 = response.getEntity();
			reply = EntityUtils.toString(entity1);		
			Log.i("RESPONSE CODE�-", ""+response.getStatusLine().getStatusCode());
			Log.i("GET RESPONSE�-", reply);
	
			return reply;
		}	
	
	   public String queryServer(String url, boolean is_logged_in) throws IOException{
	        Log.v("GetData", "Quering from: "+url);
	        String qResult = null;
	        HttpClient httpClient = new DefaultHttpClient();
	        HttpGet httpGet = new HttpGet(url);
	        httpGet.setHeader("Api-Key", "4251");
	        httpGet.setHeader("Content-Type", "application/x-www-form-urlencoded");
	        if(is_logged_in){
	        	System.out.println("societe_id: "+my_pref.getString("societe_id", ""));
	        	System.out.println("user_id: "+my_pref.getString("user_id", ""));
	        	httpGet.setHeader("societe-id", my_pref.getString("societe_id", ""));
	            httpGet.setHeader("user-id", my_pref.getString("user_id", ""));
	        } 
	         try {
	        	 HttpResponse response = httpClient.execute(httpGet);
	        	 int response_code= response.getStatusLine().getStatusCode();
	        	 Log.v("GetData", "Response Code: "+response_code);
			     HttpEntity httpEntity = response.getEntity();
			      
			      if ((httpEntity != null)&&(response_code==200)){
			    	 
			       InputStream inputStream = httpEntity.getContent();
			       Reader in = new InputStreamReader(inputStream);
			       BufferedReader bufferedreader = new BufferedReader(in,8);
			       StringBuilder stringBuilder = new StringBuilder();
			       
			       String stringReadLine = null;
			       
			       while ((stringReadLine = bufferedreader.readLine()) != null) {
			    	   stringBuilder.append(stringReadLine + "\n");
			        }
			       
			       qResult = stringBuilder.toString();
			       System.out.println(qResult);
			       inputStream.close();
			      }else{
			    	  throw new IOException(""+response_code);
			      }
	        } catch (ClientProtocolException e) {
	        	 e.printStackTrace();    	 
		     }           
		           return qResult;
	   }
	   
	   public String post_json(String endpoint, String params)
		        throws IOException {
		    URL url;
		    try {
		        url = new URL(endpoint);
		    } catch (MalformedURLException e) {
		        throw new IllegalArgumentException("invalid url: " + endpoint);
		    }

		    String body = params;
		    Log.v("POST", "Posting '" + body + "' to " + url);
		    byte[] bytes = body.getBytes();
		    HttpURLConnection conn = null;
		    try {
		        conn = (HttpURLConnection) url.openConnection();
		        conn.setDoOutput(true);
		        conn.setUseCaches(false);
		        conn.setFixedLengthStreamingMode(bytes.length);
		        conn.setRequestMethod("POST");
		        conn.setRequestProperty("Api-Key", "4251");
		        conn.setRequestProperty("Content-Type",
		                "application/x-www-form-urlencoded");
		       // if(is_logged_in){
		        System.out.println("societe_id: "+my_pref.getString("societe_id", ""));
	        	System.out.println("user_id: "+my_pref.getString("user_id", ""));
		        	conn.setRequestProperty("societe-id", my_pref.getString("societe_id", ""));
		        	conn.setRequestProperty("user-id", my_pref.getString("user_id", ""));
		       // }
//		        conn.setRequestProperty("Content-Type",
//		                "application/json");
		        conn.setRequestProperty("Connection", "close");
		        // post the request
		        OutputStream out = conn.getOutputStream();
		        out.write(bytes);
		        out.close();
		        // handle the response
		        int status = conn.getResponseCode();
		        
		        if (status != 200) {
		        	InputStream in = conn.getErrorStream();
			        StringBuffer sb = new StringBuffer();
			        try {
			            int chr;
			            while ((chr = in.read()) != -1) {
			                sb.append((char) chr);
			            }
			            reply = sb.toString();
			            System.out.println(reply);
			            
			        } finally {
			            in.close();
			        }
		          throw new IOException(""+status);
		        }
		        
		        if (status != 200) {
		          throw new IOException("Post failed with error code " + status);
		        }
		        
		        InputStream in = conn.getInputStream();
		        StringBuffer sb = new StringBuffer();
		        try {
		            int chr;
		            while ((chr = in.read()) != -1) {
		                sb.append((char) chr);
		            }
		            reply = sb.toString();
		            System.out.println(reply);
		            
		        } finally {
		            in.close();
		        }
		        
		    } finally {
		        if (conn != null) {
		            conn.disconnect();
		        }
		    }
			return reply;
	}
	   
	  public String postServer(String endpoint, List <NameValuePair> nvps)
			   throws IOException {

		  	  for (NameValuePair nvp : nvps) {
				  System.out.println("key: "+nvp.getName());
//				  System.out.println("value: "+nvp.getValue());
				  System.out.println("**************************");
			  }

		  
			   HttpClient client = new DefaultHttpClient();
			   HttpPost post = new HttpPost(endpoint);

			   post.setHeader("Api-Key", "4251");
			   System.out.println("societe_id: "+my_pref.getString("societe_id", ""));
			   System.out.println("user_id: "+my_pref.getString("user_id", ""));
			   post.setHeader("societe-id", my_pref.getString("societe_id", ""));
			   post.setHeader("user-id", my_pref.getString("user_id", ""));

			   AbstractHttpEntity ent=new UrlEncodedFormEntity(nvps, HTTP.UTF_8);
			   ent.setContentType("application/x-www-form-urlencoded; charset=UTF-8");
			   ent.setContentEncoding("UTF-8");
			   post.setEntity(ent);
			   HttpResponse response =client.execute(post);

			   String responseBody = EntityUtils.toString(response.getEntity());
			   int responseCode = response.getStatusLine().getStatusCode();
			   System.out.println("response_code: "+responseCode);
			   System.out.println("response_data: "+responseBody);
			   
			   
			   if (responseCode != 200) {
			          throw new IOException(""+responseCode);
			   }
			   
			   return responseBody;
	}
	  
	  public String postServerMultipart(String endpoint, List <NameValuePair> nvps, byte[] data)
			   throws IOException {

		  	  for (NameValuePair nvp : nvps) {
				  System.out.println("key: "+nvp.getName());
//				  System.out.println("value: "+nvp.getValue());
				  System.out.println("**************************");
			  }

		  
			   HttpClient client = new DefaultHttpClient();
			   HttpPost post = new HttpPost(endpoint);

			   post.setHeader("Api-Key", "4251");
			   System.out.println("societe_id: "+my_pref.getString("societe_id", ""));
			   System.out.println("user_id: "+my_pref.getString("user_id", ""));
			   post.setHeader("societe-id", my_pref.getString("societe_id", ""));
			   post.setHeader("user-id", my_pref.getString("user_id", ""));

//			   AbstractHttpEntity ent=new UrlEncodedFormEntity(nvps, HTTP.UTF_8);
//			   ent.setContentType("multipart/form-data; charset=UTF-8");
//			   ent.setContentEncoding("UTF-8");
			   
			   MultipartEntity entity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);
		        HttpClient httpClient = new DefaultHttpClient();
		            ByteArrayBody bab = new ByteArrayBody(data, System.currentTimeMillis() +".jpg");
		            entity.addPart("photoType", new StringBody(nvps.get(0).getValue()));
		            entity.addPart("client_id", new StringBody(nvps.get(1).getValue()));
		            entity.addPart("projet_id", new StringBody(nvps.get(2).getValue()));
		           entity.addPart("file", bab);
		       
			   
			   post.setEntity(entity);
			   HttpResponse response =client.execute(post);

			   String responseBody = EntityUtils.toString(response.getEntity());
			   int responseCode = response.getStatusLine().getStatusCode();
			   System.out.println("response_code: "+responseCode);
			   System.out.println("response_data: "+responseBody);
			   
			   
			   if (responseCode != 200) {
			          throw new IOException(""+responseCode);
			   }
			   
			   return responseBody;
	}
	  
	  public String postServer(String endpoint, HttpEntity nvps)
			   throws IOException {		  
			   HttpClient client = new DefaultHttpClient();
			   HttpPost post = new HttpPost(endpoint);

			   post.setHeader("Api-Key", "4251");
			   System.out.println("societe_id: "+my_pref.getString("societe_id", ""));
			   System.out.println("user_id: "+my_pref.getString("user_id", ""));
			   post.setHeader("societe-id", my_pref.getString("societe_id", ""));
			   post.setHeader("user-id", my_pref.getString("user_id", ""));
			   post.setHeader("user-id", "application/x-www-form-urlencoded; charset=UTF-8");
			   post.setEntity(nvps);
			   HttpResponse response =client.execute(post);

			   String responseBody = EntityUtils.toString(response.getEntity());
			   int responseCode = response.getStatusLine().getStatusCode();
			   System.out.println("response_code: "+responseCode);
			   System.out.println("response_data: "+responseBody);
			   
			   
			   if (responseCode != 200) {
			          throw new IOException(""+responseCode);
			   }
			   
			   return responseBody;
	}

}
