package com.kb.icoll.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import android.app.TimePickerDialog;
import android.app.Dialog;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.text.format.DateFormat;
import android.widget.TextView;
import android.widget.TimePicker;

public class TimePickerFragment extends DialogFragment implements TimePickerDialog.OnTimeSetListener{

	TextView tv;
	int hour,my_minute;
	String req_time_format;

	public TimePickerFragment(TextView event_date, String format) {
		tv=event_date;
		req_time_format=format;
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		final Calendar c = Calendar.getInstance();
		int hour = c.get(Calendar.HOUR_OF_DAY);
		int minute = c.get(Calendar.MINUTE);

		return new TimePickerDialog(getActivity(), this, hour, minute,
				DateFormat.is24HourFormat(getActivity()));
	}
	
	@Override
	public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
		if(view.isShown()){
			hour=hourOfDay;
			my_minute=minute;
			
			String s=hour+":"+minute;
			SimpleDateFormat sdf_default=new SimpleDateFormat("HH:mm", Locale.getDefault());
			Date d;
			try {
				d = sdf_default.parse(s);
				SimpleDateFormat new_sdf=new SimpleDateFormat(req_time_format, Locale.getDefault());			
				tv.setText(new_sdf.format(d));
				
				tv.setTextSize(14);
		        tv.setTextColor(Color.parseColor("#ffa248"));
			} catch (ParseException e) {			
				e.printStackTrace();
			}
		}					
	}	
}
